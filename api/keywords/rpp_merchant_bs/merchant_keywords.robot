*** Settings ***
Resource      ../../resources/init.robot
Resource      ../../keywords/rpp-mastermerchant/common_keywords.robot

*** Variables ***
${create_merchant_testData_path}    ../../resources/testdata/rpp_merchant_bs/createMerchantData.json
${update_merchant_testData_path}    ../../resources/testdata/rpp_merchant_bs/updateMerchantData.json
${create_merchant_endpoint}    /merchant-bs-api/v1/merchant
${merchant_detail_endpoint}    /merchant-bs-api/v1/merchant/detail/
${update_merchant_endpoint}     /merchant-bs-api/v1/merchant/update

*** Keywords ***
Get Api Merchnat Detail
    [Arguments]     ${merchant_id}
    &{header}=    Create Dictionary    Accesstoken=${accessToken}    Authorization=Basic ${RPP_GATEWAY_AUTHORIZATION_KEY}
    ${RESP}=      Get Request     ${RPP_GATEWAY_SESSION}    ${merchant_detail_endpoint}${merchant_id}    headers=&{header}
    Set Test Variable    ${RESP}

Prepare Body For Create Merchant
    [Arguments]    ${merchant_name_en}    ${merchant_name_th}
    ${txRefId} =    Get Time    format=epoch
    ${dummy_data_file_path}=    Catenate    SEPARATOR=/    ${CURDIR}    ${create_merchant_testData_path}
    ${body} =    Load JSON From File    ${dummy_data_file_path}
    ${body_edit_ref_id} =    Update Value To Json    ${body}    $.ref_id    ${txRefId}
    Get Rand Numbers Only    5
    ${body_edit_merchant_name_en}=   Update Value To Json    ${body_edit_ref_id}    $.merchant_name.en    ${merchant_name_en}${rand_num}
    ${body_edit_merchant_name_th}=   Update Value To Json    ${body_edit_merchant_name_en}    $.merchant_name.th    ${merchant_name_th}${rand_num}
    ${body_edit_sale_code}=   Update Value To Json    ${body_edit_merchant_name_th}    $.sale_code    ${SALE_CODE}
    Set Test Variable    ${FINAL_BODY}    ${body_edit_sale_code}

Post Api Create Merchant
    [Arguments]     ${request_data}
    &{header}=    Create Dictionary    Content-Type=application/json     Accesstoken=${accessToken}    Authorization=Basic ${RPP_GATEWAY_AUTHORIZATION_KEY}
    ${RESP}=      Post Request     ${RPP_GATEWAY_SESSION}    ${create_merchant_endpoint}    json=${request_data}    headers=&{header}
    Set Test Variable    ${RESP}

Prepare Body For Update Merchant
    ${endpoint} =  Catenate  SEPARATOR=    ${update_merchant_endpoint}    /5abb5981e9da8f042d15cbf3
    ${dummy_data_file_path}=    Catenate    SEPARATOR=/    ${CURDIR}    ${update_merchant_testData_path}
    ${body} =    Load JSON From File    ${dummy_data_file_path}
    Set Test Variable    ${ENDPOINT}
    Set Test Variable    ${BODY}

Put Api Update Merchant
    [Arguments]     ${request_data}    ${endpoint_and_param}
    &{header}=    Create Dictionary    Content-Type=application/json     Accesstoken=${accessToken}     Authorization=Basic ${RPP_GATEWAY_AUTHORIZATION_KEY}
    ${RESP}=      Put Request     ${RPP_GATEWAY_SESSION}    ${endpoint_and_param}    json=${request_data}    headers=&{header}
    Set Test Variable    ${RESP}
