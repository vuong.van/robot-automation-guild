*** Settings ***
Resource      ../../resources/init.robot

*** Variables ***
${check_product_endpoint}      /merchant-bs-api/v1/merchant/checkproduct

*** Keywords ***
Get Api Check Product
    [Arguments]     ${param}
    &{header}=    Create Dictionary    Accesstoken=${accessToken}       Authorization=Basic ${RPP_GATEWAY_AUTHORIZATION_KEY}
    ${RESP}=      Get Request     ${RPP_GATEWAY_SESSION}    ${check_product_endpoint}    params=&{param}    headers=&{header}
    Set Test Variable    ${RESP}