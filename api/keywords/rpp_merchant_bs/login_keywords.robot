*** Settings ***
Resource      ../../resources/init.robot
Resource      ../../../api/keywords/common/rpp_gateway_common.robot

*** Variables ***
${merchant_bs_login_endpoint}    /merchant-bs-api/v1/user/login

*** Keywords ***
Post Api Login
    [Arguments]     ${request_data}
    ${RESP}=      Post Request     ${RPP_GATEWAY_SESSION}    ${merchant_bs_login_endpoint}    data=${request_data}    headers=&{RPP_GATEWAY_HEADERS}
    Set Test Variable    ${RESP}

Get Accesstoken
    ${accessToken}    Set Variable     ${Resp.json()['data']['accessToken']}
    Set Test Variable    ${accessToken}

Authorize BS Service For Create Merchant In Audit Web
    [Arguments]     ${request_data}
    Create RPP Gateway Header    ${RPP_GATEWAY_HOST_FOR_TEST_AUDIT_WEB}
    Post Api Login     ${request_data}
    Get Accesstoken