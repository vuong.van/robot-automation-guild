*** Settings ***
Resource      ../../resources/init.robot

*** Variables ***
${search_location_endpoint}    /merchant-bs-api/v1/location

*** Keywords ***
Get Api Search Location
    &{header}=    Create Dictionary    Accesstoken=${accessToken}     Authorization=Basic ${RPP_GATEWAY_AUTHORIZATION_KEY}
    &{param}=     Create Dictionary    latitude=13.7644702    longitude=100.5660648    distance=10
    ${RESP}=      Get Request     ${RPP_GATEWAY_SESSION}    ${search_location_endpoint}    params=&{param}    headers=&{header}
    Set Test Variable    ${RESP}