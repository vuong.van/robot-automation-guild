*** Settings ***
Resource      ../../resources/init.robot

*** Variables ***
${check_dopa_endpoint}         /merchant-bs-api/v1/merchant/dopa

*** Keywords ***
Post Api Check Merchant Dopa
    [Arguments]     ${request_data}
    &{header}=    Create Dictionary    Content-Type=application/json    Accesstoken=${accessToken}     Authorization=Basic ${RPP_GATEWAY_AUTHORIZATION_KEY}
    ${RESP}=      Post Request     ${RPP_GATEWAY_SESSION}    ${check_dopa_endpoint}    data=${request_data}    headers=&{header}
    Set Test Variable    ${RESP}