*** Settings ***
Resource    ../common/api_common.robot

*** Variables ***
${payment_url}    /payment/api/charge

*** Keywords ***
Post Payment Charge
    [Arguments]    ${data}
    ${RESP}=    Post Request    ${GATEWAY_SESSION}    ${payment_url}    data=${data}    headers=&{O2O_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get Payment Query
    [Arguments]    ${transaction_reference_id}
    ${RESP}=    Get Request    ${GATEWAY_SESSION}    /payment/api/query?txRefId=${transaction_reference_id}    headers=&{O2O_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Post Payment Cancel
    [Arguments]    ${data}
    ${RESP}=    Post Request    ${GATEWAY_SESSION}    ${payment_url}/cancel    data=${data}    headers=&{O2O_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get Transaction Reference Id
    ${TRANSACTION_REF_ID}=    Get Property Value From Json By Index    .txRefId    0
    Set Test Variable    ${TRANSACTION_REF_ID}

Get Charge Payment Id
    ${CHARGE_PAYMENT_ID}=    Get Property Value From Json By Index    .paymentId    0
    Set Test Variable    ${CHARGE_PAYMENT_ID}

Get Charge Cancel Payment Id
    ${CHARGE_CANCEL_PAYMENT_ID}=    Get Property Value From Json By Index    .paymentId    0
    Set Test Variable    ${CHARGE_CANCEL_PAYMENT_ID}
