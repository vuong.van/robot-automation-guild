*** Settings ***
Resource    ../common/api_common.robot

*** Variables ***
${amount}               "102"
${currency}             "THB"
${payment_method}       "CREDIT_CARD"
${credit_card}          "00acuiWUJwD8cgVwesx1wzqrHvTIwuREtPN01Ph6lRPrzIFj+H5yj3h6UsUtD+hT0/+t3KZZEp/xysrFCv8WrNeKefwtltCvLlZTSkpQ1sBORqO9DTa4MYT2fqXnrCaBJBoAM2u3PzQ76C7s72kvzkx9qfz1WbDBheURcHFEcvue+nU=U2FsdGVkX1+WL6cQecdCtGQfiSncWO9ZfZSLp/eAWEMaXhjPUywtcC/ECFv2CbuQ"
${invalid_id}           1111111111
${meta_data}             {"หมวดหมู่ภาษาไทย" :"ทดสอบการชำระเงิน(ผ่านบัตรเครดิต)","catagory" :"2C2P_TEST001"}
${merchant_id}          2100001

*** Keywords ***
Post Payment Escrow Charge
    [Arguments]    ${project_id}    ${data}
    ${RESP}=    Post Request    ${GATEWAY_SESSION}    /payment/api/projects/${project_id}/escrow/charges    data=${data}    headers=&{O2O_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get Payment Escrow Query
    [Arguments]    ${project_id}    ${id}
    ${RESP}=    Get Request    ${GATEWAY_SESSION}    /payment/api/projects/${project_id}/escrow/query/${id}    headers=&{O2O_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Post Payment Escrow Refund
    [Arguments]    ${project_id}    ${id}    ${data}
    ${RESP}=    Post Request    ${GATEWAY_SESSION}    /payment/api/projects/${project_id}/escrow/charges/${id}/refunds    data=${data}    headers=&{O2O_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get Transaction ID
    ${transaction_id}=    Get Property Value From Json By Index    .id    0
    Set Test Variable    ${transaction_id}

Get Search Payment Escrow Transaction
    [Arguments]    ${project_id}    ${params_uri}=${EMPTY}
    ${RESP}=    Get Request    ${GATEWAY_SESSION}    /payment/api/projects/${project_id}/escrow/transactions    params=${params_uri}    headers=&{O2O_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get Search Payment Escrow Transaction By Id
    [Arguments]    ${project_id}    ${transaction_id}    ${params_uri}=${EMPTY}
    ${RESP}=    Get Request    ${GATEWAY_SESSION}    /payment/api/projects/${project_id}/escrow/transactions/${transaction_id}    params=${params_uri}    headers=&{O2O_GATEWAY_HEADER}
    Set Test Variable    ${RESP}
