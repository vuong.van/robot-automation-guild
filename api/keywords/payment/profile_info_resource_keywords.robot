*** Settings ***
Resource    ../common/api_common.robot

*** Keywords ***
Get Active Profiles
    ${RESP}=    Get Request    ${GATEWAY_SESSION}    /payment/api/profile-info    headers=&{O2O_GATEWAY_HEADER}
    Set Test Variable    ${RESP}