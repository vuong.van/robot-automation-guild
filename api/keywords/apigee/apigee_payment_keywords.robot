*** Keywords ***
Get Apigee Payment Status
    [Arguments]      ${transaction_reference_Id}
    ${resp}=    Get Request    ${APIGEE_SESSION}    /payment/v1/payment/${transaction_reference_Id}/status    headers=&{api_headers}
    Set Test Variable    ${resp}

Get Transaction By Reference ID
    ${TEST_VARIABLE_TX_REF}=    Get Property Value From Json By Index    .transactionReferenceId    0
    Set Test Variable    ${TEST_VARIABLE_TX_REF}

Get Apigee Payment Status By ClientTrxId
    [Arguments]      ${terminal_id}      ${clientTrxId}      ${brandId}      ${branchId}
    ${resp}=    Get Request    ${APIGEE_SESSION}    /payment/v1/payment/terminals/${terminal_id}/client-trxs/${clientTrxId}/status?brandId=${brandId}&branchId=${branchId}    headers=&{api_headers}
    Set Test Variable    ${resp}
