*** Settings ***
Resource    ../common/json_common.robot
Resource    ../common/api_common.robot
Resource    ../common/dummy_data_common.robot
Resource    ../../keywords/rpp-mastermerchant/common_keywords.robot

*** Variables ***
${merchants_api}    /rpp-merchant/api/merchants

*** Keywords ***
Post Create Merchant
    [Arguments]    ${json_body}
    ${RESP}=    Post Request    ${GATEWAY_SESSION}    ${merchants_api}    data=${json_body}    headers=&{O2O_GATEWAY_HEADER}
    Set Test Variable    ${RESP}
    @{merchant_sequence_id}    Get Value From Json    ${RESP.json()}    $.id
    Set Test Variable    ${MERCHANT_SEQUENCE_ID}    @{merchant_sequence_id}[0]
    @{merchant_brand_id}    Get Value From Json    ${RESP.json()}    $.merchantId
    Set Test Variable    ${MERCHANT_BRAND_ID}    @{merchant_brand_id}[0]
    Response Correct Code    ${CREATED_CODE}

Get Merchant Information
    [Arguments]    ${params_uri}=${EMPTY}
    ${RESP}=    Get Request    ${GATEWAY_SESSION}    ${merchants_api}    params=${params_uri}    headers=&{O2O_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get True You Merchant ID
    @{TY_MERCHANT_ID}=    Get Value From Json    ${resp.json()}    $.content..'trueyouId'
    Set Test Variable    ${TY_MERCHANT_ID}

Get Merchant IDs
    @{merchant_id_list}=    Get Value From Json    ${resp.json()}    $.content..merchantId
    Set Test Variable    ${TEST_DATA_MERCHANT_ID}    @{merchant_id_list}[0]
    ${TEST_DATA_MERCHANT_SEQUENCE_ID}=    Get Property Value From Json By Index    .id    0
    Set Test Variable    ${TEST_DATA_MERCHANT_SEQUENCE_ID}
    ${TEST_DATA_STORE_NAME_EN}=    Get Property Value From Json By Index    .storeNameEn    0
    Set Test Variable    ${TEST_DATA_STORE_NAME_EN}
    ${TEST_DATA_STORE_NAME_TH}=    Get Property Value From Json By Index    .storeNameTh    0
    Set Test Variable    ${TEST_DATA_STORE_NAME_TH}

Get Random Merchant IDs Follow Business Instruction
    Get Rand Numbers Only    15
    Set Test Variable    ${MERCHANT_TY_ID}    ${rand_num}
    ${merchant_id}=    Get Substring    ${MERCHANT_TY_ID}    3    9
    Set Test Variable    ${MERCHANT_BRAND_ID}    ${merchant_id}
    Get Rand Numbers Only    10
    Set Test Variable    ${MERCHANT_TMN_ID}    ${rand_num}00001
