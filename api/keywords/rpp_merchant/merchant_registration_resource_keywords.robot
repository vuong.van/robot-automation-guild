*** Settings ***
Resource    ../common/json_common.robot

*** Keywords ***
Get All Merchant Registrations Raw
    [Arguments]    ${params_uri}=${EMPTY}
    ${RESP}=    Get Request    ${GATEWAY_SESSION}    /rpp-merchant/api/merchant-registrations/raw    params=${params_uri}    headers=&{O2O_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

#Test Data Preparation
Get Test Data Merchant Id On Mongo Database
    [Arguments]    ${filter_by}=${EMPTY}
    Get All Merchant Registrations Raw    ${filter_by}
    ${TEST_DATA_MERCHANT_MONGO_ID}=    Get Property Value From Json By Index    .id    0
    Set Test Variable    ${TEST_DATA_MERCHANT_MONGO_ID}