*** Settings ***
Resource    ../common/api_common.robot

*** Variables ***
${create_activation_code_api}    /rpp-merchant/api/outlets/
${activate_activation_code_api}    /rpp-merchant/api/terminals/activate
${get_activation_code_api}    /rpp-merchant/api/activationcodes
${get_terminal_api}    /rpp-merchant/api/terminals/activatecode/
${activate_terminal_api}    /rpp-merchant/api/terminals/activate/edc

*** Keywords ***
Create And Get Activation Code ID By Outlet ID
    [Arguments]    ${outlet_id}    ${json_data}
    ${RESP}=    Post Request    ${GATEWAY_SESSION}    ${create_activation_code_api}${outlet_id}/activationcodes    data=${json_data}   headers=&{O2O_GATEWAY_HEADER}
    Set Test Variable    ${RESP}
    @{activation_codes}    Get Value From Json    ${resp.json()}    $..activationCodes
    Set Test Variable    @{ACTIVATION_CODES}

Get Activation Code Information
    [Arguments]    ${properties_and_values}
    ${RESP}=    Get Request    ${GATEWAY_SESSION}    ${get_activation_code_api}?${properties_and_values}    headers=&{O2O_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get Activation By Outlet Id
    [Arguments]    ${outlet_id}    ${params_uri}=${EMPTY}
    ${RESP}=    Get Request    ${GATEWAY_SESSION}    /rpp-merchant/api/outlets/${outlet_id}/activationcodes    params=${params_uri}    headers=&{O2O_GATEWAY_HEADER}
    Set Test Variable    ${RESP}
    
Activate Activation Code And Create Terminal For Outlet
    [Arguments]    ${json_data}
    ${RESP}=    Put Request    ${GATEWAY_SESSION}    ${activate_activation_code_api}    data=${json_data}    headers=&{O2O_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get Random Activation Code
    ${RANDOM_ACTIVATION_CODE}=    Generate Random String    12    [LETTERS][NUMBERS][UPPER]
    Set Test Variable    ${RANDOM_ACTIVATION_CODE}

Get Terminal External ID
    ${EXTERNAL_TERMINAL_ID}=    Get Value From Json    ${resp.json()}    $..terminalExternalId
    Set Test Variable    ${EXTERNAL_TERMINAL_ID}

Get Terminal Information
    [Arguments]    ${properties_and_values}
    ${RESP}=    Get Request    ${GATEWAY_SESSION}    ${get_terminal_api}${properties_and_values}    headers=&{O2O_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Activate Terminal
    [Arguments]    ${json_data}
    ${RESP}=    Put Request    ${GATEWAY_SESSION}    ${activate_terminal_api}    data=${json_data}    headers=&{O2O_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

