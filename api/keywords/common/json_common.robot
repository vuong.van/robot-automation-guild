*** Settings ***
Library    Collections
Library    String
Library    JSONLibrary    

*** Keywords ***
Get Property Value From Json By Index
    [Arguments]    ${property}    ${index}    ${input_json}=${RESP.json()}
    @{property_values}=    Get Value From Json    ${input_json}    $.${property}
    [Return]    @{property_values}[${index}]

Is Property With Value Exist In Json
    [Arguments]    ${property}    ${value}    ${input_json}=${RESP.json()}
    @{property_values}=    Get Value From Json    ${input_json}    $.${property}
    ${return_value}=    Run Keyword And Return Status    List Should Contain Value    ${property_values}    ${value}
    [Return]    ${return_value}

Get Property Value By Another Property Value
    [Arguments]    ${property}    ${value}    ${return_property}    ${input_json}=${RESP.json()}
    @{property_values}=    Get Value From Json    ${input_json}    $.${property}
    ${matched_value_index}=    Get Index From List    ${property_values}    ${value}
    @{return_property_values}=    Get Value From Json    ${input_json}    $.${return_property}
    ${return_value}=    Run Keyword If    '${matched_value_index}'!='-1'    Set Variable    @{return_property_values}[${matched_value_index}]
    [Return]    ${return_value}

Generate Json From Json Property Value
    [Arguments]    ${property}    ${generated_key}    ${input_json}=${RESP.json()}
    ${returnValue}=    Set Variable    {
    @{property_values}=    Get Value From Json    ${input_json}    $.${property}
    :FOR    ${value}    IN    @{property_values}
    \    ${returnValue}=    Catenate    SEPARATOR=,{    ${returnValue}    "${generated_key}":${value}}
    ${returnValue}=    Replace String    ${returnValue}    {,{    {
    [Return]    ${returnValue}

Update Json With Array
    [Arguments]    ${json_string}    ${update_property}    ${array_string}
    ${update_value}=    Get Property Value From Json By Index    .${update_property}    0    ${json_string}
    ${string_to_replace}=    BuiltIn.Set Variable    "${update_property}":${SPACE}${update_value}
    ${new_string}=    JSONLibrary.Convert JSON To String    ${json_string}
    ${final_string}=    String.Replace String    ${new_string}    ${string_to_replace}    "${update_property}":${SPACE}${array_string}
    ${final_json}=    JSONLibrary.Convert String to JSON    ${final_string}
    [Return]    ${final_json}

Get All Property Value
    [Arguments]    ${property}    ${input_json}=${RESP.json()}
    @{property_values}=    Get Value From Json    ${input_json}    $.${property}
    [Return]    @{property_values}