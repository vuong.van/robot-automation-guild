*** Keywords ***
Get Date With Format And Increment
    [Arguments]    ${result_format}    ${increment}
    ${desired_date} =    Get Current Date    result_format=${result_format}    increment=${increment}
    Set Test Variable    ${DESIRED_DATE}    ${desired_date}