*** Settings ***
Resource    ../common/api_common.robot

*** Keywords ***
Delete Reset All Cache
    ${RESP}=    Delete Request    ${GATEWAY_SESSION}    /uaa/api/cache/clear-all    headers=&{O2O_GATEWAY_HEADER}
    Set Test Variable   ${RESP}