*** Settings ***
Resource    ../common/api_common.robot
Resource    ../common/json_common.robot

*** Variables ***
${user_group_api}    /uaa/api/user-groups

*** Keywords ***
Delete User Group
    [Arguments]    ${user_group_id}
    ${RESP}=    Delete Request    ${GATEWAY_SESSION}    ${user_group_api}/${user_group_id}    headers=&{O2O_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Post Create User Group
    [Arguments]    ${data}
    ${RESP}=    Post Request    ${GATEWAY_SESSION}    ${user_group_api}    data=${data}    headers=&{O2O_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Put Update User Group
    [Arguments]    ${data}
    ${RESP}=    Put Request    ${GATEWAY_SESSION}    ${user_group_api}    data=${data}    headers=&{O2O_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get All User Groups
    [Arguments]    ${params_uri}=${EMPTY}
    ${RESP}=    Get Request    ${GATEWAY_SESSION}    ${user_group_api}    params=${params_uri}    headers=&{O2O_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Get User Group
    [Arguments]    ${user_group_id}
    ${RESP}=    Get Request    ${GATEWAY_SESSION}    ${user_group_api}/${user_group_id}    headers=&{O2O_GATEWAY_HEADER}
    Set Test Variable    ${RESP}

Reset Production User Group
    Get All User Groups    name.equals=${PROD_TEST_USER_GROUP}
    ${user_group_id}=    Get Property Value From Json By Index    .id    0
    Put Update User Group    { "id": ${user_group_id}, "name": "${PROD_TEST_USER_GROUP}", "description": "${PROD_TEST_USER_GROUP}","permissionGroups":[],"permissions":[],"users":[] }

#Data preparation
Get User Group Id
    [Arguments]    ${filter}=${EMPTY}
    Get All User Groups    ${filter}
    ${USER_GROUP_ID}=    Get Property Value From Json By Index    .id    0
    Set Test Variable    ${USER_GROUP_ID}

Remove All User Group
    [Arguments]    ${filter}    ${return_property}=.id
    Get All User Groups    ${filter}
    @{user_group_list}=    json_common.Get All Property Value    ${return_property}
    :FOR    ${index}    IN    @{user_group_list}
    \    Delete User Group    ${index}