*** Settings ***
Documentation    Tests to verify that payment charge api with 2C2P works correcty
Resource    ../../../resources/init.robot
Resource    ../../../keywords/payment/payment_escrow_resource_keywords.robot
Resource    ../../../keywords/payment_transaction/payment_transaction_resource_keywords.robot
Test Setup        Generate Gateway Header With Scope and Permission    ${ROLE_USER}    ${ROLE_USER_PASSWORD}    payment.payment.write
Test Teardown     Run Keywords    Delete Created Client And User Group    AND    Delete All Sessions

*** Test Cases ***
TC_O2O_08716
    [Documentation]    [Payment][PaymentCharge] 2C2P Charge success with metadata fields and valid project_id
    [Tags]    High    Regression    Smoke    UnitTest    Robot
    Generate Transaction Reference    6
    Post Payment Escrow Charge     ${VALID_PAYMENT_PROJECT_ID}     {"paymentMethod":${payment_method},"currency":${currency},"paymentInfo":${credit_card},"amount":${amount},"extRefId":"${TRANSACTION_REFERENCE}","merchantRefId":"${merchant_id}","metaData":${meta_data}}
    Response Correct Code    ${CREATED_CODE}
    Response Should Contain Property With Value    .responseCode    2_00
    Response Should Contain Property With Value    .extRefId        ${TRANSACTION_REFERENCE}
    Response Should Contain Property With Value    .status          SUCCESS
    Response Should Contain Property With Value    .action          CHARGE

TC_O2O_08717
    [Documentation]    [Payment][PaymentCharge] 2C2P Charge success without metadata fields and valid project_id
    [Tags]    High    Regression    Smoke    UnitTest    Robot
    Generate Transaction Reference    6
    Post Payment Escrow Charge     ${VALID_PAYMENT_PROJECT_ID}     {"paymentMethod":${payment_method},"currency":${currency},"paymentInfo":${credit_card},"amount":${amount},"extRefId":"${TRANSACTION_REFERENCE}","merchantRefId":"${merchant_id}"}
    Response Correct Code    ${CREATED_CODE}
    Response Should Contain Property With Value    .responseCode    2_00
    Response Should Contain Property With Value    .extRefId        ${TRANSACTION_REFERENCE}
    Response Should Contain Property With Value    .status          SUCCESS
    Response Should Contain Property With Value    .action          CHARGE
    Response Should Contain Property With Null Value    .metaData

TC_O2O_09636
    [Documentation]    [Payment][PaymentCharge] 2C2P charge with paymentMethod =WALLET
    [Tags]    High    Regression    Sanity    Smoke    UnitTest    Robot
    Generate Transaction Reference    6
    Post Payment Escrow Charge     ${VALID_PAYMENT_PROJECT_ID}     {"paymentMethod":"WALLET","currency":${currency},"paymentInfo":${credit_card},"amount":${amount},"extRefId":"${TRANSACTION_REFERENCE}","merchantRefId":"${merchant_id}","metaData":${meta_data}}
    Response Correct Code    ${BAD_REQUEST_CODE}
    Response Should Contain Property With Value    .message    error.validation
    Response Should Contain Property With Value    fieldErrors.code        payment_not_support
    Response Should Contain Property With Value    fieldErrors.message     Not support payment method
    Response Should Contain Property With Null Value    fieldErrors.id

TC_O2O_09637
    [Documentation]    [Payment][PaymentCharge] 2C2P charge with paymentMethod =TRUEYOU_MASTER_CARD
    [Tags]    Medium    Regression    UnitTest    Robot
    Generate Transaction Reference    6
    Post Payment Escrow Charge     ${VALID_PAYMENT_PROJECT_ID}     {"paymentMethod":"TRUEYOU_MASTER_CARD","currency":${currency},"paymentInfo":${credit_card},"amount":${amount},"extRefId":"${TRANSACTION_REFERENCE}","merchantRefId":"${merchant_id}","metaData":${meta_data}}
    Response Correct Code    ${BAD_REQUEST_CODE}
    Response Should Contain Property With Value    .message    error.validation
    Response Should Contain Property With Value    fieldErrors.code        payment_not_support
    Response Should Contain Property With Value    fieldErrors.message     Not support payment method
    Response Should Contain Property With Null Value    fieldErrors.id

TC_O2O_09638
    [Documentation]    [Payment][PaymentCharge] 2C2P charge with paymentMethod =ALIPAY
    [Tags]    Medium    Regression    UnitTest    Robot
    Generate Transaction Reference    6
    Post Payment Escrow Charge     ${VALID_PAYMENT_PROJECT_ID}     {"paymentMethod":"ALIPAY","currency":${currency},"paymentInfo":${credit_card},"amount":${amount},"extRefId":"${TRANSACTION_REFERENCE}","merchantRefId":"${merchant_id}","metaData":${meta_data}}
    Response Correct Code    ${BAD_REQUEST_CODE}
    Response Should Contain Property With Value    .message    error.validation
    Response Should Contain Property With Value    fieldErrors.code        payment_not_support
    Response Should Contain Property With Value    fieldErrors.message     Not support payment method
    Response Should Contain Property With Null Value    fieldErrors.id

TC_O2O_09639
    [Documentation]    [Payment][PaymentCharge] 2C2P charge with paymentMethod =DEBIT_CARD
    [Tags]    Medium    Regression    UnitTest    Robot
    Generate Transaction Reference    6
    Post Payment Escrow Charge     ${VALID_PAYMENT_PROJECT_ID}     {"paymentMethod":"DEBIT_CARD","currency":${currency},"paymentInfo":${credit_card},"amount":${amount},"extRefId":"${TRANSACTION_REFERENCE}","merchantRefId":"${merchant_id}","metaData":${meta_data}}
    Response Correct Code    ${BAD_REQUEST_CODE}
    Response Should Contain Property With Value    .message    error.validation
    Response Should Contain Property With Value    fieldErrors.code        payment_not_support
    Response Should Contain Property With Value    fieldErrors.message     Not support payment method
    Response Should Contain Property With Null Value    fieldErrors.id

TC_O2O_09640
    [Documentation]    [Payment][PaymentCharge] 2C2P charge with paymentMethod =CASH
    [Tags]    Medium    Regression    UnitTest    Robot
    Generate Transaction Reference    6
    Post Payment Escrow Charge     ${VALID_PAYMENT_PROJECT_ID}     {"paymentMethod":"CASH","currency":${currency},"paymentInfo":${credit_card},"amount":${amount},"extRefId":"${TRANSACTION_REFERENCE}","merchantRefId":"${merchant_id}","metaData":${meta_data}}
    Response Correct Code    ${BAD_REQUEST_CODE}
    Response Should Contain Property With Value    .message    error.validation
    Response Should Contain Property With Value    fieldErrors.code        payment_not_support
    Response Should Contain Property With Value    fieldErrors.message     Not support payment method
    Response Should Contain Property With Null Value    fieldErrors.id

TC_O2O_08719
    [Documentation]    [Payment][PaymentCharge] 2C2P Charge fail due to paymentMethod is Null
    [Tags]    Medium    Regression    UnitTest    Robot
    Generate Transaction Reference    6
    Post Payment Escrow Charge     ${VALID_PAYMENT_PROJECT_ID}     {"paymentMethod":null,"currency":${currency},"paymentInfo":${credit_card},"amount":${amount},"extRefId":"${TRANSACTION_REFERENCE}","merchantRefId":"${merchant_id}","metaData":${meta_data}}
    Response Correct Code    ${BAD_REQUEST_CODE}
    Response Should Contain Property With Value    .message    error.validation
    Response Should Contain Property With Value    fieldErrors..field        paymentMethod
    Response Should Contain Property With Value    fieldErrors..message      may not be empty

TC_O2O_08720
    [Documentation]    [Payment][PaymentCharge] 2C2P Charge fail due to paymentMethod is empty
    [Tags]    Medium    Regression    UnitTest    Robot
    Generate Transaction Reference    6
    Post Payment Escrow Charge     ${VALID_PAYMENT_PROJECT_ID}     {"paymentMethod":"","currency":${currency},"paymentInfo":${credit_card},"amount":${amount},"extRefId":"${TRANSACTION_REFERENCE}","merchantRefId":"${merchant_id}","metaData":${meta_data}}
    Response Correct Code    ${BAD_REQUEST_CODE}
    Response Should Contain Property With Value    .message    error.validation
    Response Should Contain Property With Value    fieldErrors..field        paymentMethod
    Response Should Contain Property With Value    fieldErrors..message      may not be empty

TC_O2O_09497
    [Documentation]    [Payment][PaymentCharge] 2C2P Charge fail due to paymentMethod is invalid
    [Tags]    Medium    Regression    UnitTest    Robot
    Generate Transaction Reference    6
    Post Payment Escrow Charge     ${VALID_PAYMENT_PROJECT_ID}     {"paymentMethod":"TRUEYOU_VISA_CARD","currency":${currency},"paymentInfo":${credit_card},"amount":${amount},"extRefId":"${TRANSACTION_REFERENCE}","merchantRefId":"${merchant_id}","metaData":${meta_data}}
    Response Correct Code    ${BAD_REQUEST_CODE}
    Response Should Contain Property With Value    .message    error.validation
    Response Should Contain Property With Value    fieldErrors..field        paymentMethod
    Response Should Contain Property With Value    fieldErrors..message      Payment Method is invalid.

TC_O2O_08721
    [Documentation]    [Payment][PaymentCharge] 2C2P Charge fail due to currency is not in ISO 4217 code
    [Tags]    Medium    Regression    UnitTest    Robot
    Generate Transaction Reference    6
    Post Payment Escrow Charge     ${VALID_PAYMENT_PROJECT_ID}     {"paymentMethod":${payment_method},"currency":"baht","paymentInfo":${credit_card},"amount":${amount},"extRefId":"${TRANSACTION_REFERENCE}","merchantRefId":"${merchant_id}","metaData":${meta_data}}
    Response Correct Code    ${BAD_REQUEST_CODE}
    Response Should Contain Property With Value    .message    error.validation
    Response Should Contain Property With Value    fieldErrors..field        currency
    Response Should Contain Property With Value    fieldErrors..message      Currency is invalid.

TC_O2O_08722
    [Documentation]    [Payment][PaymentCharge] 2C2P Charge fail due to currency is null
    [Tags]    Medium    Regression    UnitTest    Robot
    Generate Transaction Reference    6
    Post Payment Escrow Charge     ${VALID_PAYMENT_PROJECT_ID}     {"paymentMethod":${payment_method},"currency":null,"paymentInfo":${credit_card},"amount":${amount},"extRefId":"${TRANSACTION_REFERENCE}","merchantRefId":"${merchant_id}","metaData":${meta_data}}
    Response Correct Code    ${BAD_REQUEST_CODE}
    Response Should Contain Property With Value    .message    error.validation
    Response Should Contain Property With Value    fieldErrors..field        currency
    Response Should Contain Property With Value    fieldErrors..message      may not be empty

TC_O2O_08723
    [Documentation]    [Payment][PaymentCharge] 2C2P Charge fail due to paymentInfo is Null
    [Tags]    Medium    Regression    UnitTest    Robot
    Generate Transaction Reference    6
    Post Payment Escrow Charge     ${VALID_PAYMENT_PROJECT_ID}     {"paymentMethod":${payment_method},"currency":${currency},"paymentInfo":null,"amount":${amount},"extRefId":"${TRANSACTION_REFERENCE}","merchantRefId":"${merchant_id}","metaData":${meta_data}}
    Response Correct Code    ${BAD_REQUEST_CODE}
    Response Should Contain Property With Value    .message    error.validation
    Response Should Contain Property With Value    fieldErrors..field        paymentInfo
    Response Should Contain Property With Value    fieldErrors..message      may not be empty

TC_O2O_09470
    [Documentation]    [Payment][PaymentCharge] 2C2P Charge fail due to amount =0
    [Tags]    Medium    Regression    UnitTest    Robot
    Generate Transaction Reference    6
    Post Payment Escrow Charge     ${VALID_PAYMENT_PROJECT_ID}     {"paymentMethod":${payment_method},"currency":${currency},"paymentInfo":${credit_card},"amount":0,"extRefId":"${TRANSACTION_REFERENCE}","merchantRefId":"${merchant_id}","metaData":${meta_data}}
    Response Correct Code    ${BAD_REQUEST_CODE}
    Response Should Contain Property With Value    .title    Invalid Amount.
    Response Should Contain Property With Value    .message    error.validation
    Response Should Contain Property With Value    fieldErrors..code        2_99
    Response Should Contain Property With Value    fieldErrors..message     Unable to complete the request.

TC_O2O_08724
    [Documentation]    [Payment][PaymentCharge] 2C2P Charge fail due to amount is not positive value
    [Tags]    Medium    Regression    UnitTest    Robot
    Generate Transaction Reference    6
    Post Payment Escrow Charge     ${VALID_PAYMENT_PROJECT_ID}     {"paymentMethod":${payment_method},"currency":${currency},"paymentInfo":${credit_card},"amount":-90078,"extRefId":"${TRANSACTION_REFERENCE}","merchantRefId":"${merchant_id}","metaData":${meta_data}}
    Response Correct Code    ${BAD_REQUEST_CODE}
    Response Should Contain Property With Value    .message    error.validation
    Response Should Contain Property With Value    fieldErrors..field        amount
    Response Should Contain Property With Value    fieldErrors..message      Amount is invalid.

TC_O2O_08725
    [Documentation]    [Payment][PaymentCharge] 2C2P Charge fail due to amount is Null
    [Tags]    Medium    Regression    UnitTest    Robot
    Generate Transaction Reference    6
    Post Payment Escrow Charge     ${VALID_PAYMENT_PROJECT_ID}     {"paymentMethod":${payment_method},"currency":${currency},"paymentInfo":${credit_card},"amount":null,"extRefId":"${TRANSACTION_REFERENCE}","merchantRefId":"${merchant_id}","metaData":${meta_data}}
    Response Correct Code    ${BAD_REQUEST_CODE}
    Response Should Contain Property With Value    .message    error.validation
    Response Should Contain Property With Value    fieldErrors..field        amount
    Response Should Contain Property With Value    fieldErrors..message      may not be empty

TC_O2O_08726
    [Documentation]    [Payment][PaymentCharge] 2C2P Charge fail due to extRefId is Null.
    [Tags]    Medium    Regression    UnitTest    Robot
    Generate Transaction Reference    6
    Post Payment Escrow Charge     ${VALID_PAYMENT_PROJECT_ID}     {"paymentMethod":${payment_method},"currency":${currency},"paymentInfo":${credit_card},"amount":${amount},"extRefId":null,"merchantRefId":"${merchant_id}","metaData":${meta_data}}
    Response Correct Code    ${BAD_REQUEST_CODE}
    Response Should Contain Property With Value    .message    error.validation
    Response Should Contain Property With Value    fieldErrors..field        extRefId
    Response Should Contain Property With Value    fieldErrors..message      may not be empty

TC_O2O_09498
    [Documentation]    [Payment][PaymentCharge] 2C2P Charge fail due to extRefId is empty
    [Tags]    Medium    Regression    UnitTest    Robot
    Generate Transaction Reference    6
    Post Payment Escrow Charge     ${VALID_PAYMENT_PROJECT_ID}     {"paymentMethod":${payment_method},"currency":${currency},"paymentInfo":${credit_card},"amount":${amount},"extRefId":"","merchantRefId":"${merchant_id}","metaData":${meta_data}}
    Response Correct Code    ${BAD_REQUEST_CODE}
    Response Should Contain Property With Value    .message    error.validation
    Response Should Contain Property With Value    fieldErrors..field        extRefId
    Response Should Contain Property With Value    fieldErrors..message      may not be empty

TC_O2O_08727
    [Documentation]    [Payment][PaymentCharge] 2C2P Charge fail due to merchantRefId is Null
    [Tags]    Medium    Regression    UnitTest    Robot
    Generate Transaction Reference    6
    Post Payment Escrow Charge     ${VALID_PAYMENT_PROJECT_ID}     {"paymentMethod":${payment_method},"currency":${currency},"paymentInfo":${credit_card},"amount":${amount},"extRefId":"${TRANSACTION_REFERENCE}","merchantRefId":null,"metaData":${meta_data}}
    Response Correct Code    ${BAD_REQUEST_CODE}
    Response Should Contain Property With Value    .message    error.validation
    Response Should Contain Property With Value    fieldErrors..field        merchantRefId
    Response Should Contain Property With Value    fieldErrors..message      may not be empty

TC_O2O_09499
    [Documentation]    [Payment][PaymentCharge] 2C2P Charge fail due to merchantRefId is empty
    [Tags]    Medium    Regression    UnitTest    Robot
    Generate Transaction Reference    6
    Post Payment Escrow Charge     ${VALID_PAYMENT_PROJECT_ID}     {"paymentMethod":${payment_method},"currency":${currency},"paymentInfo":${credit_card},"amount":${amount},"extRefId":"${TRANSACTION_REFERENCE}","merchantRefId":"","metaData":${meta_data}}
    Response Correct Code    ${BAD_REQUEST_CODE}
    Response Should Contain Property With Value    .message    error.validation
    Response Should Contain Property With Value    fieldErrors..field        merchantRefId
    Response Should Contain Property With Value    fieldErrors..message      may not be empty

TC_O2O_08729
    [Documentation]    [Payment][PaymentCharge] 2C2P Charge fail due to paymentInfo is invalid
    [Tags]    Medium    Regression    UnitTest    Robot
    Generate Transaction Reference    6
    Post Payment Escrow Charge     ${VALID_PAYMENT_PROJECT_ID}     {"paymentMethod":${payment_method},"currency":${currency},"paymentInfo":"bG53emEwMDc=","amount":${amount},"extRefId":"${TRANSACTION_REFERENCE}","merchantRefId":"${merchant_id}","metaData":${meta_data}}
    Response Correct Code    ${BAD_REQUEST_CODE}
    Response Should Contain Property With Value    .title      The length of 'pan' field does not match.
    Response Should Contain Property With Value    .message    error.validation
    Response Should Contain Property With Value    fieldErrors..code        2_99
    Response Should Contain Property With Value    fieldErrors..message     Unable to complete the request.

TC_O2O_09473
    [Documentation]    [Payment][PaymentCharge] 2C2P Charge fail due to dupplicate request
    [Tags]    Medium    Regression    UnitTest    Robot
    Post Payment Escrow Charge     ${VALID_PAYMENT_PROJECT_ID}     {"paymentMethod":${payment_method},"currency":${currency},"paymentInfo":${credit_card},"amount":${amount},"extRefId":"dupp001","merchantRefId":"${merchant_id}","metaData":${meta_data}}
    Response Correct Code    ${BAD_REQUEST_CODE}
    Response Should Contain Property With Value    .title      Duplicate External Transaction
    Response Should Contain Property With Value    .message    error.validation
    Response Should Contain Property With Value    fieldErrors..code        2_duplicate_transaction
    Response Should Contain Property With Value    fieldErrors..message     Duplicate External Transaction

TC_O2O_09878
    [Documentation]    [Payment][PaymentCharge] 2C2P charge fail due to project_code >4 digit
    [Tags]    Medium    Regression    Smoke    Robot
    Generate Transaction Reference    6
    Post Payment Escrow Charge     ${OVER_LIMIT_PAYMENT_PROJECT_ID}     {"paymentMethod":${payment_method},"currency":${currency},"paymentInfo":${credit_card},"amount":${amount},"extRefId":"${TRANSACTION_REFERENCE}","merchantRefId":"${merchant_id}","metaData":${meta_data}}
    Response Correct Code    ${BAD_REQUEST_CODE}
    Response Should Contain Property With Value    fieldErrors..code       2_99
    Response Should Contain Property With Value    fieldErrors..message    Unable to complete the request.

TC_O2O_09879
    [Documentation]    [Payment][PaymentCharge] 2C2P charge fail due to invalid project_id
    [Tags]    High    Smoke    UnitTest    Regression    Robot
    Generate Transaction Reference    6
    Post Payment Escrow Charge     ${INVALID_PAYMENT_PROJECT_ID}     {"paymentMethod":${payment_method},"currency":${currency},"paymentInfo":${credit_card},"amount":${amount},"extRefId":"${TRANSACTION_REFERENCE}","merchantRefId":"${merchant_id}","metaData":${meta_data}}
    Response Correct Code    ${NOT_FOUND_CODE}
    Response Should Contain Property With Value    fieldErrors..code       3_not_found
    Response Should Contain Property With Value    fieldErrors..message    Not Found

TC_O2O_10298
    [Documentation]    [Payment][PaymentCharge] verify response when access with invalid client scope on weomni gateway
    [Tags]    Medium    Smoke    Regression    Robot
    [Setup]   Generate Gateway Header With Scope and Permission    ${ROLE_USER}    ${ROLE_USER_PASSWORD}    payment.payment.read
    Generate Transaction Reference    6
    Post Payment Escrow Charge     ${VALID_PAYMENT_PROJECT_ID}     {"paymentMethod":${payment_method},"currency":${currency},"paymentInfo":${credit_card},"amount":${amount},"extRefId":"${TRANSACTION_REFERENCE}","merchantRefId":"${merchant_id}","metaData":${meta_data}}
    Response Correct Code    ${FORBIDDEN_CODE}
    Response Should Contain Property With Value    .detail          Access is denied
