*** Settings ***
Documentation    Tests to verify that payment inquiry api with 2C2P works correcty
Resource    ../../../resources/init.robot
Resource    ../../../keywords/payment/payment_escrow_resource_keywords.robot
Test Setup        Generate Gateway Header With Scope and Permission    ${ROLE_USER}    ${ROLE_USER_PASSWORD}    payment.payment.read
Test Teardown     Run Keywords    Delete Created Client And User Group    AND    Delete All Sessions

*** Test Cases ***
TC_O2O_09259
    [Documentation]    [Payment][PaymentRefund] 2C2P inquiry fail due to id is invalid
    [Tags]    Medium    Regression    Smoke    UnitTest    Robot
    Get Payment Escrow Query   ${VALID_PAYMENT_PROJECT_ID}   ${invalid_id}
    Response Correct Code    ${BAD_REQUEST_CODE}
    Response Should Contain Property With Value    .message    error.validation
    Response Should Contain Property With Value    fieldErrors..field      transaction
    Response Should Contain Property With Value    fieldErrors..message    Transaction is not found.

TC_O2O_10302
    [Documentation]    [Payment][PaymentRefund] verify response when access with invalid client scope on weomni gateway
    [Tags]    Medium    Smoke    Regression    Robot
    [Setup]   Generate Gateway Header With Scope and Permission    ${ROLE_USER}    ${ROLE_USER_PASSWORD}    payment.payment.write
    Get Payment Escrow Query   ${VALID_PAYMENT_PROJECT_ID}   ${invalid_id}
    Response Correct Code    ${FORBIDDEN_CODE}
    Response Should Contain Property With Value    .detail    Access is denied
