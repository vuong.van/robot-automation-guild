*** Settings ***
Documentation    Tests to verify that payment transaction search api by id with 2C2P works correcty
Resource    ../../../resources/init.robot
Resource    ../../../keywords/payment/payment_escrow_resource_keywords.robot
Resource    ../../../keywords/payment_transaction/payment_transaction_resource_keywords.robot
Test Setup        Generate Gateway Header With Scope and Permission    ${ROLE_USER}    ${ROLE_USER_PASSWORD}    payment.payment.write,payment.escrow.r
Test Teardown     Run Keywords    Delete Created Client And User Group    AND    Delete All Sessions

*** Test Cases ***
TC_O2O_10622
    [Documentation]    [Payment][PaymentTransaction] can not get transaction detail due to invalid client scope
    [Tags]    Medium    Regression    Smoke    Robot
    [Setup]   Generate Gateway Header With Scope and Permission    ${ROLE_USER}    ${ROLE_USER_PASSWORD}    payment.escrow.l
    Get Search Payment Escrow Transaction By Id    ${VALID_PAYMENT_PROJECT_ID}    ${invalid_id}
    Response Correct Code    ${FORBIDDEN_CODE}
    Response Should Contain Property With Value    .detail          Access is denied

TC_O2O_10623
    [Documentation]    [Payment][PaymentTransaction] can get transaction when send charge id (without Query Params)
    [Tags]    High    Regression    Smoke    Sanity    Robot
    Generate Transaction Reference    6
    Post Payment Escrow Charge     ${VALID_PAYMENT_PROJECT_ID}    {"paymentMethod":${payment_method},"currency":${currency},"paymentInfo":${credit_card},"amount":${amount},"extRefId":"${TRANSACTION_REFERENCE}","merchantRefId":"${merchant_id}"}
    Get Transaction ID
    Get Search Payment Escrow Transaction By Id    ${VALID_PAYMENT_PROJECT_ID}    ${transaction_id}
    Response Correct Code    ${SUCCESS_CODE}
    Response Should Contain Property With Value    .paymentGatewayType    2C2P
    Response Should Contain Property With Value    .transactionType    ESCROW
    Response Should Contain Property With Value    .paymentMethod    CREDIT_CARD
    Response Should Contain Property With Value    .status    SUCCESS
    Response Should Contain Property With Value    .action    CHARGE
    Response Should Not Contain Property           .details

TC_O2O_10624
    [Documentation]    [Payment][PaymentTransaction] can get transaction when send charge id (detail =false)
    [Tags]    High    Regression    Smoke    Sanity    Robot
    Generate Transaction Reference    6
    Post Payment Escrow Charge     ${VALID_PAYMENT_PROJECT_ID}    {"paymentMethod":${payment_method},"currency":${currency},"paymentInfo":${credit_card},"amount":${amount},"extRefId":"${TRANSACTION_REFERENCE}","merchantRefId":"${merchant_id}"}
    Get Transaction ID
    Get Search Payment Escrow Transaction By Id    ${VALID_PAYMENT_PROJECT_ID}    ${transaction_id}    detail=false
    Response Correct Code    ${SUCCESS_CODE}
    Response Should Contain Property With Value    .paymentGatewayType    2C2P
    Response Should Contain Property With Value    .transactionType    ESCROW
    Response Should Contain Property With Value    .paymentMethod    CREDIT_CARD
    Response Should Contain Property With Value    .status            SUCCESS
    Response Should Contain Property With Value    .action            CHARGE
    Response Should Not Contain Property           .details

TC_O2O_10625
    [Documentation]    [Payment][PaymentTransaction] can get transaction when send charge id (detail =true) without void/refund transaction
    [Tags]    Medium    Regression    Smoke    Robot
    Generate Transaction Reference    6
    Post Payment Escrow Charge     ${VALID_PAYMENT_PROJECT_ID}    {"paymentMethod":${payment_method},"currency":${currency},"paymentInfo":${credit_card},"amount":${amount},"extRefId":"${TRANSACTION_REFERENCE}","merchantRefId":"${merchant_id}"}
    Get Transaction ID
    Get Search Payment Escrow Transaction By Id    ${VALID_PAYMENT_PROJECT_ID}    ${transaction_id}    detail=true
    Response Correct Code    ${SUCCESS_CODE}
    Response Should Contain Property With Value    .paymentGatewayType    2C2P
    Response Should Contain Property With Value    .transactionType    ESCROW
    Response Should Contain Property With Value    .paymentMethod    CREDIT_CARD
    Response Should Contain Property With Value    .status            SUCCESS
    Response Should Contain Property With Value    .action            CHARGE
    Response Should Contain Property With Value    details..status    APPROVED
    Response Should Contain Property With Empty Value    .voided
    Response Should Contain Property With Empty Value    .refunds

TC_O2O_10626
    [Documentation]    [Payment][PaymentTransaction] can get transaction when send charge id (detail =true) has success void transaction
    [Tags]    Medium    Regression    Smoke    Robot
    Generate Transaction Reference    6
    Post Payment Escrow Charge     ${VALID_PAYMENT_PROJECT_ID}    {"paymentMethod":${payment_method},"currency":${currency},"paymentInfo":${credit_card},"amount":${amount},"extRefId":"${TRANSACTION_REFERENCE}","merchantRefId":"${merchant_id}"}
    Get Transaction ID
    Post Payment Escrow Refund   ${VALID_PAYMENT_PROJECT_ID}   ${transaction_id}     {"amount" :${amount},"voided": true}
    Get Search Payment Escrow Transaction By Id    ${VALID_PAYMENT_PROJECT_ID}    ${transaction_id}    detail=true
    Response Correct Code    ${SUCCESS_CODE}
    Response Should Contain Property With Value    .paymentGatewayType    2C2P
    Response Should Contain Property With Value    .transactionType    ESCROW
    Response Should Contain Property With Value    .paymentMethod    CREDIT_CARD
    Response Should Contain Property With Value    .status            SUCCESS
    Response Should Contain Property With Value    .action            CHARGE
    Response Should Contain Property With Value    details..status    VOIDED
    Response Should Contain Property With Value    details..voided..paymentGatewayType    2C2P
    Response Should Contain Property With Value    details..voided..transactionType    ESCROW
    Response Should Contain Property With Value    details..voided..paymentMethod    CREDIT_CARD
    Response Should Contain Property With Value    details..voided..status    SUCCESS
    Response Should Contain Property With Value    details..voided..action    VOID
    Response Should Contain Property With Empty Value    .refunds

TC_O2O_10629
    [Documentation]    [Payment][PaymentTransaction] can get transaction when send charge id (detail =true) have fail and success void transaction
    [Tags]    Medium    Regression    Smoke    Robot
    Generate Transaction Reference    6
    Post Payment Escrow Charge     ${VALID_PAYMENT_PROJECT_ID}    {"paymentMethod":${payment_method},"currency":${currency},"paymentInfo":${credit_card},"amount":${amount},"extRefId":"${TRANSACTION_REFERENCE}","merchantRefId":"${merchant_id}"}
    Get Transaction ID
    Post Payment Escrow Refund   ${VALID_PAYMENT_PROJECT_ID}   ${transaction_id}     {"amount" :${amount},"voided": true}
    Post Payment Escrow Refund   ${VALID_PAYMENT_PROJECT_ID}   ${transaction_id}     {"amount" :${amount},"voided": true}
    Get Search Payment Escrow Transaction By Id    ${VALID_PAYMENT_PROJECT_ID}    ${transaction_id}    detail=true
    Response Correct Code    ${SUCCESS_CODE}
    Response Should Contain Property With Value    .paymentGatewayType    2C2P
    Response Should Contain Property With Value    .transactionType    ESCROW
    Response Should Contain Property With Value    .paymentMethod    CREDIT_CARD
    Response Should Contain Property With Value    .status            SUCCESS
    Response Should Contain Property With Value    .action            CHARGE
    Response Should Contain Property With Value    details..status    VOIDED
    Response Should Contain Property With Value    details..voided[0]..paymentGatewayType    2C2P
    Response Should Contain Property With Value    details..voided[0]..transactionType    ESCROW
    Response Should Contain Property With Value    details..voided[0]..paymentMethod    CREDIT_CARD
    Response Should Contain Property With Value    details..voided[0]..status    FAIL
    Response Should Contain Property With Value    details..voided[0]..action    VOID
    Response Should Contain Property With Value    details..voided[1]..paymentGatewayType    2C2P
    Response Should Contain Property With Value    details..voided[1]..transactionType    ESCROW
    Response Should Contain Property With Value    details..voided[1]..paymentMethod    CREDIT_CARD
    Response Should Contain Property With Value    details..voided[1]..status    SUCCESS
    Response Should Contain Property With Value    details..voided[1]..action    VOID
    Response Should Contain Property With Empty Value    .refunds

TC_O2O_10632
    [Documentation]    [Payment][PaymentTransaction] can get transaction when send charge id (detail =true) has fail refund transaction
    [Tags]    Medium    Regression    Smoke    Robot
    Generate Transaction Reference    6
    Post Payment Escrow Charge     ${VALID_PAYMENT_PROJECT_ID}    {"paymentMethod":${payment_method},"currency":${currency},"paymentInfo":${credit_card},"amount":${amount},"extRefId":"${TRANSACTION_REFERENCE}","merchantRefId":"${merchant_id}"}
    Get Transaction ID
    Post Payment Escrow Refund   ${VALID_PAYMENT_PROJECT_ID}   ${transaction_id}     {"amount" :${amount},"voided": false}
    Get Search Payment Escrow Transaction By Id    ${VALID_PAYMENT_PROJECT_ID}    ${transaction_id}    detail=true
    Response Correct Code    ${SUCCESS_CODE}
    Response Should Contain Property With Value    .paymentGatewayType    2C2P
    Response Should Contain Property With Value    .transactionType    ESCROW
    Response Should Contain Property With Value    .paymentMethod    CREDIT_CARD
    Response Should Contain Property With Value    .status            SUCCESS
    Response Should Contain Property With Value    .action            CHARGE
    Response Should Contain Property With Value    .details..status    APPROVED
    Response Should Contain Property With Empty Value    .voided
    Response Should Contain Property With Value    details..refunds..paymentGatewayType    2C2P
    Response Should Contain Property With Value    details..refunds..transactionType    ESCROW
    Response Should Contain Property With Value    details..refunds..paymentMethod    CREDIT_CARD
    Response Should Contain Property With Value    details..refunds..status    FAIL
    Response Should Contain Property With Value    details..refunds..action    REFUND

TC_O2O_10636
    [Documentation]    [Payment][PaymentTransaction] can get transaction when send void id (detail =true) and id.void transaction is success
    [Tags]    Medium    Regression    Smoke    Robot
    Generate Transaction Reference    6
    Post Payment Escrow Charge     ${VALID_PAYMENT_PROJECT_ID}    {"paymentMethod":${payment_method},"currency":${currency},"paymentInfo":${credit_card},"amount":${amount},"extRefId":"${TRANSACTION_REFERENCE}","merchantRefId":"${merchant_id}"}
    Get Transaction ID
    Post Payment Escrow Refund   ${VALID_PAYMENT_PROJECT_ID}   ${transaction_id}     {"amount" :${amount},"voided": true}
    Get Transaction ID
    Get Search Payment Escrow Transaction By Id    ${VALID_PAYMENT_PROJECT_ID}    ${transaction_id}    detail=true
    Response Correct Code    ${SUCCESS_CODE}
    Response Should Contain Property With Value    .paymentGatewayType    2C2P
    Response Should Contain Property With Value    .transactionType    ESCROW
    Response Should Contain Property With Value    .paymentMethod    CREDIT_CARD
    Response Should Contain Property With Value    .status            SUCCESS
    Response Should Contain Property With Value    .action            VOID
    Response Should Contain Property With Value    details..status    VOIDED
    Response Should Contain Property With Value    details..voided..paymentGatewayType    2C2P
    Response Should Contain Property With Value    details..voided..transactionType    ESCROW
    Response Should Contain Property With Value    details..voided..paymentMethod    CREDIT_CARD
    Response Should Contain Property With Value    details..voided..status    SUCCESS
    Response Should Contain Property With Value    details..voided..action    VOID
    Response Should Contain Property With Empty Value    .refunds

TC_O2O_10638
    [Documentation]    [Payment][PaymentTransaction] can get transaction when send void id (detail =true) have void transaction is fail and success
    [Tags]    Medium    Regression    Smoke    Robot
    Generate Transaction Reference    6
    Post Payment Escrow Charge     ${VALID_PAYMENT_PROJECT_ID}    {"paymentMethod":${payment_method},"currency":${currency},"paymentInfo":${credit_card},"amount":${amount},"extRefId":"${TRANSACTION_REFERENCE}","merchantRefId":"${merchant_id}"}
    Get Transaction ID
    Post Payment Escrow Refund   ${VALID_PAYMENT_PROJECT_ID}   ${transaction_id}     {"amount" :${amount},"voided": true}
    Post Payment Escrow Refund   ${VALID_PAYMENT_PROJECT_ID}   ${transaction_id}     {"amount" :${amount},"voided": true}
    Get Transaction ID
    Get Search Payment Escrow Transaction By Id    ${VALID_PAYMENT_PROJECT_ID}    ${transaction_id}    detail=true
    Response Correct Code    ${SUCCESS_CODE}
    Response Should Contain Property With Value    .paymentGatewayType    2C2P
    Response Should Contain Property With Value    .transactionType    ESCROW
    Response Should Contain Property With Value    .paymentMethod    CREDIT_CARD
    Response Should Contain Property With Value    .status            FAIL
    Response Should Contain Property With Value    .action            VOID
    Response Should Contain Property With Value    details..status    VOIDED
    Response Should Contain Property With Value    details..voided[0]..paymentGatewayType    2C2P
    Response Should Contain Property With Value    details..voided[0]..transactionType    ESCROW
    Response Should Contain Property With Value    details..voided[0]..paymentMethod    CREDIT_CARD
    Response Should Contain Property With Value    details..voided[0]..status    FAIL
    Response Should Contain Property With Value    details..voided[0]..action    VOID
    Response Should Contain Property With Value    details..voided[1]..paymentGatewayType    2C2P
    Response Should Contain Property With Value    details..voided[1]..transactionType    ESCROW
    Response Should Contain Property With Value    details..voided[1]..paymentMethod    CREDIT_CARD
    Response Should Contain Property With Value    details..voided[1]..status    SUCCESS
    Response Should Contain Property With Value    details..voided[1]..action    VOID
    Response Should Contain Property With Empty Value    .refunds

TC_O2O_10641
    [Documentation]    [Payment][PaymentTransaction] can get transaction when send void id (detail =true) have success void and refund transaction is fail
    [Tags]    Medium    Regression    Smoke    Robot
    Generate Transaction Reference    6
    Post Payment Escrow Charge     ${VALID_PAYMENT_PROJECT_ID}    {"paymentMethod":${payment_method},"currency":${currency},"paymentInfo":${credit_card},"amount":${amount},"extRefId":"${TRANSACTION_REFERENCE}","merchantRefId":"${merchant_id}"}
    Get Transaction ID
    Post Payment Escrow Refund   ${VALID_PAYMENT_PROJECT_ID}   ${transaction_id}     {"amount" :${amount},"voided": false}
    Post Payment Escrow Refund   ${VALID_PAYMENT_PROJECT_ID}   ${transaction_id}     {"amount" :${amount},"voided": true}
    Get Transaction ID
    Get Search Payment Escrow Transaction By Id    ${VALID_PAYMENT_PROJECT_ID}    ${transaction_id}    detail=true
    Response Correct Code    ${SUCCESS_CODE}
    Response Should Contain Property With Value    .paymentGatewayType    2C2P
    Response Should Contain Property With Value    .transactionType    ESCROW
    Response Should Contain Property With Value    .paymentMethod    CREDIT_CARD
    Response Should Contain Property With Value    .status            SUCCESS
    Response Should Contain Property With Value    .action            VOID
    Response Should Contain Property With Value    details..status    VOIDED
    Response Should Contain Property With Value    details..voided..paymentGatewayType    2C2P
    Response Should Contain Property With Value    details..voided..transactionType    ESCROW
    Response Should Contain Property With Value    details..voided..paymentMethod    CREDIT_CARD
    Response Should Contain Property With Value    details..voided..status    SUCCESS
    Response Should Contain Property With Value    details..voided..action    VOID
    Response Should Contain Property With Value    details..refunds..paymentGatewayType    2C2P
    Response Should Contain Property With Value    details..refunds..transactionType    ESCROW
    Response Should Contain Property With Value    details..refunds..paymentMethod    CREDIT_CARD
    Response Should Contain Property With Value    details..refunds..status    FAIL
    Response Should Contain Property With Value    details..refunds..action    REFUND

TC_O2O_10644
    [Documentation]    [Payment][PaymentTransaction] can get transaction when send refund id (detail =true) have fail refund transaction
    [Tags]    Medium    Regression    Smoke    Robot
    Generate Transaction Reference    6
    Post Payment Escrow Charge     ${VALID_PAYMENT_PROJECT_ID}    {"paymentMethod":${payment_method},"currency":${currency},"paymentInfo":${credit_card},"amount":${amount},"extRefId":"${TRANSACTION_REFERENCE}","merchantRefId":"${merchant_id}"}
    Get Transaction ID
    Post Payment Escrow Refund   ${VALID_PAYMENT_PROJECT_ID}   ${transaction_id}     {"amount" :${amount},"voided": false}
    Get Transaction ID
    Get Search Payment Escrow Transaction By Id    ${VALID_PAYMENT_PROJECT_ID}    ${transaction_id}    detail=true
    Response Correct Code    ${SUCCESS_CODE}
    Response Should Contain Property With Value    .paymentGatewayType    2C2P
    Response Should Contain Property With Value    .transactionType    ESCROW
    Response Should Contain Property With Value    .paymentMethod    CREDIT_CARD
    Response Should Contain Property With Value    .status            FAIL
    Response Should Contain Property With Value    .action            REFUND
    Response Should Contain Property With Value    details..status    APPROVED
    Response Should Contain Property With Empty Value    .voided
    Response Should Contain Property With Value    details..refunds..paymentGatewayType    2C2P
    Response Should Contain Property With Value    details..refunds..transactionType    ESCROW
    Response Should Contain Property With Value    details..refunds..paymentMethod    CREDIT_CARD
    Response Should Contain Property With Value    details..refunds..status    FAIL
    Response Should Contain Property With Value    details..refunds..action    REFUND

TC_O2O_10647
    [Documentation]    [Payment][PaymentTransaction] can get transaction when send refund id (detail =true) have success void and fail refund transaction
    [Tags]    High    Regression    Smoke    Sanity    Robot
    Generate Transaction Reference    6
    Post Payment Escrow Charge     ${VALID_PAYMENT_PROJECT_ID}    {"paymentMethod":${payment_method},"currency":${currency},"paymentInfo":${credit_card},"amount":${amount},"extRefId":"${TRANSACTION_REFERENCE}","merchantRefId":"${merchant_id}"}
    Get Transaction ID
    Post Payment Escrow Refund   ${VALID_PAYMENT_PROJECT_ID}   ${transaction_id}     {"amount" :${amount},"voided": true}
    Post Payment Escrow Refund   ${VALID_PAYMENT_PROJECT_ID}   ${transaction_id}     {"amount" :${amount},"voided": false}
    Get Transaction ID
    Get Search Payment Escrow Transaction By Id    ${VALID_PAYMENT_PROJECT_ID}    ${transaction_id}    detail=true
    Response Correct Code    ${SUCCESS_CODE}
    Response Should Contain Property With Value    .paymentGatewayType    2C2P
    Response Should Contain Property With Value    .transactionType    ESCROW
    Response Should Contain Property With Value    .paymentMethod    CREDIT_CARD
    Response Should Contain Property With Value    .status            FAIL
    Response Should Contain Property With Value    .action            REFUND
    Response Should Contain Property With Value    details..status    VOIDED
    Response Should Contain Property With Value    details..voided..paymentGatewayType    2C2P
    Response Should Contain Property With Value    details..voided..transactionType    ESCROW
    Response Should Contain Property With Value    details..voided..paymentMethod    CREDIT_CARD
    Response Should Contain Property With Value    details..voided..status    SUCCESS
    Response Should Contain Property With Value    details..voided..action    VOID
    Response Should Contain Property With Value    details..refunds..paymentGatewayType    2C2P
    Response Should Contain Property With Value    details..refunds..transactionType    ESCROW
    Response Should Contain Property With Value    details..refunds..paymentMethod    CREDIT_CARD
    Response Should Contain Property With Value    details..refunds..status    FAIL
    Response Should Contain Property With Value    details..refunds..action    REFUND

TC_O2O_10648
    [Documentation]    [Payment][PaymentTransaction] can not get transaction due to invalid id
    [Tags]    Medium    Regression    Smoke    Robot
    Get Search Payment Escrow Transaction By Id    ${VALID_PAYMENT_PROJECT_ID}    ${invalid_id}    detail=true
    Response Correct Code    ${BAD_REQUEST_CODE}
    Response Should Contain Property With Value    .title    Transaction Not Found
    Response Should Contain Property With Value    .message    error.validation
    Response Should Contain Property With Value    fieldErrors..field    transaction
    Response Should Contain Property With Value    fieldErrors..message    Transaction is not found.

TC_O2O_10649
    [Documentation]    [Payment][PaymentTransaction] can not get transaction due to invalid Query Params
    [Tags]    Medium    Regression    Robot
    Get Search Payment Escrow Transaction By Id    ${VALID_PAYMENT_PROJECT_ID}    ${invalid_id}    detail=TRUES
    Response Correct Code    ${BAD_REQUEST_CODE}
    Response Should Contain Property With Value    .title    Bad Request
    Response Should Contain Property With Value    detail    Failed to convert value of type 'java.lang.String' to required type 'boolean'; nested exception is java.lang.IllegalArgumentException: Invalid boolean value [TRUES]
