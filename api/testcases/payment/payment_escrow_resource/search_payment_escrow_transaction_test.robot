*** Settings ***
Documentation    Tests to verify that payment search transaction api with 2C2P works correcty
Resource    ../../../resources/init.robot
Resource    ../../../keywords/payment/payment_escrow_resource_keywords.robot
Test Setup        Generate Gateway Header With Scope and Permission    ${ROLE_USER}    ${ROLE_USER_PASSWORD}    payment.escrow.l
Test Teardown     Run Keywords    Delete Created Client And User Group    AND    Delete All Sessions

*** Test Cases ***
TC_O2O_10249
    [Documentation]    [Payment][PaymentTransaction] can get transaction list by project id only
    [Tags]    High    Regression    Smoke    UnitTest    Sanity    Robot
    Get Search Payment Escrow Transaction   ${VALID_PAYMENT_PROJECT_ID}
    Response Correct Code    ${SUCCESS_CODE}
    Response Should Contain Property With Value    $..projectId    ${VALID_PAYMENT_PROJECT_ID}

TC_O2O_10251
    [Documentation]    [Payment][PaymentTransaction] can get transaction list by valid project id and valid merchant_id
    [Tags]    High    Regression    Smoke    UnitTest    Robot
    Get Search Payment Escrow Transaction   ${VALID_PAYMENT_PROJECT_ID}   merchantId.in=${merchant_id}
    Response Correct Code    ${SUCCESS_CODE}
    Response Should Have Number Of Records    20    $..merchantId
    Get Search Payment Escrow Transaction   ${VALID_PAYMENT_PROJECT_ID}   merchantId.equals=${merchant_id}
    Response Correct Code    ${SUCCESS_CODE}
    Response Should Have Number Of Records    20    $..merchantId
    Get Search Payment Escrow Transaction   ${VALID_PAYMENT_PROJECT_ID}   merchantId.specified=true
    Response Correct Code    ${SUCCESS_CODE}
    Response Should Have Number Of Records    20    $..merchantId
    Get Search Payment Escrow Transaction   ${VALID_PAYMENT_PROJECT_ID}   merchantId.specified=false
    Response Correct Code    ${SUCCESS_CODE}
    Response Should Not Contain Property    $..merchantId
    Get Search Payment Escrow Transaction   ${VALID_PAYMENT_PROJECT_ID}   merchantId.contains=${merchant_id}
    Response Correct Code    ${SUCCESS_CODE}
    Response Should Have Number Of Records    20    $..merchantId

TC_O2O_10253
    [Documentation]    [Payment][PaymentTransaction] can get transaction list by valid project id and valid currency
    [Tags]    High    Regression    Smoke    UnitTest    Robot
    Get Search Payment Escrow Transaction   ${VALID_PAYMENT_PROJECT_ID}   currency.equals=THB
    Response Correct Code    ${SUCCESS_CODE}
    Response Should Have Number Of Records    20    $..currency
    Get Search Payment Escrow Transaction   ${VALID_PAYMENT_PROJECT_ID}   currency.specified=true
    Response Correct Code    ${SUCCESS_CODE}
    Response Should Have Number Of Records    20    $..currency
    Get Search Payment Escrow Transaction   ${VALID_PAYMENT_PROJECT_ID}   currency.specified=false
    Response Correct Code    ${SUCCESS_CODE}
    Response Should Not Contain Property    $..currency
    Get Search Payment Escrow Transaction   ${VALID_PAYMENT_PROJECT_ID}   currency.in=THB
    Response Correct Code    ${SUCCESS_CODE}
    Response Should Have Number Of Records    20    $..currency

TC_O2O_10254
    [Documentation]    [Payment][PaymentTransaction] can get transaction list by valid project id and valid amount
    [Tags]    High    Regression    Smoke    UnitTest    Robot
    Get Search Payment Escrow Transaction   ${VALID_PAYMENT_PROJECT_ID}   amount.in=102
    Response Correct Code    ${SUCCESS_CODE}
    Response Should Have Number Of Records    20    $..amount
    Get Search Payment Escrow Transaction   ${VALID_PAYMENT_PROJECT_ID}   amount.equals=102
    Response Correct Code    ${SUCCESS_CODE}
    Response Should Have Number Of Records    20    $..amount
    Get Search Payment Escrow Transaction   ${VALID_PAYMENT_PROJECT_ID}   amount.specified=true
    Response Correct Code    ${SUCCESS_CODE}
    Response Should Have Number Of Records    20    $..amount
    Get Search Payment Escrow Transaction   ${VALID_PAYMENT_PROJECT_ID}   amount.specified=false
    Response Correct Code    ${SUCCESS_CODE}
    Response Should Not Contain Property    $..amount
    Get Search Payment Escrow Transaction   ${VALID_PAYMENT_PROJECT_ID}   amount.greaterThan=0
    Response Correct Code    ${SUCCESS_CODE}
    Response Should Have Number Of Records    20    $..amount
    Get Search Payment Escrow Transaction   ${VALID_PAYMENT_PROJECT_ID}   amount.lessThan=200
    Response Correct Code    ${SUCCESS_CODE}
    Response Should Have Number Of Records    20    $..amount
    Get Search Payment Escrow Transaction   ${VALID_PAYMENT_PROJECT_ID}   amount.greaterOrEqualThan=102
    Response Correct Code    ${SUCCESS_CODE}
    Response Should Have Number Of Records    20    $..amount
    Get Search Payment Escrow Transaction   ${VALID_PAYMENT_PROJECT_ID}   amount.lessOrEqualThan=102
    Response Correct Code    ${SUCCESS_CODE}
    Response Should Have Number Of Records    20    $..amount

TC_O2O_10255
     [Documentation]    [Payment][PaymentTransaction] can get transaction list by valid project id and valid external_transaction_ref_id
     [Tags]    High    Regression    Smoke    UnitTest    Sanity    Robot
     Get Search Payment Escrow Transaction   ${VALID_PAYMENT_PROJECT_ID}   externalTransactionRefId.in=dupp001
     Response Correct Code    ${SUCCESS_CODE}
     Response Should Have Number Of Records    1    $..externalTransactionRefId
     Get Search Payment Escrow Transaction   ${VALID_PAYMENT_PROJECT_ID}   externalTransactionRefId.equals=dupp001
     Response Correct Code    ${SUCCESS_CODE}
     Response Should Have Number Of Records    1    $..externalTransactionRefId
     Get Search Payment Escrow Transaction   ${VALID_PAYMENT_PROJECT_ID}   externalTransactionRefId.specified=true
     Response Correct Code    ${SUCCESS_CODE}
     Response Should Have Number Of Records    20    $..externalTransactionRefId
     Get Search Payment Escrow Transaction   ${VALID_PAYMENT_PROJECT_ID}   externalTransactionRefId.specified=false
     Response Correct Code    ${SUCCESS_CODE}
     Get Search Payment Escrow Transaction   ${VALID_PAYMENT_PROJECT_ID}   externalTransactionRefId.contains=dupp00
     Response Correct Code    ${SUCCESS_CODE}
     Response Should Have Number Of Records    1    $..externalTransactionRefId

TC_O2O_10257
    [Documentation]    [Payment][PaymentTransaction] can get transaction list by valid project id and valid status
    [Tags]    High    Regression    Smoke    UnitTest    Robot
    Get Search Payment Escrow Transaction   ${VALID_PAYMENT_PROJECT_ID}   status.in=FAIL,SUCCESS
    Response Correct Code    ${SUCCESS_CODE}
    Response Should Have Number Of Records    20    $..currency
    Get Search Payment Escrow Transaction   ${VALID_PAYMENT_PROJECT_ID}   status.equals=SUCCESS
    Response Correct Code    ${SUCCESS_CODE}
    Response Should Have Number Of Records    20    $..currency
    Get Search Payment Escrow Transaction   ${VALID_PAYMENT_PROJECT_ID}   status.specified=true
    Response Correct Code    ${SUCCESS_CODE}
    Response Should Have Number Of Records    20    $..currency
    Get Search Payment Escrow Transaction   ${VALID_PAYMENT_PROJECT_ID}   status.specified=false
    Response Correct Code    ${SUCCESS_CODE}
    Response Should Not Contain Property    $..currency

TC_O2O_10263
    [Documentation]    [Payment][PaymentTransaction] can get transaction list by valid project id and request page
    [Tags]    High    Regression    Smoke    UnitTest    Robot
    Get Search Payment Escrow Transaction   ${VALID_PAYMENT_PROJECT_ID}   page=1
    Response Correct Code    ${SUCCESS_CODE}
    Response Should Have Number Of Records    20    $..projectId

TC_O2O_10272
    [Documentation]    [Payment][PaymentTransaction] verify response when access with invalid client scope on weomni gateway
    [Tags]    Medium    Smoke    Regression    Robot
    [Setup]   Generate Gateway Header With Scope and Permission    ${ROLE_USER}    ${ROLE_USER_PASSWORD}    payment.payment.read
    Get Search Payment Escrow Transaction   ${VALID_PAYMENT_PROJECT_ID}   ${empty}
    Response Correct Code    ${FORBIDDEN_CODE}
    Response Should Contain Property With Value    .detail          Access is denied
