*** Variables ***
${valid_clientTrx_id}             AutomateTest001
${invalid_clientTrx_id}           AutomateTest000

*** Settings ***
Documentation    Tests to verify that Get Payment Status by clientTrxId api works correctly

Resource    ../../../resources/init.robot
Resource    ../../../keywords/apigee/apigee_keywords.robot
Resource    ../../../keywords/apigee/apigee_payment_keywords.robot
Test Setup       Generate Apigee Header
Test Teardown    Delete All Sessions

*** Test Cases ***
TC_O2O_09882
    [Documentation]    [API] [Apigee] [Payment] Query successful with clientTrxId
    [Tags]    High    Smoke      Regression      Sanity
    Get Apigee Payment Status By ClientTrxId    ${TERMINAL_ID}    ${valid_clientTrx_id}      ${BRAND_ID}      ${BRANCH_ID}
    Response Correct Code      ${SUCCESS_CODE}
    Response Should Contain Property With Value          .payment..status    SUCCESS
    Response Should Contain Property With Value          .payment..code      1_success
    Response Should Contain Property With Value          .payment..message   Success
    Response Should Contain Property With Empty Value    .refund

TC_O2O_09884
    [Documentation]    [API] [Apigee] [Payment] Query fail due to invalid data
    [Tags]    High    Smoke      Regression      Sanity
    Get Apigee Payment Status By ClientTrxId    ${TERMINAL_ID}    ${invalid_clientTrx_id}      ${BRAND_ID}      ${BRANCH_ID}
    Response Correct Code      ${BAD_REQUEST_CODE}
    Response Should Contain Property With Value          .code    bad_request
    Response Should Contain Property With Value          .message      Transaction does not exist.
