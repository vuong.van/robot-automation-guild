*** Settings ***
Library         RequestsLibrary
Library         OperatingSystem
Library         JSONLibrary
Resource        ../../../keywords/rpp_merchant_bs/login_keywords.robot
Resource        ../../../keywords/rpp_merchant_bs/merchant_keywords.robot
Resource        ../../../keywords/common/rpp_gateway_common.robot
Test Setup      Create RPP Gateway Header
Test Teardown    Run Keywords    Delete All Sessions        

*** Variables ***
${real_merchant_id}                  5b925af53416175d3417de42
${fake_merchant_id}                  5b9253417de42
${merchant_name_en}               Test_verify_merchant_
${merchant_name_th}               ทดสอบ_ตรวจสอบเมอชานต์_

*** Test Cases ***
TC_O2O_10330
    [Documentation]   Verification 'get merchant detail' API by Inputting id truly exist
    [Tags]    Regression    Smoke
    Post Api Login     { "username":"egg1", "password":"1234", "user_type":"sale" }
    Get Accesstoken
    Get Api Merchnat Detail     ${real_merchant_id}
    Response Correct Code    ${SUCCESS_CODE}
    Response Should Contain Property With Value    status.code    ${200}
    Response Should Contain Property With Value    status.text    Success
    Response Should Contain Property With Value    data.id    ${real_merchant_id}

TC_O2O_10331
    [Documentation]   Verification 'get merchant detail' API by Inputting id doesn't truly exist
    [Tags]    Regression    Smoke
    Post Api Login     { "username":"egg1", "password":"1234", "user_type":"sale" }
    Get Accesstoken
    Get Api Merchnat Detail     ${fake_merchant_id}
    Response Correct Code    ${BAD_REQUEST_CODE}
    Response Should Contain Property With Value    status.code    ${400}
    Response Should Contain Property With Value    status.message    Bad Request
    Response Should Contain Property With Value    error.message    Mission Fail

TC_O2O_10332
    [Documentation]   Verification 'Create merchant' API by Inputting data correctly
    [Tags]    Regression    Smoke    Sanity
    Post Api Login     { "username":"egg1", "password":"1234", "user_type":"sale" }
    Get Accesstoken
    Prepare Body For Create Merchant    ${merchant_name_en}    ${merchant_name_th}
    Post Api Create Merchant     ${finalbody}
    Response Correct Code    ${SUCCESS_CODE}
    Response Should Contain Property With Value    status.code    ${200}
    Response Should Contain Property With Value    status.text    Success
    Response Should Contain Property With String Value    data.id
    Response Should Contain Property With String Value    data.merchant_id

TC_O2O_10333 
    [Documentation]   Verification 'Update merchant' API
    [Tags]    Regression    Smoke
    Post Api Login     { "username":"egg1", "password":"1234", "user_type":"sale" }
    Get Accesstoken
    Prepare Body For Update Merchant
    Put Api Update Merchant     ${body}    ${endpoint}
    Response Correct Code    ${SUCCESS_CODE}
    Response Should Contain Property With Value    status.code    ${200}
    Response Should Contain Property With Value    status.text    Success
    Response Should Contain Property With String Value    data.success  