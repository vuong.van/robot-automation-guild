*** Settings ***
Library         RequestsLibrary
Library         OperatingSystem
Library         JSONLibrary
Resource        ../../../keywords/rpp_merchant_bs/login_keywords.robot
Resource        ../../../keywords/rpp_merchant_bs/location_keyword.robot
Resource        ../../../keywords/common/rpp_gateway_common.robot
Test Setup      Create RPP Gateway Header
Test Teardown    Run Keywords    Delete All Sessions

*** Test Cases ***
TC_O2O_10334
    [Documentation]   Verification 'search location'
    [Tags]    Regression    Sanity    Smoke   
    
    Post Api Login     { "username":"egg1", "password":"1234", "user_type":"sale" }
    Get Accesstoken
    Get Api Search Location
    Response Correct Code    ${SUCCESS_CODE}
    Response Should Contain Property With Value    status.code    ${200}
    Response Should Contain Property With Value    status.text    Success
    Response Should Contain All Property Values Are String    data..merchant_id
    Response Should Contain All Property Values Are String    data..merchant_name.th
    Response Should Contain All Property Values Are String    data..merchant_name.en
    Response Should Contain Property Matches Regex    data..latitude    \\d+.\\d+
    Response Should Contain Property Matches Regex    data..longitude    \\d+.\\d+
    Response Should Contain All Property Values Are String   data..status