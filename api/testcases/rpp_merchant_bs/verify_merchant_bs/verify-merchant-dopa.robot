*** Settings ***
Library         RequestsLibrary
Library         OperatingSystem
Library         JSONLibrary
Resource        ../../../keywords/rpp_merchant_bs/login_keywords.robot
Resource        ../../../keywords/rpp_merchant_bs/merchant_dopa_keywords.robot
Resource        ../../../keywords/common/rpp_gateway_common.robot
Test Setup      Create RPP Gateway Header
Test Teardown    Run Keywords    Delete All Sessions  

*** Test Cases ***
TC_O2O_10338
    [Documentation]   Check Merchant Dopa
    [Tags]    Regression    Sanity    Smoke   
    
    Post Api Login     { "username":"egg1", "password":"1234", "user_type":"sale" }
    Get Accesstoken
    Post Api Check Merchant Dopa     { "mobile" : "0866636566", "card_no" : "1600100366042" }
    Response Correct Code    ${SUCCESS_CODE}
    Response Should Contain Property With Value    status.code    ${200}
    Response Should Contain Property With Value    status.text    Success
    Response Should Contain Property With Value    data.result    success
    Response Should Contain Property With Empty Value    data.message