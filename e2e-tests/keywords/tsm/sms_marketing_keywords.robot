*** Settings ***
Library    String
Library    DateTime    

*** Keywords ***
Generate Sms Campaign Data
    ${letter_string}=    String.Generate Random String    6    [LETTERS]
    ${today_day_month}=    DateTime.Get Current Date    result_format=%d%m
    BuiltIn.Set Test Variable    ${TEST_DATA_SMS_CAMPAIGN_TITLE}    หัวข้อ $%^${letter_string}${today_day_month}
    ${letter_string}=    String.Generate Random String    19    [LETTERS]
    BuiltIn.Set Test Variable    ${TEST_DATA_SMS_CAMPAIGN_CONTENT}    :D :) !!!!! #$%$ข้อมูลการทดสอบแคมเปญ :D :) !!!!${letter_string}${today_day_month}