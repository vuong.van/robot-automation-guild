*** Settings ***
Documentation    E2E test for SMS Marketing

Resource    ../../../api/resources/init.robot
Resource    ../../../mobile/resources/init.robot
Resource    ../../../mobile/keywords/common/mobile_common.robot
Resource    ../../../mobile/keywords/tsm/home_keywords.robot
Resource    ../../../mobile/keywords/tsm/check_store_registration_status_keywords.robot
Resource    ../../../mobile/keywords/tsm/otp_keywords.robot
Resource    ../../../mobile/keywords/tsm/merchant_home_keywords.robot
Resource    ../../../mobile/keywords/tsm/qr_code_merchant_keywords.robot
Resource    ../../../mobile/keywords/tsm/merchant_privilege_tutorial_keywords.robot
Resource    ../../../mobile/keywords/tsm/login_true_id_keywords.robot
Resource    ../../../mobile/keywords/tsm/start_using_true_id_keywords.robot
Resource    ../../../mobile/keywords/tsm/customer_privilege_list_keywords.robot
Resource    ../../../mobile/keywords/tsm/promotional_items_keywords.robot
Resource    ../../../mobile/keywords/tsm/sms_dashboard_keywords.robot
Resource    ../../../mobile/keywords/tsm/create_sms_campaign_keywords.robot
Resource    ../../../mobile/keywords/tsm/create_sms_campaign_successful_keywords.robot
Resource    ../../../mobile/keywords/tsm/sms_campaign_details_keywords.robot
Resource    ../../../web/resources/init.robot
Resource    ../../../web/keywords/common/web_common.robot
Resource    ../../../web/keywords/admintools/common_keywords.robot
Resource    ../../../web/keywords/admintools/main_page/login_keywords.robot
Resource    ../../../web/keywords/admintools/sms_dashboard_backoffice_keywords.robot
Resource    ../../keywords/tsm/sms_marketing_keywords.robot

Suite Setup         Open Apps   TSM
Suite Teardown      Close Application
Test Setup    Generate Gateway Header With Scope and Permission    ${ROLE_USER}    ${ROLE_USER_PASSWORD}    permission_group_name=notificationApproval
Test Teardown    Run Keywords    Delete Created Client And User Group    AND    Delete All Sessions    AND    Clean Environment

*** Variables ***
${merchant_feature_ios}    QR CODE ของฉัน
${merchant_feature_android}    QR code ของฉัน 
${qr_code_privilege}    บริหารร้านค้า
${customer_privilege}    สร้างส่งเสริมการขาย
${promotional_item}    ส่ง SMS

${sms_campaign_wait_for_approve_status}    รออนุมัติ
${sms_campaign_sending_status}    กำลังส่ง

*** Test Cases ***
TC_O2O_11697
    [Documentation]    [SMS Marketing] Merchant can create sms campaign, and sms messages are sent out when the created sms campaign is approved by admin
    [Tags]    Regression    High    E2E    Sanity    Robot
    #Create SMS Campaign on TSM
    sms_marketing_keywords.Generate Sms Campaign Data
    home_keywords.Tap On Find Merchant By Id Card Button
    check_store_registration_status_keywords.Tap On Allow Button On Popup
    check_store_registration_status_keywords.Tap On Allow Button On Popup
    check_store_registration_status_keywords.Access With Id Card Number    ${ID_CARD_NUMBER}
    check_store_registration_status_keywords.Select Desired Merchant    ${DESIRED_MERCHANT_NAME}
    otp_keywords.Submit Otp Number    ${OTP_NUMBER}
    merchant_home_keywords.Select Merchant Feature    ${merchant_feature_${OS}}
    qr_code_merchant_keywords.Select Merchant Privilege    ${qr_code_privilege}
    merchant_privilege_tutorial_keywords.Tap On Continue Button
    start_using_true_id_keywords.Tap On Start Using True Id Button
    login_true_id_keywords.Login With True Id    ${TRUE_ID_ACCOUNT}    ${TRUE_ID_PASSWORD}
    customer_privilege_list_keywords.Select Customer Privilege    ${customer_privilege}
    promotional_items_keywords.Select Promotional Item    ${promotional_item}
    sms_dashboard_keywords.Tap On Create Sms Campaign Button
    create_sms_campaign_keywords.Create Sms Campaign    ${TEST_DATA_SMS_CAMPAIGN_TITLE}    ${TEST_DATA_SMS_CAMPAIGN_CONTENT}
    create_sms_campaign_successful_keywords.Tap On Back To Sms Dashboard Button
    sms_dashboard_keywords.Verify Created Sms Campaign On Sms Dashboard    ${TEST_DATA_SMS_CAMPAIGN_TITLE}    ${sms_campaign_wait_for_approve_status}    1
    sms_dashboard_keywords.Tap On Sms Title On Table    ${TEST_DATA_SMS_CAMPAIGN_TITLE}
    sms_campaign_details_keywords.Verify Sms Campaign Summary    ${SMS_CAMPAIGN_CREATED_DATE}    1 คน    สมาชิก    1 เครดิต    
    sms_campaign_details_keywords.Verify Sms Campaign Content    ${TEST_DATA_SMS_CAMPAIGN_TITLE}    ${TEST_DATA_SMS_CAMPAIGN_CONTENT}
    sms_campaign_details_keywords.Verify Cancel Sms Campaign Button Is Enabled    
    #Approve the created SMS Campaign on Backoffice
    web_common.Open Browser With Option    ${ADMIN_TOOLS_URL}    headless_mode=${False}
    login_keywords.Login Backoffice    ${ROLE_USER}    ${ROLE_USER_PASSWORD}
    web_common.Click the Hamburger Menu
    web_common.Navigate On Right Menu Bar    SMS    Dashboard
    common_keywords.Wait Until Progress Loading Page Disappear
    sms_dashboard_backoffice_keywords.Click Edit Button By Sms Title    ${TEST_DATA_SMS_CAMPAIGN_TITLE}
    sms_dashboard_backoffice_keywords.Submit Sender Name And SMS Status    APPROVED    APPROVED
    #Check SMS Campaign's status is changed after approving on Backoffice
    sms_dashboard_keywords.Tap On Arrow Back Button
    sms_dashboard_keywords.Verify Created Sms Campaign On Sms Dashboard    ${TEST_DATA_SMS_CAMPAIGN_TITLE}    ${sms_campaign_sending_status}    1
    sms_dashboard_keywords.Tap On Sms Title On Table    ${TEST_DATA_SMS_CAMPAIGN_TITLE}
    sms_campaign_details_keywords.Verify Sms Campaign Summary    ${SMS_CAMPAIGN_CREATED_DATE}    1 คน    สมาชิก    1 เครดิต
    sms_campaign_details_keywords.Verify Sms Campaign Content    ${TEST_DATA_SMS_CAMPAIGN_TITLE}    ${TEST_DATA_SMS_CAMPAIGN_CONTENT}
    sms_campaign_details_keywords.Verify Cancel Sms Campaign Button Is Disabled