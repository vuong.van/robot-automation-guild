*** Settings ***
Documentation    Tests to verify All payment api with 2C2P works correctly

Resource    ../../../api/resources/init.robot
Resource    ../../../api/keywords/payment/payment_escrow_resource_keywords.robot
Resource    ../../../api/keywords/payment_transaction/payment_transaction_resource_keywords.robot
Test Setup        Generate Gateway Header With Scope and Permission    ${ROLE_USER}    ${ROLE_USER_PASSWORD}    payment.payment.write,payment.payment.read,payment.escrow.r
Test Teardown     Run Keywords    Delete Created Client And User Group    AND    Delete All Sessions

*** Test Cases ***
TC_O2O_09976
    [Documentation]    [Payment][E2E] User can create payment void and get the transaction status with payment method =Credit_Card via 2C2P
    [Tags]    High    Regression    E2E    Sanity    Smoke
    Generate Transaction Reference    6
    Post Payment Escrow Charge     ${VALID_PAYMENT_PROJECT_ID}    {"paymentMethod":${payment_method},"currency":${currency},"paymentInfo":${credit_card},"amount":${amount},"extRefId":"${TRANSACTION_REFERENCE}","merchantRefId":"${merchant_id}"}
    Get Transaction ID
    Get Payment Escrow Query   ${VALID_PAYMENT_PROJECT_ID}   ${transaction_id}
    Response Correct Code    ${SUCCESS_CODE}
    Response Should Contain Property With Value    .status            SUCCESS
    Response Should Contain Property With Value    .paymentMethod     CREDIT_CARD
    Response Should Contain Property With Value    .action            CHARGE
    Response Should Contain Property With Value    .responseCode      2_00
    Response Should Contain Property With Value    .responseMessage   Success
    Response Should Contain Property With Value    .paymentStatus     APPROVED
    Post Payment Escrow Refund   ${VALID_PAYMENT_PROJECT_ID}   ${transaction_id}     {"amount" :${amount},"voided": false}
    Response Correct Code    ${BAD_REQUEST_CODE}
    Response Should Contain Property With Value    fieldErrors..code        2_12
    Response Should Contain Property With Value    fieldErrors..message     Transaction status is not valid to perform your action.
    Post Payment Escrow Refund   ${VALID_PAYMENT_PROJECT_ID}   ${transaction_id}     {"amount" :${amount},"voided": true}
    Response Correct Code    ${CREATED_CODE}
    Response Should Contain Property With Value    .responseCode        2_00
    Response Should Contain Property With Value    .responseMessage     Success
    Response Should Contain Property With Value    .paymentMethod       CREDIT_CARD
    Response Should Contain Property With Value    .action              VOID
    Get Transaction ID
    Get Payment Escrow Query   ${VALID_PAYMENT_PROJECT_ID}   ${transaction_id}
    Response Correct Code    ${SUCCESS_CODE}
    Response Should Contain Property With Value    .status            SUCCESS
    Response Should Contain Property With Value    .paymentMethod     CREDIT_CARD
    Response Should Contain Property With Value    .action            VOID
    Response Should Contain Property With Value    .responseCode      2_00
    Response Should Contain Property With Value    .responseMessage   Success
    Response Should Contain Property With Value    .paymentStatus     VOIDED
    Get Search Payment Escrow Transaction By Id    ${VALID_PAYMENT_PROJECT_ID}    ${transaction_id}    detail=true
    Response Correct Code    ${SUCCESS_CODE}
    Response Should Contain Property With Value    .paymentGatewayType    2C2P
    Response Should Contain Property With Value    .transactionType    ESCROW
    Response Should Contain Property With Value    .paymentMethod    CREDIT_CARD
    Response Should Contain Property With Value    .status            SUCCESS
    Response Should Contain Property With Value    .action            VOID
    Response Should Contain Property With Value    .details..status    VOIDED
    Response Should Contain Property With Value    .details..voided..paymentGatewayType    2C2P
    Response Should Contain Property With Value    .details..voided..transactionType    ESCROW
    Response Should Contain Property With Value    .details..voided..paymentMethod    CREDIT_CARD
    Response Should Contain Property With Value    .details..voided..status    SUCCESS
    Response Should Contain Property With Value    .details..voided..action    VOID
    Response Should Contain Property With Value    .details..refunds..paymentGatewayType    2C2P
    Response Should Contain Property With Value    .details..refunds..transactionType    ESCROW
    Response Should Contain Property With Value    .details..refunds..paymentMethod    CREDIT_CARD
    Response Should Contain Property With Value    .details..refunds..status    FAIL
    Response Should Contain Property With Value    .details..refunds..action    REFUND
