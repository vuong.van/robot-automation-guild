*** Settings ***
Documentation    E2E test for Merchant onbroading process for both Organize and Unorganize merchant

Resource        ../../../mobile/keywords/common/mobile_common.robot
Resource        ../../../api/keywords/common/api_common.robot
Resource        ../../../api/keywords/common/rpp_common.robot
Resource        ../../../api/keywords/common/log_common.robot
Resource        ../../../mobile/resources/init.robot
Resource        ../../../api/resources/init.robot
Resource        ../../../mobile/keywords/tsm/on_boarding/on_boarding_keywords.robot
Resource        ../../../web/keywords/saleapp/web_audit/merchant_management_keyword.robot
Resource        ../../../api/keywords/rpp_merchant/merchant_resource_keywords.robot
Resource        ../../../api/keywords/rpp_merchant/activation_code_resource_keywords.robot
Resource        ../../../api/resources/configs/alpha/test_accounts.robot
Resource        ../../../web/resources/configs/alpha/env_config.robot
Resource        ../../../api/keywords/rpp_merchant/outlet_v_2_resource_keywords.robot
Resource        ../../../api/keywords/rpp_merchant/merchant_registration_resource_keywords.robot
Resource        ../../../web/resources/init.robot
Resource        ../../../web/keywords/common/web_common.robot
Resource        ../../../web/keywords/retail_admin_portal/login_keywords.robot
Resource        ../../../web/keywords/retail_admin_portal/retail_admin_portal_common_keywords.robot
Resource        ../../../web/keywords/retail_admin_portal/merchant_list_keywords.robot
Resource        ../../../web/keywords/retail_admin_portal/merchant_profile_keywords.robot
Resource        ../../../web/keywords/deal_management/deal_management_common_keywords.robot
Resource        ../../../web/keywords/deal_management/login_keywords.robot
Resource        ../../../web/keywords/deal_management/partner_requests_list_keywords.robot
Resource        ../../../web/keywords/deal_management/partner_requests_detail_keywords.robot
Resource        ../../../web/keywords/deal_management/merchant_detail_keywords.robot
Resource        ../../../web/keywords/deal_management/campaign_detail_keywords.robot
Resource        ../../../api/keywords/rpp-mastermerchant/search-merchant-keywords.robot
Resource        ../../../api/keywords/rpp_payment/rpp_api_keywords.robot
Resource        ../../../api/keywords/common/rpp_gateway_common.robot
Resource        ../../../api/keywords/rpp_merchant_bs/login_keywords.robot
Resource        ../../../api/keywords/rpp_merchant_bs/merchant_keywords.robot

Test Setup    Generate Gateway Header With Scope and Permission    ${ROLE_USER}    ${ROLE_USER_PASSWORD}    merchant.merchant.write,merchant.outlet.actAsAdmin,merchant.merchant.read,activationcode.write,activationcode.read,merchant.terminal.read,merchant.outlet.actAsAdmin    permission_name=merchant.outlet.actAsAdmin
Test Teardown    Run Keywords    Delete Created Client And User Group    AND    Delete All Sessions    AND    Clean Environment    AND    Log Bug For Test Case
Suite Setup        Open SaleApp Android          SALEAPP
Suite Teardown   Close Application

*** Variables ***
${login_user}            taxi
${login_password}        taxi
${retail_admin_portal_url}        https://retail-admin-portal.tmn-dev.com/login/
${chrm_backoffice_url}            http://chrm-backoffice-dev.trueyou.co.th/

*** Test Cases ***
TC_O2O_08909
    [Documentation]    Ensure Organize merchant can onboard from Sales App with 5 new outlets with Activation code and Terminal.(Service Type TY+TMN and Device type EDC).
    [Tags]     Regression     Medium    Sanity    Smoke    E2E
    Login To SaleApp            ${login_user}            ${login_password}
    Random Merchant Name
    Register New Merchant       ${random_merchant}       ${random_english_merchant}
    Merchant Name Should Displayed Correctly             ${random_merchant}
    
    Get Merchant Information    storeNameTh=ทดสอบสร้างนิวเม50
    Get Merchant IDs
    Response Should Contain Property With Value    content..status    WAITING
    Response Should Contain Property With Value    content..tmnStatus    WAITING
    Response Should Contain Property With Value    content..'trueyouStatus'    WAITING
    
    outlet_v_2_resource_keywords.Post Create Outlets And Address Ref Merchant Id    { "outlets": [ { "addresses": [ { "address": "TEST New Multi Outlet", "addressType": "store", "contentType": "test e2e", "districtId": 1, "latitude": "53.286518", "longtitude": "-6.416770", "postCode": "10250", "provinceId": 1, "road": "Ascesnd", "subdistrictId": 6 } ], "contactPersons": [ { "birthDate": "1988-10-10", "email": "pancake-automate-pool@ascend.com", "firstNameEn": "Ascend", "firstNameTh": "แอสเซนด์", "gender": "Female", "lastNameEn": "Only", "lastNameTh": "ออนลี่", "mobileNo": "66-864403645", "occupation": "Testing", "passportNo": null, "phoneNo": "020000000", "refId": 1234567890123, "refType": "CID", "thaiId": "${ACCOUNT_VALUE}", "titleId": 2, "titleOtherName": null } ], "merchantId": "${TEST_DATA_MERCHANT_SEQUENCE_ID}", "franchisee": true, "headQuarter": true, "outletDetail": "Test Outlet details", "outletNameEn": "${outlet_name_en}", "outletNameTh": "ทดสอบ_นิว เอ้าท์เล็ท ", "registerChannel": "app_sale_agent", "saleId": "0002", "status": "DRAFT", "tmnOutletId": null, "tmnStatus": "WAITING", "trueyouStatus": "WAITING" }] }
    Get Outlet Ids Test Data
    Response Correct Code    ${CREATED_CODE}
    Response Should Contain Property With Value    outlets..outletNameEn    ${outlet_name_en}
    Response Should Contain Property With Value    outlets..status    WAITING
    Response Should Contain Property With Value    outlets..tmnStatus    WAITING
    Response Should Contain Property With Value    outlets.."trueyouStatus"    WAITING

    outlet_v_2_resource_keywords.Get Search Outlet By Id    ${TEST_DATA_OUTLET_ID}
    Response Correct Code    ${SUCCESS_CODE}
    Response Should Contain Property With Value    status    WAITING
    Response Should Contain Property With Value    tmnStatus    WAITING
    Response Should Contain Property With Value    "trueyouStatus"    WAITING

    Get Random Activation Code
    Create And Get Activation Code ID By Outlet ID    ${TEST_DATA_OUTLET_ID}    { "activationCode" : "${RANDOM_ACTIVATION_CODE}", "amount" : 1, "notification": { "email" : "automation.pool@ascendcorp.com", "mobile" : "66-864403645", "receiver": "CUSTOM", "isGroup": true }, "terminal" : { "device" : "EDC" } }
    Get Terminal External ID
    Wait Until Api Keyword Response Property Change    Get Activation Code Information    content..status    CREATED    10    activationCode=${RANDOM_ACTIVATION_CODE}
    Response Should Contain Property With Value    content..status    CREATED
    
    Open Browser With Option    ${retail_admin_portal_url}    headless_mode=${False}
    Login True Money Admin Portal    ${TRUE_MONEY_USERNAME}    ${TRUE_MONEY_PASSWORD}
    Navigate To True Money Admin Portal Menu    Merchant Management    Merchant List
    merchant_list_keywords.Search For Merchant    Merchant Name    ${TEST_DATA_STORE_NAME_TH}
    merchant_list_keywords.Click Approve Link
    merchant_profile_keywords.Click Update Merchant Kyb Link
    merchant_profile_keywords.Update Kyb Status    Certificated
    Clean Environment

    merchant_registration_resource_keywords.Get Test Data Merchant Id On Mongo Database    merchant_name.en=${TEST_DATA_STORE_NAME_EN}
    Create RPP Header
    rpp_api_keywords.Get True Money Check Approve    id=${TEST_DATA_MERCHANT_MONGO_ID}
    merchant_resource_keywords.Get Merchant Information    id=${TEST_DATA_MERCHANT_SEQUENCE_ID}
    Response Correct Code    ${SUCCESS_CODE}
    Response Should Contain Property With Value    content..status    WAITING
    Response Should Contain Property With Value    content..tmnStatus    APPROVE
    Response Should Contain Property With Value    content.."trueyouStatus"    WAITING

    Wait Until Api Keyword Response Property Change    outlet_v_2_resource_keywords.Get Search Outlet By Id    tmnStatus    APPROVE    10    ${TEST_DATA_OUTLET_ID}
    outlet_v_2_resource_keywords.Get Search Outlet By Id    ${TEST_DATA_OUTLET_ID}
    Response Correct Code    ${SUCCESS_CODE}
    Response Should Contain Property With Value    status    WAITING
    Response Should Contain Property With Value    tmnStatus    APPROVE
    Response Should Contain Property With Value    "trueyouStatus"    WAITING

    Open Browser With Option    ${chrm_backoffice_url}    headless_mode=${False}
    login_keywords.Login Deal Management    natarod    natarod
    deal_management_common_keywords.Navigate To Deal Management Menu    RPP-EDC    PROSPECT
    partner_requests_list_keywords.Search For Merchant    ${TEST_DATA_STORE_NAME_EN}
    partner_requests_list_keywords.Click On Magnifier Icon
    partner_requests_detail_keywords.Click Button On Partner Requests Detail Page    Next
    partner_requests_detail_keywords.Click Ok On Popups
    merchant_detail_keywords.Click Button On Merchant Detail Page    Next
    campaign_detail_keywords.Input Test Data Campaign Details Popup    ช้อปกับทรู     ของเล่น/เกม    Brief   EDC/GMD    กรุงเทพ-ปริมณฑล
    Clean Environment
    
    Wait Until Api Keyword Response Property Change    outlet_v_2_resource_keywords.Get Search Outlet By Id    "trueyouStatus"    APPROVE    10    ${TEST_DATA_OUTLET_ID}
    merchant_resource_keywords.Get Merchant Information    id=${TEST_DATA_MERCHANT_SEQUENCE_ID}
    Response Correct Code    ${SUCCESS_CODE}
    Response Should Contain Property With Value    content..status    APPROVE
    Response Should Contain Property With Value    content..tmnStatus    APPROVE
    Response Should Contain Property With Value    content.."trueyouStatus"    APPROVE

    outlet_v_2_resource_keywords.Get Search Outlet By Id    ${TEST_DATA_OUTLET_ID}
    Response Correct Code    ${SUCCESS_CODE}
    Response Should Contain Property With Value    status    APPROVE
    Response Should Contain Property With Value    tmnStatus    APPROVE
    Response Should Contain Property With Value    "trueyouStatus"    APPROVE
    Get Outlet Ids Test Data

    log    Below step is fail because the issue from ASCO2O-3922
    Activate Terminal    { "activationCode" : "${RANDOM_ACTIVATION_CODE}", "deviceNumber" : "EDC0001", "merchantId" : "${TEST_DATA_MERCHANT_ID}", "outletId" : "${TEST_DATA_OUTLET_FAI_USE_ID}", "terminalId" : "@{EXTERNAL_TERMINAL_ID}[0]"}
    Get Activation Code Information      activationCode=${RANDOM_ACTIVATION_CODE}
    Response Should Contain Property With Value    content..status    USED
    Get Terminal Information    ${RANDOM_ACTIVATION_CODE}
    Response Should Contain Property With Value    .status    ACTIVATE

TC_O2O_10886
    [Documentation]    Ensure user can register new merchant from Sales app (Service Type TY+TMN and Device type EDC).
    [Tags]     Sanity    Smoke
    Login To SaleApp            ${login_user}            ${login_password}
    Random Merchant Name
    Register New Merchant       ${random_merchant}       ${random_english_merchant}
    Merchant Name Should Displayed Correctly             ${random_merchant}

    Get Merchant Information    storeNameTh=${random_merchant}
    Get Merchant IDs
    Response Should Contain Property With Value    content..status    WAITING
    Response Should Contain Property With Value    content..tmnStatus    WAITING
    Response Should Contain Property With Value    content..'trueyouStatus'    WAITING

TC_O2O_10887
    [Documentation]    Ensure user can create new outlet for merchant (Service Type TY+TMN and Device type EDC).
    [Tags]     Sanity    Smoke
    Create RPP Gateway Header
    Post Api Login     { "username":"egg1", "password":"1234", "user_type":"sale" }
    Get Accesstoken
    Prepare Body For Create Merchant
    Post Api Create Merchant     ${finalbody}
    Response Correct Code    ${SUCCESS_CODE}
    Response Should Contain Property With Value    status.code    ${200}
    Response Should Contain Property With Value    status.text    Success
    Response Should Contain Property With String Value    data.id
    Response Should Contain Property With String Value    data.merchant_id

    Get Merchant Information    storeNameTh=${MERCHANT_TH_NAME}
    Get Merchant IDs
    Response Should Contain Property With Value    content..status    WAITING
    Response Should Contain Property With Value    content..tmnStatus    WAITING
    Response Should Contain Property With Value    content..'trueyouStatus'    WAITING

    outlet_v_2_resource_keywords.Post Create Outlets And Address Ref Merchant Id    { "outlets": [ { "addresses": [ { "address": "TEST New Multi Outlet", "addressType": "store", "contentType": "test e2e", "districtId": 1, "latitude": "53.286518", "longtitude": "-6.416770", "postCode": "10250", "provinceId": 1, "road": "Ascesnd", "subdistrictId": 6 } ], "contactPersons": [ { "birthDate": "1988-10-10", "email": "pancake-automate-pool@ascend.com", "firstNameEn": "Ascend", "firstNameTh": "แอสเซนด์", "gender": "Female", "lastNameEn": "Only", "lastNameTh": "ออนลี่", "mobileNo": "66-864403645", "occupation": "Testing", "passportNo": null, "phoneNo": "020000000", "refId": 1234567890123, "refType": "CID", "thaiId": "${ACCOUNT_VALUE}", "titleId": 2, "titleOtherName": null } ], "merchantId": "${TEST_DATA_MERCHANT_SEQUENCE_ID}", "franchisee": true, "headQuarter": true, "outletDetail": "Test Outlet details", "outletNameEn": "${outlet_name_en}", "outletNameTh": "ทดสอบ_นิว เอ้าท์เล็ท ", "registerChannel": "app_sale_agent", "saleId": "0002", "status": "DRAFT", "tmnOutletId": null, "tmnStatus": "WAITING", "trueyouStatus": "WAITING" }] }
    Get Outlet Ids Test Data
    Response Correct Code    ${CREATED_CODE}
    Response Should Contain Property With Value    outlets..outletNameEn    ${outlet_name_en}
    Response Should Contain Property With Value    outlets..status    WAITING
    Response Should Contain Property With Value    outlets..tmnStatus    WAITING
    Response Should Contain Property With Value    outlets.."trueyouStatus"    WAITING

    outlet_v_2_resource_keywords.Get Search Outlet By Id    ${TEST_DATA_OUTLET_ID}
    Response Correct Code    ${SUCCESS_CODE}
    Response Should Contain Property With Value    status    WAITING
    Response Should Contain Property With Value    tmnStatus    WAITING
    Response Should Contain Property With Value    "trueyouStatus"    WAITING

    Get Random Activation Code
    Create And Get Activation Code ID By Outlet ID    ${TEST_DATA_OUTLET_ID}    { "activationCode" : "${RANDOM_ACTIVATION_CODE}", "amount" : 1, "notification": { "email" : "automation.pool@ascendcorp.com", "mobile" : "66-864403645", "receiver": "CUSTOM", "isGroup": true }, "terminal" : { "device" : "EDC" } }
    Get Terminal External ID
    Wait Until Api Keyword Response Property Change    Get Activation Code Information    content..status    CREATED    10    activationCode=${RANDOM_ACTIVATION_CODE}
    Response Should Contain Property With Value    content..status    CREATED

    Open Browser With Option    ${retail_admin_portal_url}    headless_mode=${False}
    Login True Money Admin Portal    ${TRUE_MONEY_USERNAME}    ${TRUE_MONEY_PASSWORD}
    Navigate To True Money Admin Portal Menu    Merchant Management    Merchant List
    merchant_list_keywords.Search For Merchant    Merchant Name    ${TEST_DATA_STORE_NAME_TH}
    merchant_list_keywords.Click Approve Link
    merchant_profile_keywords.Click Update Merchant Kyb Link
    merchant_profile_keywords.Update Kyb Status    Certificated
    Clean Environment

    merchant_registration_resource_keywords.Get Test Data Merchant Id On Mongo Database    merchant_name.en=${TEST_DATA_STORE_NAME_EN}
    Create RPP Header
    rpp_api_keywords.Get True Money Check Approve    id=${TEST_DATA_MERCHANT_MONGO_ID}
    merchant_resource_keywords.Get Merchant Information    id=${TEST_DATA_MERCHANT_SEQUENCE_ID}
    Response Correct Code    ${SUCCESS_CODE}
    Response Should Contain Property With Value    content..status    WAITING
    Response Should Contain Property With Value    content..tmnStatus    APPROVE
    Response Should Contain Property With Value    content.."trueyouStatus"    WAITING

    Wait Until Api Keyword Response Property Change    outlet_v_2_resource_keywords.Get Search Outlet By Id    tmnStatus    APPROVE    10    ${TEST_DATA_OUTLET_ID}
    outlet_v_2_resource_keywords.Get Search Outlet By Id    ${TEST_DATA_OUTLET_ID}
    Response Correct Code    ${SUCCESS_CODE}
    Response Should Contain Property With Value    status    WAITING
    Response Should Contain Property With Value    tmnStatus    APPROVE
    Response Should Contain Property With Value    "trueyouStatus"    WAITING

    Open Browser With Option    ${chrm_backoffice_url}    headless_mode=${False}
    login_keywords.Login Deal Management    natarod    natarod
    deal_management_common_keywords.Navigate To Deal Management Menu    RPP-EDC    PROSPECT
    partner_requests_list_keywords.Search For Merchant    ${TEST_DATA_STORE_NAME_EN}
    partner_requests_list_keywords.Click On Magnifier Icon
    partner_requests_detail_keywords.Click Button On Partner Requests Detail Page    Next
    partner_requests_detail_keywords.Click Ok On Popups
    merchant_detail_keywords.Click Button On Merchant Detail Page    Next
    campaign_detail_keywords.Input Test Data Campaign Details Popup    ช้อปกับทรู     ของเล่น/เกม    Brief   EDC/GMD    กรุงเทพ-ปริมณฑล
    Clean Environment

    Wait Until Api Keyword Response Property Change    outlet_v_2_resource_keywords.Get Search Outlet By Id    "trueyouStatus"    APPROVE    10    ${TEST_DATA_OUTLET_ID}
    merchant_resource_keywords.Get Merchant Information    id=${TEST_DATA_MERCHANT_SEQUENCE_ID}
    Response Correct Code    ${SUCCESS_CODE}
    Response Should Contain Property With Value    content..status    APPROVE
    Response Should Contain Property With Value    content..tmnStatus    APPROVE
    Response Should Contain Property With Value    content.."trueyouStatus"    APPROVE

    outlet_v_2_resource_keywords.Get Search Outlet By Id    ${TEST_DATA_OUTLET_ID}
    Response Correct Code    ${SUCCESS_CODE}
    Response Should Contain Property With Value    status    APPROVE
    Response Should Contain Property With Value    tmnStatus    APPROVE
    Response Should Contain Property With Value    "trueyouStatus"    APPROVE
    Get Outlet Ids Test Data

    log    Below step is fail because the issue from ASCO2O-3922
    Activate Terminal    { "activationCode" : "${RANDOM_ACTIVATION_CODE}", "deviceNumber" : "EDC0001", "merchantId" : "${TEST_DATA_MERCHANT_ID}", "outletId" : "${TEST_DATA_OUTLET_FAI_USE_ID}", "terminalId" : "@{EXTERNAL_TERMINAL_ID}[0]"}
    Get Activation Code Information      activationCode=${RANDOM_ACTIVATION_CODE}
    Response Should Contain Property With Value    content..status    USED
    Get Terminal Information    ${RANDOM_ACTIVATION_CODE}
    Response Should Contain Property With Value    .status    ACTIVATE

