*** Settings ***
Documentation    E2E test for Merchant onbroading process for both Organize and Unorganize merchant

Resource       ../../../mobile/resources/init.robot
Resource       ../../../mobile/keywords/common/mobile_common.robot
# Resource    ../../../mobile/keywords/tsm/home_keywords.robot
Resource    ../../../mobile/keywords/tsm/tutorial/tutorial_keywords.robot

# Resource    ../../../mobile/keywords/tsm/open_shop_steps_keywords.robot
Resource    ../../../mobile/keywords/tsm/login_trueid/open_shop_steps_keywords.robot

# Resource    ../../../mobile/keywords/tsm/login_true_id_keywords.robot
Resource    ../../../mobile/keywords/tsm/login_trueid/login_true_id_keywords.robot

Resource    ../../../mobile/keywords/tsm/connect_true_money_keywords.robot
Resource    ../../../mobile/keywords/tsm/allow_tsm_connect_true_money_keywords.robot
Resource    ../../../mobile/keywords/tsm/select_merchant_type_keywords.robot
Resource    ../../../mobile/keywords/tsm/create_merchant_keywords.robot
Resource    ../../../mobile/keywords/tsm/register_qr_merchant_keywords.robot


Resource       ../../../api/keywords/common/api_common.robot
Resource       ../../../api/keywords/common/rpp_common.robot
Resource       ../../../api/resources/init.robot
Resource       ../../../web/keywords/saleapp/web_audit/merchant_management_keyword.robot
Resource       ../../../api/keywords/rpp_merchant/merchant_resource_keywords.robot
Resource       ../../../api/keywords/rpp_merchant/activation_code_resource_keywords.robot
Resource       ../../../api/resources/configs/alpha/test_accounts.robot
Resource       ../../../web/resources/configs/alpha/env_config.robot
Resource       ../../../api/keywords/rpp_merchant/outlet_v_2_resource_keywords.robot
Resource       ../../../api/keywords/rpp_merchant/merchant_registration_resource_keywords.robot
Resource       ../../../web/resources/init.robot
Resource       ../../../web/keywords/common/web_common.robot
Resource       ../../../web/keywords/retail_admin_portal/login_keywords.robot
Resource       ../../../web/keywords/retail_admin_portal/retail_admin_portal_common_keywords.robot
Resource       ../../../web/keywords/retail_admin_portal/merchant_list_keywords.robot
Resource       ../../../web/keywords/retail_admin_portal/merchant_profile_keywords.robot
Resource       ../../../web/keywords/deal_management/deal_management_common_keywords.robot
Resource       ../../../web/keywords/deal_management/login_keywords.robot
Resource       ../../../web/keywords/deal_management/partner_requests_list_keywords.robot
Resource       ../../../web/keywords/deal_management/partner_requests_detail_keywords.robot
Resource       ../../../web/keywords/deal_management/merchant_detail_keywords.robot
Resource       ../../../web/keywords/deal_management/campaign_detail_keywords.robot
Resource    ../../../web/keywords/saleapp/web_audit/common_web_audit_keyword.robot
Resource    ../../../web/keywords/saleapp/web_audit/merchant_list_keyword.robot
Resource    ../../../web/keywords/saleapp/web_audit/shop_registration_details_keyword.robot
Resource       ../../../api/keywords/rpp-mastermerchant/search-merchant-keywords.robot
Resource       ../../../api/keywords/rpp_payment/rpp_api_keywords.robot
Resource       ../../../api/keywords/common/api_common.robot
Resource       ../../../api/resources/init.robot

Suite Setup         Open Apps   TSM
# Suite Teardown      Close Application
# Test Setup    Generate Gateway Header With Scope and Permission    ${ROLE_USER}    ${ROLE_USER_PASSWORD}    merchant.merchant.read,activationcode.write,activationcode.read,merchant.terminal.read,merchant.outlet.actAsAdmin    permission_name=merchant.outlet.actAsAdmin
# Test Teardown       Run Keywords    Delete Created Client And User Group    AND    Delete All Sessions    AND    Clean Environment

*** Variables ***
${outlet_id}           91250
${outlet_name_en}       PancakeAutomatPool
${chrm_backoffice_url}            http://chrm-backoffice-dev.trueyou.co.th/
${merchant_sequence_id}    108039
${merchant_type}    มีหน้าร้าน

${owner_title}    นางสาว
${merchant_gender}    ชาย
${province}    กรุงเทพมหานคร
${district}    หนองจอก
${sub_district}    คลองสิบสอง
${contact_information}    ข้อมูลเดียวกับเจ้าของร้าน
${merchant_category}    อาหารและเครื่องดื่ม
${shop_highlight}    กรุณากรอกข้อมูลประเภทธุรกิจ บรรยากาศร้าน เมนู/สินค้าเด่น ที่ทรูยู จะช่วยโปรโมทร้านคุณ ให้เป็นที่รู้จักของลูกค้าทรูและทั่วไป

*** Test Cases ***
TC_020_08908
    [Documentation]    Ensure UnOrganize merchant can onboard with 1 new outlet and Generate new Activation code before create Terminal (Service Type TY+TMN and Device type QRCODE)
    [Tags]    #TBD
    tutorial_keywords.Tap On Check Merchant Status Menu
    open_shop_steps_keywords.Tap On Embark Button
    login_true_id_keywords.Login With True Id    ${TRUE_ID_ACCOUNT}    ${TRUE_ID_PASSWORD}
    
    connect_true_money_keywords.Connect True Money    ${TRUE_MONEY_ACCOUNT_MOBILE_NO}    ${TRUE_MONEY_ACCOUNT_THAI_ID}    ${TRUE_MONEY_ACCOUNT_MOCKED_OTP}
    allow_tsm_connect_true_money_keywords.Tap On Submit Button
    select_merchant_type_keywords.Select Merchant By Type    ${merchant_type}
    create_merchant_keywords.Tap On Allow Button
    # create_merchant_keywords.Tap On Start To Create A New Merchant
        
    create_merchant_keywords.Random Merchant Name
    register_qr_merchant_keywords.Select Owner Title    ${owner_title}
    register_qr_merchant_keywords.Select Merchant Owner Gender    ${merchant_gender}
    register_qr_merchant_keywords.Select Merchant Province    ${province}    ${district}    ${sub_district}
    register_qr_merchant_keywords.Upload Id Card Picture
    register_qr_merchant_keywords.Enter Email Address    ${EMAIL_ADDRESS_FOR_TESTING}
    register_qr_merchant_keywords.Enter Merchant Name    ${RANDOM_THAI_MERCHANT}    ${RANDOM_ENGLISH_MERCHANT}
    register_qr_merchant_keywords.Check Use Same Address With Business Owner Checkbox
    register_qr_merchant_keywords.Enter Merchant Information Details    ${IPHONE_10_PHONE_NUMBER}    ${merchant_category}    ${shop_highlight}
    register_qr_merchant_keywords.Upload Pictures Of Store And Product
    register_qr_merchant_keywords.Select Contact Information    ${contact_information}
    register_qr_merchant_keywords.Submit Merchant Registration Information    

    # Create Merchant Information Data          ${RANDOM_THAI_MERCHANT}       ${RANDOM_ENGLISH_MERCHANT}
    # Create TrueYou Information Data

    # Get Merchant Information     storeNameTh=${random_thai_merchant}
    # Get Merchant IDs
    # Response Should Contain Property With Value    content..status    DRAFT
    # Response Should Contain Property With Value    content..tmnStatus    WAITING
    # Response Should Contain Property With Value    content..'trueyouStatus'    WAITING

    # Get True You Merchant ID
    # Set Library Search Order	           SeleniumLibrary
    # Open Browser With Option    ${WEB_AUDIT_URL}    headless_mode=${False}

    # Login Web Audit    ${DRIVER_USERNAME}    ${DRIVER_PASSWORD}
    # Search Merchant By True You Merchant ID    ${TY_MERCHANT_ID}
    # Upload Pictures From Audit Web
    # Wait Until Api Keyword Response Property Change    Get Merchant Information    storeNameTh=${random_thai_merchant}    content..status    WAITING    10
    # Response Should Contain Property With Value    content..status    WAITING
    # Response Should Contain Property With Value    content..tmnStatus    WAITING
    # Response Should Contain Property With Value    content..'trueyouStatus'    WAITING
    
    
    # outlet_v_2_resource_keywords.Post Create Outlets And Address Ref Merchant Id    { "outlets": [ { "addresses": [ { "address": "TEST New Multi Outlet", "addressType": "store", "contentType": "test e2e", "districtId": 1, "latitude": "53.286518", "longtitude": "-6.416770", "postCode": "10250", "provinceId": 1, "road": "Ascesnd", "subdistrictId": 6 } ], "contactPersons": [ { "birthDate": "1988-10-10", "email": "${ROBOT_TEST_EMAIL_ADDRESS}", "firstNameEn": "Ascend", "firstNameTh": "แอสเซนด์", "gender": "Female", "lastNameEn": "Only", "lastNameTh": "ออนลี่", "mobileNo": "${ROBOT_TEST_PHONE_NUMBER}", "occupation": "Testing", "passportNo": null, "phoneNo": "020000000", "refId": 1234567890123, "refType": "CID", "thaiId": "${ACCOUNT_VALUE}", "titleId": 2, "titleOtherName": null } ], "merchantId": "${TEST_DATA_MERCHANT_SEQUENCE_ID}", "franchisee": true, "headQuarter": true, "outletDetail": "Test Outlet details", "outletNameEn": "${outlet_name_en}", "outletNameTh": "ทดสอบ_นิว เอ้าท์เล็ท ", "registerChannel": "app_sale_agent", "saleId": "0002", "status": "DRAFT", "tmnOutletId": null, "tmnStatus": "WAITING", "trueyouStatus": "WAITING" }] }
    # Get Outlet Ids Test Data
    # Response Correct Code    ${CREATED_CODE}
    # Response Should Contain Property With Value    outlets..outletNameEn    ${outlet_name_en}
    # Response Should Contain Property With Value    outlets..status    WAITING
    # Response Should Contain Property With Value    outlets..tmnStatus    WAITING
    # Response Should Contain Property With Value    outlets.."trueyouStatus"    WAITING

    # outlet_v_2_resource_keywords.Get Search Outlet By Id    ${TEST_DATA_OUTLET_ID}
    # Response Correct Code    ${SUCCESS_CODE}
    # Response Should Contain Property With Value    status    WAITING
    # Response Should Contain Property With Value    tmnStatus    WAITING
    # Response Should Contain Property With Value    "trueyouStatus"    WAITING


    # Create And Get Activation Code ID By Outlet ID    ${outlet_id}    { "amount" : 2, "notification" : { "email" : "patthamaporn.kam@ascendcorp.com", "mobile": "66-864403645", "isGroup": false, "receiver": "CUSTOM"} }
    # Wait Until Api Keyword Response Property Change    Get Activation Code Information    activationCode=@{activation_codes}[0]    content..status    WAITING_ACTIVATE    10
    # log    Real status should be CREATED
    # Response Should Contain Property With Value    content..status    WAITING_ACTIVATE

    # Get Merchant Information     storeNameTh=${random_thai_merchant}
    # log    Real status should be WAITING
    # Response Should Contain Property With Value    content..status    DRAFT
    # log    Real status should be APPROVE
    # Response Should Contain Property With Value    content..tmnStatus    WAITING
    # Response Should Contain Property With Value    content..'trueyouStatus'    WAITING

    # Get Merchant Information     storeNameTh=${random_thai_merchant}
    # log    Real status should be APPROVE
    # Response Should Contain Property With Value    content..status    DRAFT
    # log    Real status should be APPROVE
    # Response Should Contain Property With Value    content..tmnStatus    WAITING
    # log    Real status should be APPROVE
    # Response Should Contain Property With Value    content..'trueyouStatus'    WAITING

    # Get Activation Code Information     activationCode=@{activation_codes}[0]
    # Response Should Contain Property With Value    content..status    WAITING_ACTIVATE



    # Open Browser With Option    ${WEB_AUDIT_URL}    headless_mode=${False}
    # common_web_audit_keyword.Login Web Audit    ${FRAUD_USERNAME}    ${FRAUD_PASSWORD}
    # merchant_list_keyword.Search Merchant    Merchant Name (EN)    ${TEST_DATA_STORE_NAME_EN}
    # merchant_list_keyword.Click Manifier Icon
    # shop_registration_details_keyword.Change Shop Registration Details    APPROVE
    # merchant_list_keyword.Success Alert Show Up
    # merchant_registration_resource_keywords.Get Test Data Merchant Id On Mongo Database    merchant_name.en=${TEST_DATA_STORE_NAME_EN}
    # Create RPP Header
    # rpp_api_keywords.Get True Money Check Approve    id=${TEST_DATA_MERCHANT_MONGO_ID}
    # merchant_resource_keywords.Get Merchant Information    id=${TEST_DATA_MERCHANT_SEQUENCE_ID}
    # Response Correct Code    ${SUCCESS_CODE}
    # Response Should Contain Property With Value    content..status    WAITING
    # Response Should Contain Property With Value    content..tmnStatus    APPROVE
    # Response Should Contain Property With Value    content.."trueyouStatus"    WAITING

    # Wait Until Api Keyword Response Property Change    outlet_v_2_resource_keywords.Get Search Outlet By Id    ${TEST_DATA_OUTLET_ID}    tmnStatus    NA    10
    # outlet_v_2_resource_keywords.Get Search Outlet By Id    ${TEST_DATA_OUTLET_ID}
    # Response Correct Code    ${SUCCESS_CODE}
    # Response Should Contain Property With Value    status    WAITING
    # Response Should Contain Property With Value    tmnStatus    NA
    # Response Should Contain Property With Value    "trueyouStatus"    WAITING
    
    # Open Browser With Option    ${chrm_backoffice_url}    headless_mode=${False}
    # login_keywords.Login Deal Management    natarod    natarod
    # deal_management_common_keywords.Navigate To Deal Management Menu    RPP-EDC    PROSPECT
    # partner_requests_list_keywords.Search For Merchant    ${TEST_DATA_STORE_NAME_EN}
    # partner_requests_list_keywords.Click On Magnifier Icon
    # partner_requests_detail_keywords.Click Button On Partner Requests Detail Page    Next
    # partner_requests_detail_keywords.Click Ok On Popups
    # merchant_detail_keywords.Click Button On Merchant Detail Page    Next
    # campaign_detail_keywords.Input Test Data Campaign Details Popup    ช้อปกับทรู    กาแฟ/ชา    Brief   EDC/GMD    กรุงเทพ-ปริมณฑล
    # Clean Environment
    
    # Wait Until Api Keyword Response Property Change    outlet_v_2_resource_keywords.Get Search Outlet By Id    ${TEST_DATA_OUTLET_ID}    "trueyouStatus"    APPROVE    10
    # merchant_resource_keywords.Get Merchant Information    id=${TEST_DATA_MERCHANT_SEQUENCE_ID}
    # Response Correct Code    ${SUCCESS_CODE}
    # Response Should Contain Property With Value    content..status    APPROVE
    # Response Should Contain Property With Value    content..tmnStatus    APPROVE
    # Response Should Contain Property With Value    content.."trueyouStatus"    APPROVE

    # outlet_v_2_resource_keywords.Get Search Outlet By Id    ${TEST_DATA_OUTLET_ID}
    # Response Correct Code    ${SUCCESS_CODE}
    # Response Should Contain Property With Value    status    APPROVE
    # Response Should Contain Property With Value    tmnStatus    NA
    # Response Should Contain Property With Value    "trueyouStatus"    APPROVE

    # Activate Activation Code And Create Terminal For Outlet    { "activationCode": "@{activation_codes}[0]", "device": "SELLER_APP", "deviceNumber" : "SELLER_0001" }
    # Get Terminal External ID
    
    # Wait Until Api Keyword Response Property Change    Get Activation Code Information    activationCode=@{activation_codes}[0]    content..status    USED    10
    # Response Should Contain Property With Value    content..status    USED
    # Get Terminal Information    @{activation_codes}[0]
    # Response Should Contain Property With Value    .status    ACTIVATE
    # Clean Environment

# TC_020_10884
    # [Documentation]    Ensure user can register new merchant from TSM(Service Type TY+TMN and Device type QRCODE)
    # [Tags]    Sanity    Smoke
    # Login To TrueID            ${username}        ${password}
    # Login To TrueMoney          ${phone}          ${thai_id}
    # Random Merchant Name
    # Create Merchant Information Data          ${random_thai_merchant}       ${random_english_merchant}
    # Create TrueYou Information Data

    # Get Merchant Information     id=${merchant_sequence_id}
    # Get Merchant IDs
    # Response Correct Code    ${SUCCESS_CODE}
    # Response Should Contain Property With Value    content..status    DRAFT
    # Response Should Contain Property With Value    content..tmnStatus    WAITING
    # Response Should Contain Property With Value    content..'trueyouStatus'    WAITING

    # Get True You Merchant ID
    # Set Library Search Order	           SeleniumLibrary
    # Open Browser With Option    ${WEB_AUDIT_URL}    headless_mode=${False}

    # Login Web Audit    ${DRIVER_USERNAME}    ${DRIVER_PASSWORD}
    # Search Merchant By True You Merchant ID    ${TY_MERCHANT_ID}
    # Upload Pictures From Audit Web
    # Wait Until Api Keyword Response Property Change    Get Merchant Information    storeNameTh=${random_thai_merchant}    content..status    WAITING    10
    # Response Should Contain Property With Value    content..status    WAITING
    # Response Should Contain Property With Value    content..tmnStatus    WAITING
    # Response Should Contain Property With Value    content..'trueyouStatus'    WAITING

    # Open Browser With Option    ${WEB_AUDIT_URL}    headless_mode=${False}
    # common_web_audit_keyword.Login Web Audit    ${FRAUD_USERNAME}    ${FRAUD_PASSWORD}
    # merchant_list_keyword.Search Merchant    Merchant Name (EN)    ${TEST_DATA_STORE_NAME_EN}
    # merchant_list_keyword.Click Manifier Icon
    # shop_registration_details_keyword.Change Shop Registration Details    APPROVE
    # merchant_list_keyword.Success Alert Show Up
    # merchant_registration_resource_keywords.Get Test Data Merchant Id On Mongo Database    merchant_name.en=${TEST_DATA_STORE_NAME_EN}
    # Create RPP Header
    # rpp_api_keywords.Get True Money Check Approve    id=${TEST_DATA_MERCHANT_MONGO_ID}
    # merchant_resource_keywords.Get Merchant Information    id=${TEST_DATA_MERCHANT_SEQUENCE_ID}
    # Response Correct Code    ${SUCCESS_CODE}
    # Response Should Contain Property With Value    content..status    WAITING
    # Response Should Contain Property With Value    content..tmnStatus    APPROVE
    # Response Should Contain Property With Value    content.."trueyouStatus"    WAITING

    # Open Browser With Option    ${chrm_backoffice_url}    headless_mode=${False}
    # login_keywords.Login Deal Management    natarod    natarod
    # deal_management_common_keywords.Navigate To Deal Management Menu    RPP-EDC    PROSPECT
    # partner_requests_list_keywords.Search For Merchant    ${TEST_DATA_STORE_NAME_EN}
    # partner_requests_list_keywords.Click On Magnifier Icon
    # partner_requests_detail_keywords.Click Button On Partner Requests Detail Page    Next
    # partner_requests_detail_keywords.Click Ok On Popups
    # merchant_detail_keywords.Click Button On Merchant Detail Page    Next
    # campaign_detail_keywords.Input Test Data Campaign Details Popup    ช้อปกับทรู    กาแฟ/ชา    Brief   EDC/GMD    กรุงเทพ-ปริมณฑล
    # Clean Environment

    # Wait Until Api Keyword Response Property Change    outlet_v_2_resource_keywords.Get Search Outlet By Id    ${TEST_DATA_OUTLET_ID}    "trueyouStatus"    APPROVE    10
    # merchant_resource_keywords.Get Merchant Information    id=${TEST_DATA_MERCHANT_SEQUENCE_ID}
    # Response Correct Code    ${SUCCESS_CODE}
    # Response Should Contain Property With Value    content..status    APPROVE
    # Response Should Contain Property With Value    content..tmnStatus    APPROVE
    # Response Should Contain Property With Value    content.."trueyouStatus"    APPROV4

# TC_O2O_10885
    # [Documentation]    Ensure user can create new outlet for exiting merchant (Service Type TY+TMN and Device type QRCODE)
    # [Tags]    #TBD
# #    This case have to use static created merchant. Because we cannot create merchant with expected statuses via BS service. If try, It will create incorrect test data in the system
    # Get Merchant Information     id=${merchant_sequence_id}
    # Get Merchant IDs
    # Response Correct Code    ${SUCCESS_CODE}
    # Response Should Contain Property With Value    content..status    APPROVE
    # Response Should Contain Property With Value    content..tmnStatus    APPROVE
    # Response Should Contain Property With Value    content.."trueyouStatus"    APPROVE

    # outlet_v_2_resource_keywords.Post Create Outlets And Address Ref Merchant Id    { "outlets": [ { "addresses": [ { "address": "TEST New Multi Outlet", "addressType": "store", "contentType": "test e2e", "districtId": 1, "latitude": "53.286518", "longtitude": "-6.416770", "postCode": "10250", "provinceId": 1, "road": "Ascesnd", "subdistrictId": 6 } ], "contactPersons": [ { "birthDate": "1988-10-10", "email": "automation.pool@ascendcorp.com", "firstNameEn": "Ascend", "firstNameTh": "แอสเซนด์", "gender": "Female", "lastNameEn": "Only", "lastNameTh": "ออนลี่", "mobileNo": "66-864403645", "occupation": "Testing", "passportNo": null, "phoneNo": "020000000", "refId": 1234567890123, "refType": "CID", "thaiId": "${ACCOUNT_VALUE}", "titleId": 2, "titleOtherName": null } ], "merchantId": "${TEST_DATA_MERCHANT_SEQUENCE_ID}", "franchisee": true, "headQuarter": true, "outletDetail": "Test Outlet details", "outletNameEn": "${outlet_name_en}", "outletNameTh": "ร้านแพนเค้กออโตเมทพูล", "registerChannel": "app_sale_agent", "saleId": "0002", "status": "DRAFT", "tmnOutletId": null, "tmnStatus": "WAITING", "trueyouStatus": "WAITING" }] }
    # Get Outlet Ids Test Data
    # Response Correct Code    ${CREATED_CODE}
    # Response Should Contain Property With Value    outlets..outletNameEn    ${outlet_name_en}

    # Wait Until Api Keyword Response Property Change    outlet_v_2_resource_keywords.Get Search Outlet By Id    ${TEST_DATA_OUTLET_ID}    "trueyouStatus"    APPROVE    10
    # Response Correct Code    ${SUCCESS_CODE}
    # Response Should Contain Property With Value    status    APPROVE
    # Response Should Contain Property With Value    tmnStatus    NA
    # Response Should Contain Property With Value    "trueyouStatus"    APPROVE

    # Create And Get Activation Code ID By Outlet ID    ${outlet_id}    { "amount" : 2, "notification" : { "email" : "patthamaporn.kam@ascendcorp.com", "mobile": "66-864403645", "isGroup": false, "receiver": "CUSTOM"} }
    # Wait Until Api Keyword Response Property Change    Get Activation Code Information    activationCode=@{activation_codes}[0]    content..status    WAITING_ACTIVATE    10
    # Response Should Contain Property With Value    content..status    WAITING_ACTIVATE

    # Activate Activation Code And Create Terminal For Outlet    { "activationCode": "@{activation_codes}[0]", "device": "SELLER_APP", "deviceNumber" : "SELLER_0001" }
    # Get Terminal External ID

    # Wait Until Api Keyword Response Property Change    Get Activation Code Information    activationCode=@{activation_codes}[0]    content..status    USED    10
    # Response Should Contain Property With Value    content..status    USED
    # Get Terminal Information    @{activation_codes}[0]
    # Response Should Contain Property With Value    .status    ACTIVATE
    # Clean Environment
