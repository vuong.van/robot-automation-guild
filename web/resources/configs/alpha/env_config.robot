*** Variables ***
${BROWSER}        Chrome
${DELAY}          0.5
${ADMIN_TOOLS_URL}      https://alpha-backoffice.weomni-test.com/
${WE_PLATFROM_URL}    https://alpha-platform.weomni-test.com
${AUDIT_WEB_URL}      http://saleapp-audit-alpha.eggdigital.com
${IMPLICIT_WAIT}    60s