*** Variables ***
${ddl_search_from_on_web_audit_merchant_list_page}    id:searching
${txt_search_value_on_web_audit_merchant_list_page}    //input[@id="data_searching"]
${txt_start_date_on_web_audit_merchant_list_page}    id:start_date
${txt_end_date_on_web_audit_merchant_list_page}    id:end_date
${btn_manifier_on_web_audit_merchant_list_page}    //span[@class=" fa fa-search"]//ancestor::a
${sta_alert_on_web_audit_merchant_list_page}    //div[@class="alert alert-success alert-dismissible"]
