*** Variables ***
${lbl_project_id_path}    //div/dl/dd[1]/span
${ddl_record_per_page}    //div[@class='d-flex justify-content-between']//select
${lbl_column_header_path}    //table[@class='table table-striped']//thead//tr
#Dynamic locators
${lbl_navigate_link_menu}    //a[@class='nav-link']//span[contains(text(),'_DYNAMIC_0')]
${lbl_page_title_path}    //h2[@id='page-heading']//span[contains(text(),'_DYNAMIC_0')]
${lbl_sort_column_header_path}    //span[contains(text(),'_DYNAMIC_0')]
${ico_sort_path}    //th[@jhisortby ='_DYNAMIC_0']/fa-icon/*[@data-icon='_DYNAMIC_1']
${lbl_transaction_list_path}    //tr[_DYNAMIC_0]/td[_DYNAMIC_1]
${lbl_index_column_path}    //th[_DYNAMIC_0]/span
