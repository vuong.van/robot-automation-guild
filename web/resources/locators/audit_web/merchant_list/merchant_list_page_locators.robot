*** Variables ***
${btn_searching}    xpath=//button[@type= 'submit']
${ddl_searching_by}    id=searching
${icn_magnifier_of_top_search_result}    xpath=//table//a
${icn_pencil_of_top_search_result}    xpath=//td[@data-dt-column="11"]/a
${icn_qr_code_of_top_search_result}    xpath=//td[@data-dt-column="12"]/a
${lbl_top_table_row}    xpath=//tbody/tr
#Add Delivery & Pickup Status Modal
${mdl_delivery_and_pickup_status}    xpath=//div[@class="modal-content"]
${ddl_status}    xpath=//select[@name="status" and @required]
${ddl_action_by}    xpath=//select[@name="updated_by"]
${btn_save}    xpath=//button[@type="submit" and text()="Save"]
${tbd_delivery_and_pickup_list}    xpath=//div[@class="modal-content"]//tbody
${icn_exit_modal}    xpath=//button[@data-dismiss="modal"]
#Dynamic locators
${search_field}    xpath=//*[contains(text(),"_DYNAMIC_0")]//ancestor::div[contains(@class,"col-md-3")]//*[@class="form-control"]