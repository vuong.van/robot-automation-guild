*** Variables ***
${btn_login}    //button[@class='btn btn-primary btn-lg']//span[contains(text(),'Sign in')]
${txt_user_name}    //input[@id='username']
${txt_password}    //input[@id='password']
${btn_submit_login}    //span[text()='Log in']
${lbl_administrator}    //h1[contains(text(),'Admintools')]