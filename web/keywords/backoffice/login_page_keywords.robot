*** Settings ***
Resource    ../../resources/locators/backoffice/login_page_locators.robot

*** Keywords ***
Login Backoffice
    [Arguments]    ${username}    ${password}
    SeleniumLibrary.Click Element    ${btn_login}
    SeleniumLibrary.Input Text    ${txt_user_name}    ${username}
    SeleniumLibrary.Input Text    ${txt_password}    ${password}
    SeleniumLibrary.Click Element    ${btn_submit_login}

User Login Successfullty
    SeleniumLibrary.Wait Until Element Is Visible    ${lbl_administrator}