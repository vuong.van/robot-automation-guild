*** Settings ***
Resource    ../../../../web/resources/locators/audit_web/merchant_list/merchant_list_page_locators.robot
Resource    ../../../../web/keywords/common/locator_common.robot

*** Keywords ***
Search Merchant
    [Arguments]    &{text_above_search_field_and_values_for_searching}
    :FOR    ${text_above_search_field}    IN    @{text_above_search_field_and_values_for_searching.keys()}
    \    ${search_value} =    Set Variable    ${text_above_search_field_and_values_for_searching}[${text_above_search_field}]
    \    ${searching_data_locator} =    Generate Element From Dynamic Locator    ${search_field}    ${text_above_search_field}
#Check if field type is text or not. If yes, input search value. If not, select dropdown instead
    \    ${type_attribute_value} =    Get Element Attribute    ${searching_data_locator}    type
    \    Run keyword If    '${type_attribute_value}'=='text'    Input Text    ${searching_data_locator}    ${search_value}
    \    Run keyword If    '${type_attribute_value}'!='text'    Select From List By Label    ${searching_data_locator}    ${search_value}
    Click Button    ${btn_searching}

Add Delivery Status Information
    [Arguments]    ${status}    ${action_by}    ${expected_delivery_status}
    Wait Until Element Is Visible    ${mdl_delivery_and_pickup_status}
    Select From List By Label    ${ddl_status}    ${status}
    Select From List By Label    ${ddl_action_by}    ${action_by}
    Wait Until Element Is Enabled    ${btn_save}
    Click Button    ${btn_save}
    Handle Alert
    Wait Until Element Contains   ${tbd_delivery_and_pickup_list}    ${status}
    Click Element    ${icn_exit_modal}
    Wait Until Keyword Succeeds    5s    1s    Element Should Contain   ${lbl_top_table_row}    ${expected_delivery_status}

Click Magnifier Icon On Top Of Search Result
    Click Element    ${icn_magnifier_of_top_search_result}

Click Pencil Icon On Top Of Search Result
    Click Element    ${icn_pencil_of_top_search_result}