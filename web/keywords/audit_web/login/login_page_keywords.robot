*** Settings ***
Resource    ../../../../web/resources/locators/audit_web/login/login_page_locators.robot

*** Keywords ***
Login To Audit Web
    [Arguments]    ${username}    ${password}
    Input Text    ${txt_username}    ${username}
    Input Text    ${txt_password}    ${password}
    Click Element    ${btn_signin}
    Wait Until Page Contains    Login Success

