*** Settings ***
Resource    ../../../../web/keywords/common/list_common.robot
Resource    ../../../../web/keywords/common/locator_common.robot
Resource    ../../../../web/resources/locators/we_platform/projects/projects_page_locators.robot
Resource    ../../../../web/resources/locators/we_platform/payment/payment_transaction_list_and_search_locators.robot

*** Variables ***
${default_row_number}    1
${maximum_column_number}    6

*** Keywords ***
Verify Project Name Is Not Display
    [Arguments]    ${project_name}
    Wait Until Page Contains Element    ${btn_create_project}
    ${lbl_project_locator}=    Generate Element From Dynamic Locator    ${lbl_project_name}    ${project_name}
    Page Should Not Contain Element    ${lbl_project_locator}    5s

Verify Left Navigation Menu Is Not Display
    [Arguments]    ${navigate_menu_name}
    ${lbl_navigate_link_locator}=    Generate Element From Dynamic Locator    ${lbl_navigate_link_menu}    ${navigate_menu_name}
    Page Should Not Contain Element    ${lbl_navigate_link_locator}    5s

Navigate To Left Menu
    [Arguments]    ${navigate_menu_name}
    ${lbl_navigate_link_locator}=    Generate Element From Dynamic Locator    ${lbl_navigate_link_menu}    ${navigate_menu_name}
    Wait Until Element Is Visible    ${lbl_navigate_link_locator}    5s
    Click Element    ${lbl_navigate_link_locator}

Verify Default Page Title Information
    [Arguments]    ${page_title_name}
    ${lbl_page_title}=    Generate Element From Dynamic Locator    ${lbl_page_title_path}     ${page_title_name}
    Wait Until Page Contains Element    ${lbl_page_title}

Verify Page Url
    [Arguments]    ${expected_project_id}    ${expected_param}
    ${current_url}=   Get Location
    @{split_current_url}=     Split String    ${current_url}    /
    ${current_project_id}=    Set Variable    @{split_current_url}[5]
    ${current_param}=    Set Variable    @{split_current_url}[6]
    Should Be True    '${expected_project_id}'=='${current_project_id}'
    Should Be True    '${expected_param}'=='${current_param}'

Verify Number Of Record Per Page
    [Arguments]    ${expected_record_number}
    ${current_record_number}=   Get Selected List Value    ${ddl_record_per_page}
    Should Be True    '${expected_record_number}'=='${current_record_number}'

Verify Column Header Name
    [Arguments]    ${expected_column_name}
    Element Should Contain    ${lbl_column_header_path}    ${expected_column_name}

Click Column Header For Sorting
    [Arguments]    ${expected_column_name}    ${expected_sorted_by_information}    ${expected_action}
    Get Column Number From Column Name    ${expected_column_name}
    ${lbl_column_header}=    Generate Element From Dynamic Locator    ${lbl_sort_column_header_path}    ${expected_column_name}
    ${ico_sort_by}=    Generate Element From Dynamic Locator    ${ico_sort_path}    ${expected_sorted_by_information}    ${expected_action}
    ${lbl_transaction_list}=    Generate Element From Dynamic Locator    ${lbl_transaction_list_path}    ${default_row_number}    ${COLUMN_NUMBER}
    ${before_sort_value}   Get Text     ${lbl_transaction_list}
    Click Element    ${lbl_column_header}
    Wait Until Page Contains Element    ${ico_sort_by}
    Wait Until Keyword Succeeds    5s    0.5s    Display Value List After Sorting Is Successful    ${lbl_transaction_list}    ${before_sort_value}

Display Value List After Sorting Is Successful
    [Arguments]    ${lbl_transaction_list}    ${before_sort_value}
    ${after_sort_value}   Get Text     ${lbl_transaction_list}
    Should Not Be Equal    ${before_sort_value}    ${after_sort_value}

Compare Value List After Sorting Is Correct
    [Arguments]    ${expected_column_name}
    @{actual_list}=   Create List
    :FOR    ${index}    IN RANGE    1    11
    \    ${row_number}=    Convert To String    ${index}
    \    ${column_number}=    Convert To String    ${COLUMN_NUMBER}
    \    ${lbl_transaction_list}=    Generate Element From Dynamic Locator    ${lbl_transaction_list_path}    ${row_number}    ${COLUMN_NUMBER}
    \    ${row_value}   Get Text     ${lbl_transaction_list}
    \    Append To List  ${actual_list}  ${row_value}
    Ascending Sort From List Of Values    ${actual_list}
    Lists Should Be Equal    ${actual_list}    ${SORT_LIST}

Get Column Number From Column Name
    [Arguments]    ${column_name}
    :FOR    ${index}    IN RANGE    1    ${maximum_column_number}
    \    ${COLUMN_NUMBER}=    Convert To String    ${index}
    \    ${index_column_name}=    Generate Element From Dynamic Locator    ${lbl_index_column_path}    ${COLUMN_NUMBER}
    \    ${actual_column_name}   Get Text     ${index_column_name}
    \    Exit For Loop If    '${column_name}'=='${actual_column_name}'
    Set Suite Variable    ${COLUMN_NUMBER}
