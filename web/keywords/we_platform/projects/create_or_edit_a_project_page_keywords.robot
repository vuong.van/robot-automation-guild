*** Settings ***
Resource    ../../../../web/resources/locators/we_platform/projects/create_or_edit_a_project_page_locators.robot
Resource    ../../../../web/keywords/we_platform/projects/projects_page_keywords.robot

*** Keywords ***
Create Project
    [Arguments]    ${project_name}    ${project_description}    ${project_code}
    Input Text    ${txt_name}    ${project_name}
    Input Text    ${txt_descrition}    ${project_description}
    Input Text    ${txt_code}    ${project_code}
    Click Element    ${btn_save}
    Wait Until Project Is Created    ${project_name}

Edit Project Information
    [Arguments]    ${project_description}    ${project_code}
    Input Text    ${txt_descrition}    ${project_description}
    Input Text    ${txt_code}    ${project_code}
    Click Element    ${btn_save}

