*** Settings ***
Resource    ../../../../web/resources/locators/we_platform/projects/welcome_to_projects_page_locators.robot

*** Keywords ***
Verify Project Information
    [Arguments]    ${project_name}    ${project_description}    ${project_code}
    ${lbl_project_details_txt}=    Get Text    ${lbl_project_details}
    Wait Until Element Contains    ${lbl_project_details}    Name${\n}${project_name}${\n}Description${\n}${project_description}${\n}Code${\n}${project_code}

Select To Edit Current Project
    Click Element    ${btn_edit_project}

Select To Back To The Project Page
    Click Element    ${btn_back}
