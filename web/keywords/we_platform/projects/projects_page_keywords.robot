*** Settings ***
Resource    ../../../../web/resources/locators/we_platform/projects/projects_page_locators.robot
Resource    ../../../../web/keywords/common/locator_common.robot

*** Keywords ***
Delete Project
    [Arguments]    ${project_name}
    Click Element    ${btn_project_toggle_menu}
    Click Element    ${btn_delete_project}
    Wait Until Element Contains    ${lbl_delete_confirmation_message}    Are you sure you want to delete Project    30
    Click Element    ${btn_confirm_delete_project}
    Page Should Contain 	  A Project is deleted

Clear All Projects
    @{elements}=    Get WebElements    ${lbl_project_title}
    :FOR    ${element}    IN    @{elements}
    \    Delete Project    ${element.text}

Wait Until Project Is Created
    [Arguments]    ${project_name}
    ${lbl_project_locator}=    Generate Element From Dynamic Locator    ${lbl_project_name}    ${project_name}
    Wait Until Element Is Visible    ${lbl_project_locator}    30

Select To Create Project
    Wait Until Element Is Enabled    ${btn_create_project}    5
    Click Element    ${btn_create_project}

Browsing Project Information
    [Arguments]    ${project_name}
    ${lbl_project_locator}=    Generate Element From Dynamic Locator    ${lbl_project_name}    ${project_name}
    Click Element    ${lbl_project_locator}

Get Project Id
    ${PROJECT_ID}  Get Text  ${lbl_project_id_path}
    Set Test Variable    ${PROJECT_ID}
