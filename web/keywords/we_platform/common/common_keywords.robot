*** Settings ***
Resource    ../../../../web/keywords/common/locator_common.robot
Resource    ../../../../web/resources/locators/we_platform/common/common_locators.robot

*** Keywords ***
Navigate To Main Menu And Sub Main Menu
    [Arguments]    ${main_menu_name}    ${sub_menu_name}
    ${main_menu_locator}=    Generate Element From Dynamic Locator    ${lnk_main_menu}    ${main_menu_name}
    ${sub_menu_locator}=    Generate Element From Dynamic Locator    ${lnk_sub_menu}    ${sub_menu_name}
    Click Element    ${main_menu_locator}
    Click Element    ${sub_menu_locator}
