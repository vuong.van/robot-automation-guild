*** Settings ***
Resource    ../../../../web/resources/locators/we_platform/login/login_page_locators.robot

*** Keywords ***
Login To We Platform Website
    [Arguments]    ${we_platform_username}    ${we_platform_password}
    Click Element    ${btn_signin}
    Input Text    ${txt_username}    ${we_platform_username}
    Input Text    ${txt_password}    ${we_platform_password}
    Click Element    ${btn_signin_modal}
    Wait Until Element Is Not Visible    ${mdl_login}    30

