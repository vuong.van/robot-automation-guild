*** Settings ***
Resource    ../../../keywords/saleapp/web_audit/common_web_audit_keyword.robot
Resource    ../../../resources/configs/alpha/test_accounts.robot
Resource    ../../../resources/locators/saleapp/webaudit/index_page.robot
Resource    ../../../resources/locators/saleapp/webaudit/merchant_detail_page.robot
Library    SeleniumLibrary    timeout=5s    implicit_wait=2s

*** Variables ***
${merchant_img_path}    ${CURDIR}/Merchant.jpg
${outlet_img_path}    ${CURDIR}/Outlet.jpeg
${identity_img_path}    ${CURDIR}/Identity.jpeg

*** Keywords ***
Search Merchant By True You Merchant ID
    [Arguments]    ${TY_merchant_ID}
    select from list by label    ${searching_dropdown}    TrueYou MID
    input text    ${data_searching_textfield}    ${TY_merchant_ID}
    click button    ${searching_button}
    click element    ${top_search_result}

Upload Pictures From Audit Web
    log    ${merchant_img_path}
    log    ${outlet_img_path}
    Wait Until Element Is Enabled    xpath=.//button[@class="btn btn-block btn-success" and @id="btn-submit"]    120
    choose file    ${1st_merchant_img_browse_button}     ${merchant_img_path}
    Wait Until Element Is Enabled    xpath=.//button[@class="btn btn-block btn-success" and @id="btn-submit"]    120
    choose file    ${2nd_merchant_img_browse_button}     ${merchant_img_path}
    Wait Until Element Is Enabled    xpath=.//button[@class="btn btn-block btn-success" and @id="btn-submit"]    120
    choose file    ${3rd_merchant_img_browse_button}     ${merchant_img_path}
    Wait Until Element Is Enabled    xpath=.//button[@class="btn btn-block btn-success" and @id="btn-submit"]    120
    choose file    ${1st_outlet_img_browse_button}     ${outlet_img_path}
    Wait Until Element Is Enabled    xpath=.//button[@class="btn btn-block btn-success" and @id="btn-submit"]    120
    choose file    ${2nd_outlet_img_browse_button}     ${outlet_img_path}
    Wait Until Element Is Enabled    xpath=.//button[@class="btn btn-block btn-success" and @id="btn-submit"]    120
    choose file    ${3rd_outlet_img_browse_button}     ${outlet_img_path}
    Wait Until Element Is Enabled    xpath=.//button[@class="btn btn-block btn-success" and @id="btn-submit"]    120
    choose file    ${1st_identity_img_browse_button}     ${identity_img_path}
    Wait Until Element Is Enabled    xpath=.//button[@class="btn btn-block btn-success" and @id="btn-submit"]    120
    choose file    ${2nd_identity_img_browse_button}     ${identity_img_path}
    Wait Until Element Is Enabled    xpath=.//button[@class="btn btn-block btn-success" and @id="btn-submit"]    120
    choose file    ${3rd_identity_img_browse_button}     ${identity_img_path}
    Capture Page Screenshot
    Wait Until Element Is Enabled    xpath=.//button[@class="btn btn-block btn-success" and @id="btn-submit"]    120
    click element    ${submit_merchant_imgs_button}
    Wait Until Page Contains    Save draft success    120


