*** Settings ***
Resource    ../../../resources/locators/saleapp/webaudit/login_page.robot

*** Keywords ***
Login Web Audit
    [Arguments]    ${user_name}    ${password}
    Set Library Search Order    SeleniumLibrary
    Input Text    username   ${username}
    Input Text    password   ${password}
    Click Button    ${submit_button}
