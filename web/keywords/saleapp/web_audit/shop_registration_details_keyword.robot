*** Settings ***
Resource    ../../../resources/locators/saleapp/webaudit/shop_registration_details_page.robot

*** Keywords ***
Change Shop Registration Details
    [Arguments]    ${true_money_status}
    Select From List By Label    ${ddl_true_money_status}    ${true_money_status}
    Click Element    ${btn_owner_information_collect_data}