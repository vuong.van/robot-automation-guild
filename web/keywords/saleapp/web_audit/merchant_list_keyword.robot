*** Settings ***
Resource    ../../../resources/locators/saleapp/webaudit/merchant_list_page.robot
Resource    ../../common/web_common.robot

*** Keywords ***
Search Merchant
    [Arguments]    ${search_from}    ${search_value}
    SeleniumLibrary.Select From List By Label    ${ddl_search_from_on_web_audit_merchant_list_page}    ${search_from}
    SeleniumLibrary.Input Text    ${txt_search_value_on_web_audit_merchant_list_page}    ${search_value}
    SeleniumLibrary.Clear Element Text    ${txt_end_date_on_web_audit_merchant_list_page}
    SeleniumLibrary.Input Text    ${txt_start_date_on_web_audit_merchant_list_page}    02/08/2018 00:00
    SeleniumLibrary.Click Button    Apply
    SeleniumLibrary.Input Text    ${txt_end_date_on_web_audit_merchant_list_page}    02/08/2022 00:00
    SeleniumLibrary.Click Button    Search

Click Manifier Icon
    SeleniumLibrary.Click Element    ${btn_manifier_on_web_audit_merchant_list_page}

Success Alert Show Up
    SeleniumLibrary.Wait Until Element Is Visible    ${sta_alert_on_web_audit_merchant_list_page}