*** Settings ***
Resource    ../../resources/locators/retail_admin_portal/merchant_profile_page.robot

*** Keywords ***
Click Update Merchant Kyb Link
    SeleniumLibrary.Click Element    ${lnk_update_merchant_kyb}

Update Kyb Status
    [Arguments]    ${kyb_status}
    SeleniumLibrary.Input Text    ${txt_kyb_status}    ${kyb_status}
    SeleniumLibrary.Press Key    ${txt_kyb_status}    \\13
    SeleniumLibrary.Click Element    ${btn_update_kyb_status}
    SeleniumLibrary.Wait Until Element Is Visible    ${btn_update_status_confirm_dialog}
    SeleniumLibrary.Click Element    ${btn_update_status_confirm_dialog}
    SeleniumLibrary.Wait Until Element Is Visible    ${tbl_contract_list}