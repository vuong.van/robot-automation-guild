*** Settings ***
Resource    ../../resources/locators/retail_admin_portal/login_page.robot

*** Keywords ***
Login True Money Admin Portal
    [Arguments]    ${user_name}    ${password}
    SeleniumLibrary.Input Text    username   ${username}
    SeleniumLibrary.Input Text    password   ${password}
    SeleniumLibrary.Click Element    ${btn_sign_in_true_money_admin_portal}