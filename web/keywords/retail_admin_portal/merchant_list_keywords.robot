*** Settings ***
Resource    ../../resources/locators/retail_admin_portal/merchant_list_page.robot

*** Keywords ***
Search For Merchant
    [Arguments]    ${search_by}    ${search_value}
    SeleniumLibrary.Wait Until Element Is Not Visible    ${prg_loading_icon}
    SeleniumLibrary.Select From List By Label    ${ddl_search_by}    ${search_by}
    SeleniumLibrary.Input Text    ${txt_search_value}    ${search_value}
    SeleniumLibrary.Click Element    ${btn_search}

Click Approve Link
    SeleniumLibrary.Wait Until Element Is Not Visible    ${prg_loading_icon}
    SeleniumLibrary.Click Element    ${lnk_approve}