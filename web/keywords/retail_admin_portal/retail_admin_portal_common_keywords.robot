*** Settings ***
Resource    ../../resources/locators/retail_admin_portal/retail_admin_portal_common_page.robot
Resource    ../common/web_common.robot

*** Keywords ***
Navigate To True Money Admin Portal Menu
    [Arguments]    ${menu}    ${sub_menu}
    ${menu_locator}=    Generate Element From Dynamic Locator    ${mnu_menu_true_money_admin_portal}    ${menu}
    SeleniumLibrary.Wait Until Element Is Visible    ${menu_locator}
    SeleniumLibrary.Click Element    ${menu_locator}
    ${sub_menu_locator}=    Generate Element From Dynamic Locator    ${mnu_sub_true_money_admin_portal}    ${menu}    ${sub_menu}
    SeleniumLibrary.Wait Until Element Is Visible    ${sub_menu_locator}
    SeleniumLibrary.Click Element    ${sub_menu_locator}