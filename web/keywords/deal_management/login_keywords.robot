*** Settings ***
Resource    ../../resources/locators/deal_management/login_page.robot

*** Keywords ***
Login Deal Management
    [Arguments]    ${user_name}    ${password}
    SeleniumLibrary.Input Text    username   ${username}
    SeleniumLibrary.Input Text    password   ${password}
    SeleniumLibrary.Click Element    ${btn_submit_login_true_you}