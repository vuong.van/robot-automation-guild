*** Settings ***
Resource    ../../resources/locators/deal_management/partner_requests_list_page.robot
Resource    deal_management_common_keywords.robot

*** Keywords ***
Search For Merchant
    [Arguments]    ${merchant_name}
    Wait Progress Bar Disappear
    SeleniumLibrary.Input Text    ${txt_merchant_name}   ${merchant_name}
    SeleniumLibrary.Click Button    ${btn_partner_requests_list_search}

Click On Magnifier Icon
    Wait Progress Bar Disappear
    SeleniumLibrary.Click Element    ${btn_magnifier_icon}