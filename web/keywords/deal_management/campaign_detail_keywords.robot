*** Settings ***
Resource    ../../resources/locators/deal_management/campaign_detail_page.robot
Resource    ../../../web/keywords/deal_management/deal_management_common_keywords.robot
Resource    ../common/locator_common.robot

*** Keywords ***
Input Test Data Campaign Details Popup
    [Arguments]    ${campaign_name}    ${food_type}    ${campaign_status}    ${lot}    ${coverage_area}
    Wait Progress Bar Disappear
    Select Campaign Name On Campaign Detail Popup    ${campaign_name}
    Select Food Type On Campaign Detail Popup    ${food_type}
    Select Campaign Status On Campaign Detail Popup    ${campaign_status}
    Wait Block Message Bar Disappear
    Select Coverage Area On Campaign Detail Popup    ${coverage_area}
    Select Lot On Campaign Detail Popup    ${lot}
    SeleniumLibrary.Click Button    Save
    Wait Until Element Is Disappear    ${bar_please_wait}

Select Campaign Name On Campaign Detail Popup
    [Arguments]    ${campaign_name}
    SeleniumLibrary.Click Element    ${btn_campaign_name}
    ${lbl_campaign_name}=    Generate Element From Dynamic Locator    ${lbl_dropdown_item}    ${campaign_name}
    SeleniumLibrary.Click Element    ${lbl_campaign_name}

Select Food Type On Campaign Detail Popup
    [Arguments]    ${food_type}
    SeleniumLibrary.Click Element    ${btn_food_type}
    ${lbl_dropdown_item}=    Generate Element From Dynamic Locator    ${lbl_dropdown_item}    ${food_type}
    SeleniumLibrary.Click Element    ${lbl_dropdown_item}

Select Campaign Status On Campaign Detail Popup
    [Arguments]    ${campaign_status}
    SeleniumLibrary.Click Element    ${btn_campaign_status}
    ${lbl_dropdown_item}=    Generate Element From Dynamic Locator    ${lbl_dropdown_item}    ${campaign_status}
    SeleniumLibrary.Click Element    ${lbl_dropdown_item}

Select Lot On Campaign Detail Popup
    [Arguments]    ${lot}
    SeleniumLibrary.Click Element    ${btn_lot}
    ${lbl_dropdown_item}=    Generate Element From Dynamic Locator    ${lbl_dropdown_item}    ${lot}
    SeleniumLibrary.Wait Until Element Is Visible    ${lbl_dropdown_item}
    SeleniumLibrary.Click Element    ${lbl_dropdown_item}

Select Coverage Area On Campaign Detail Popup
    [Arguments]    ${coverage_area}
    SeleniumLibrary.Click Element    ${btn_coverage_area}
    ${lbl_dropdown_item}=    Generate Element From Dynamic Locator    ${lbl_dropdown_item}    ${coverage_area}
    SeleniumLibrary.Click Element    ${lbl_dropdown_item}