*** Keywords ***
Click Button On Partner Requests Detail Page
    [Arguments]    ${button_name}
    SeleniumLibrary.Click Button    ${button_name}

Click Ok On Popups
    SeleniumLibrary.Handle Alert    ACCEPT
    SeleniumLibrary.Handle Alert    ACCEPT