*** Settings ***
Resource    ../../resources/locators/deal_management/deal_management_common_page.robot
Resource    ../common/web_common.robot

*** Keywords ***
Navigate To Deal Management Menu
    [Arguments]    ${menu}    ${sub_menu}
    ${menu_locator}=    Generate Element From Dynamic Locator    ${mnu_menu_deal_management}    ${menu}
    SeleniumLibrary.Wait Until Element Is Visible    ${menu_locator}
    SeleniumLibrary.Mouse Over    ${menu_locator}
    ${sub_menu_locator}=    Generate Element From Dynamic Locator    ${mnu_sub_menu_deal_management}    ${menu}    ${sub_menu}
    SeleniumLibrary.Wait Until Element Is Visible    ${sub_menu_locator}
    SeleniumLibrary.Click Element    ${sub_menu_locator}

Wait Progress Bar Disappear
    Wait Until Element Is Disappear    ${bar_please_wait}

Wait Block Message Bar Disappear
    Wait Until Element Is Disappear    ${bar_block_page}