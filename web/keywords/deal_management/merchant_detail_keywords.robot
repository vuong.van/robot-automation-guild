*** Settings ***
Resource    deal_management_common_keywords.robot

*** Keywords ***
Click Button On Merchant Detail Page
    [Arguments]    ${button_name}
    Wait Progress Bar Disappear
    SeleniumLibrary.Click Button    ${button_name}