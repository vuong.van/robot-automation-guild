*** Settings ***
Resource    ../../resources/locators/admintools/sms_dashboard_locators.robot
Resource    ../common/locator_common.robot

*** Keywords ***
Click Edit Button By Sms Title
    [Arguments]    ${sms_title}
    ${locator}=    locator_common.Generate Element From Dynamic Locator    ${btn_edit_by_sms_title}    ${sms_title}
    SeleniumLibrary.Click Element    ${locator}

Submit Sender Name And SMS Status
    [Arguments]    ${sender_name_status}    ${sms_status}
    SeleniumLibrary.Select From List By Value    ${ddl_approve_sms}    ${sms_status}
    SeleniumLibrary.Click Element    ${btn_save}