*** Settings ***
Documentation    Verify users can upload / add delivery status / and approve the brand new merchant
Resource    ../../../web/resources/init.robot
Resource    ../../../api/resources/import.robot
Resource    ../../../api/keywords/rpp_merchant_bs/login_keywords.robot
Resource    ../../../api/keywords/rpp_merchant_bs/merchant_keywords.robot
Resource    ../../../api/keywords/common/date_time_common.robot
Resource    ../../../web/keywords/common/web_common.robot
Resource    ../../../web/keywords/audit_web/common/common_keywords.robot
Resource    ../../../web/keywords/audit_web/login/login_page_keywords.robot
Resource    ../../../web/keywords/audit_web/merchant_list/merchant_list_page_keywords.robot
Resource    ../../../web/keywords/audit_web/merchant_details/merchant_details_page_keywords.robot
Test Setup    Run Keywords    Get Date With Format And Increment    %d/%m/%Y    1 day    AND    Open Browser With Option    ${AUDIT_WEB_URL}    headless_mode=${True}    AND    Set Browser Implicit Wait    ${IMPLICIT_WAIT}
Test Teardown    Run Keywords    Delete All Sessions    AND    Clean Environment

*** Variables ***
${merchant_name_en}    Test_verify_merchant_
${merchant_name_th}    ทดสอบ_ตรวจสอบเมอชานต์_
${delivery_status}    DELIVERED (ส่งสินค้าสำเร็จ)
${action_by}    Sendit (Sendit)
${expected_delivery_status}    DELIVERED
${registration_type}    Makro

*** Test Cases ***
TC_O2O_11606
    [Documentation]    Verify Sale user can upload merchant pictures, Sendit user can add QR code delivery information, and Fraud user approve the brand new merchant
    [Tags]    Sanity    Smoke    Robot
#Create merchant via BS service
    Authorize BS Service For Create Merchant In Audit Web    { "username":"egg1", "password":"${SALE_BS_PASSWORD}", "user_type":"sale" }
    Prepare Body For Create Merchant    ${merchant_name_en}    ${merchant_name_th}
    Post Api Create Merchant    ${FINAL_BODY}
#Sale user upload merchant images
    Login To Audit Web   ${AUDIT_WEB_SALE_USERNAME}    ${AUDIT_WEB_SALE_PASSWORD}
    Search Merchant    ค้นหา=ค้นหาชื่อร้านภาษาไทย    กรุณาใส่คำที่ต้องการค้นหา=${merchant_name_th}${rand_num}
    Click Magnifier Icon On Top Of Search Result
    Select Merchant Registration Type    ${registration_type}
    Search Merchant     ค้นหา=ค้นหาชื่อร้านภาษาไทย    กรุณาใส่คำที่ต้องการค้นหา=${merchant_name_th}${rand_num}
    Click Magnifier Icon On Top Of Search Result
    Upload Merchant Images
    Logout From Audit Web
#Sendit user update QR code delivery status
    Login To Audit Web   ${AUDIT_WEB_SENDIT_USERNAME}    ${AUDIT_WEB_SENDIT_PASSWORD}
    Search Merchant   ค้นหา=ค้นหาชื่อร้านภาษาไทย    กรุณาใส่คำที่ต้องการค้นหา=${merchant_name_th}${rand_num}    จนถึงวันที่=${DESIRED_DATE} 12:00
    Click Pencil Icon On Top Of Search Result
    Add Delivery Status Information    ${delivery_status}    ${action_by}    ${expected_delivery_status}
    Logout From Audit Web
#Fraud user approve merchant TMN status
    Login To Audit Web   ${AUDIT_WEB_FRAUD_USERNAME}    ${AUDIT_WEB_FRAUD_PASSWORD}
    Search Merchant    Search From=Merchant Name (TH)    Search Value=${merchant_name_th}${rand_num}    Registration Date (To)=${DESIRED_DATE} 12:00
    Click Magnifier Icon On Top Of Search Result
    Verify Merchant Detail Page Should Contain Information    ชื่อร้าน (TH)   ${merchant_name_th}${rand_num}
    Verify Merchant Detail Page Should Contain Information    อีเว้นท์ / ประเภทการสมัคร    ${registration_type}
    Verify Merchant Detail Page Should Contain Information    สถานะการสมัคร    WAITING
    Verify Merchant Detail Page Should Contain Information    โดย    sendit
    Update TMN Status    APPROVE
    Search Merchant    Search Value=${merchant_name_th}${rand_num}    Registration Date (To)=${DESIRED_DATE} 12:00
    Click Magnifier Icon On Top Of Search Result
    Verify Merchant Detail Page Should Contain Information    ชื่อร้าน (TH)   ${merchant_name_th}${rand_num}
    Verify Merchant Detail Page Should Contain Information    สถานะการสมัคร    APPROVE
    Verify Merchant Detail Page Should Contain Information    สถานะทรูมันนี่    APPROVE