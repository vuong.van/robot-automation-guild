*** Settings ***
Documentation    Verify user can create / view detail / update and delete own projects

Resource    ../../../resources/init.robot
Test Setup    Open Browser With Option    https://alpha-backoffice.weomni-test.com/    headless_mode=${False}
Test Teardown    Clean Environment

*** Variables ***


*** Test Cases ***
TC_ROBOT_AUTOMATION_GUILD
    [Documentation]    User can login successfully
    [Tags]    Regression