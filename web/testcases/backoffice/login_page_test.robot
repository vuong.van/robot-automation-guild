*** Settings ***
Documentation    Verify User can login backoffice successfully

Library    SeleniumLibrary    10    10
Resource    ../../resources/init.robot
Resource    ../../keywords/backoffice/login_page_keywords.robot
Test Setup    Open Browser With Option    https://alpha-backoffice.weomni-test.com/    headless_mode=${False}
Test Teardown    Clean Environment

*** Variables ***


*** Test Cases ***
TC_ROBOT_AUTOMATION_GUILD
    [Documentation]    User can login successfully
    [Tags]    Regression
    # SeleniumLibrary.Click Element    //button[@class='btn btn-primary btn-lg']//span[contains(text(),'Sign in')]    
    # SeleniumLibrary.Input Text    //input[@id='username']    66639202326
    # SeleniumLibrary.Input Text    //input[@id='password']    TestAscend1234
    # SeleniumLibrary.Click Element    //span[text()='Log in']
    login_page_keywords.Login Backoffice    66639202326    TestAscend1234
    login_page_keywords.User Login Successfullty    