*** Settings ***
Documentation    Verify user can create / view detail / update and delete own projects
Resource    ../../../../web/resources/init.robot
Resource    ../../../../web/keywords/we_platform/login/login_keyword.robot
Resource    ../../../../web/keywords/we_platform/common/common_keywords.robot
Resource    ../../../../web/keywords/we_platform/projects/projects_page_keywords.robot
Resource    ../../../../web/keywords/we_platform/payment/payment_transaction_list_keyword.robot
Test Setup    Run Keywords    Open Browser With Option    ${WE_PLATFROM_URL}    headless_mode=${true}    AND    Login To We Platform Website    ${WE_PLATFORM_PAYMENT_USER}    ${WE_PLATFORM_PAYMENT_PASSWORD}
Test Teardown    Clean Environment

*** Variables ***
${project_name}    WeShop Payment
${payment_page_default_param_url}   payment?page=1&size=50&sort=createdDate,desc
${payment_page_default_record}    50

*** Test Cases ***
TC_O2O_10344
    [Documentation]    User cannot view/search the transaction if not in project member list
    [Tags]    Medium    Regression    Robot
    [Setup]    Run Keywords    Open Browser With Option    ${WE_PLATFROM_URL}    headless_mode=${true}    AND    Login To We Platform Website    ${WE_PLATFORM_USER}    ${WE_PLATFORM_PASSWORD}
    Navigate To Main Menu And Sub Main Menu    Entities    Project
    Verify Project Name Is Not Display    ${project_name}
    Verify Left Navigation Menu Is Not Display    Payment

TC_O2O_10345
    [Documentation]    User can view the transaction if in project member list
    [Tags]    High    Regression    UnitTest    Smoke    Sanity    Robot
    Navigate To Main Menu And Sub Main Menu    Entities    Project
    Browsing Project Information    ${project_name}
    Get Project Id
    Navigate To Left Menu    Payment
    Verify Default Page Title Information    Payment
    Verify Page Url    ${PROJECT_ID}    ${payment_page_default_param_url}
    Verify Number Of Record Per Page    ${payment_page_default_record}
    Verify Column Header Name    Order No
    Verify Column Header Name    Transaction Date/Time
    Verify Column Header Name    Merchant ID
    Verify Column Header Name    Total Amount (THB)
    Verify Column Header Name    Transaction Status

TC_O2O_10346
    [Documentation]    User can sort the transaction for each columns (Page load)
    [Tags]    Low    Regression    Robot
    Navigate To Main Menu And Sub Main Menu    Entities    Project
    Browsing Project Information    ${project_name}
    Get Project Id
    Navigate To Left Menu    Payment
    Verify Default Page Title Information    Payment
    Verify Page Url    ${PROJECT_ID}    ${payment_page_default_param_url}
    Click Column Header For Sorting    Order No    externalTransactionRefId    sort-down
    Compare Value List After Sorting Is Correct    Order No
    Click Column Header For Sorting    Merchant ID    merchantId    sort-down
    Compare Value List After Sorting Is Correct    Merchant ID
    Click Column Header For Sorting    Transaction Status    status    sort-down
    Compare Value List After Sorting Is Correct    Transaction Status
