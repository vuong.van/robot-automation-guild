#!/usr/bin/env bash
set +e
echo -e "Executing robot test"
robot --variable env:${TEST_ENV} --loglevel TRACE -i ${TEST_TYPE} --outputdir ${REPORT_DIR} /robot/${AUTOMATE_TYPE}/testcases/${SERVICE_NAME}

#echo -e "Inserting test results into the Merchant Center DB"
#python insert_automation_result.py "2" ${AUTOMATE_TYPE} ${SERVICE_NAME} ${TEST_TYPE} ${TEST_ENV} ${REPORT_DIR}"/output.xml"