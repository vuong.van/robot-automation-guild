*** Variables ***
${REMOTE_URL_ANDROID}               http://127.0.0.1:4723/wd/hub
${REMOTE_URL_IOS}                   http://127.0.0.1:4725/wd/hub
#TSM
${TSM_PACKAGE}                      com.eggdigital.trueyouedc.staging
${SHORT_WAIT_TIME}                  1
${MEDIUM_WAIT_TIME}                 3
${LONG_WAIT_TIME}    5
${VERY_LONG_WAIT_TIME}    10
${DEFAULT_TIMEOUT}    20
