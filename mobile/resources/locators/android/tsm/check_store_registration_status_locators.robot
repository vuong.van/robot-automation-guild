# ตรวจสอบสถานะการลงทะเบียนร้าน

*** Variables ***
${txt_id_number}    xpath=//*[contains(@resource-id,'edt_merchant_search_input_id')]
${btn_agreed}    xpath=//*[contains(@resource-id,'button_merchant_search')]
${btn_allow_popup}    xpath=//*[contains(@resource-id,'permission_allow_button')]

# Dynamic locators
${lbl_merchant}    xpath=//*[contains(@resource-id,'merchant_name_tv')][@text='_DYNAMIC_0']