*** Variables ***
${ddl_owner_title_name}    xpath=(//*[contains(@resource-id,'tv_content')])[1]
${txt_merchant_thai_lastname}    xpath=(//*[contains(@resource-id,'edt_content')])[1]
${txt_merchant_thai_surname}    xpath=(//*[contains(@resource-id,'edt_content')])[2]
${ddl_province}    xpath=//*[@text='กรุงเทพ']
${txt_search_on_popup}    xpath=//*[contains(@resource-id,'edt_search_address')]
${ddl_district}    xpath=//*[@text='กรุณาเลือกเขต/อำเภอ']
${ddl_sub_district}    xpath=//*[@text='กรุณาเลือกแขวง/ตำบล']
${btn_upload_id_card_picture}    xpath=//*[contains(@resource-id,'owner_info_photo_id_card')]//android.widget.ImageView
${btn_upload_shop_keeper_with_id_card_picture}    xpath=//*[contains(@resource-id,'owner_info_photo_owner_id_card')]//android.widget.ImageView
${btn_camera_upload_picture_popup}    xpath=//*[contains(@resource-id,'dialogInfoNegativeButton')]
${btn_allow_popup}    xpath=//*[contains(@resource-id,'permission_allow_button')]
${btn_take_picture}    xpath=//*[contains(@resource-id,'shutter_button')]
${btn_done_taking_picture}    xpath=//*[contains(@resource-id,'done_button')]
${txt_thai_store_name}    xpath=//*[contains(@resource-id,'edt_merchant_thai_name')]//android.widget.EditText
${txt_english_store_name}    xpath=//*[contains(@resource-id,'edt_merchant_eng_name')]//android.widget.EditText
${chk_use_same_address_with_business_owner}    xpath=//*[contains(@resource-id,'cb_use_owner_address')]
${txt_merchant_phone_number}    xpath=//*[contains(@resource-id,'edt_merchant_phone_number')]//android.widget.EditText
${ddl_merchant_type}    xpath=//*[contains(@resource-id,'tv_merchant_business_type')]//android.widget.TextView[2]
${txa_shop_highlight}    xpath=//*[contains(@resource-id,'edt_highlight_of_shop')]
${btn_upload_store_product_picture}    xpath=//*[contains(@resource-id,'merchant_info_photo_of_merchant_product')]//android.widget.ImageView
${btn_confirmation}    xpath=//*[contains(@resource-id,'btn_confimation')]
${txt_email_address}    xpath=//*[@text='อีเมล*']//following-sibling::android.widget.EditText

#Dynamic Locators
${rdo_owner_title_name}    xpath=//*[contains(@resource-id,'tv_content') and @text='_DYNAMIC_0']
${rdo_merchant_gender}    xpath=//*[contains(@text,'_DYNAMIC_0')]
${ddl_item_on_popup}    xpath=//*[contains(@resource-id,'tv_data_item_content') and @text='_DYNAMIC_0']
${rdo_merchant_type}    xpath=//*[contains(@text,'_DYNAMIC_0')]
${rdo_contact_information}    xpath=//*[contains(@text,'_DYNAMIC_0')]