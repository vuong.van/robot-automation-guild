*** Variables ***
${lbl_sms_remaining_credit}    xpath=//*[contains(@content-desc,'คงเหลือ ')][contains(@content-desc,' เครดิต')]
${btn_create_sms_campaign}    xpath=//*[contains(@content-desc,'close')]
${lbl_sms_credit}    xpath=//*[contains(@content-desc,'SMS เครดิต')]
${btn_arrow_back}    xpath=//android.widget.Button[@content-desc="arrow back "]

# Dynamic Locators
${tbl_cell_content_by_content}    xpath=(//*[@content-desc='_DYNAMIC_0'])[_DYNAMIC_1]