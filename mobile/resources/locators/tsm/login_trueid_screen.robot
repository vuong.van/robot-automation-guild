*** Variables ***
${trueid_login}              &{trueid_login_${OS}}

&{trueid_login_android}    btn_register=id=tutorialRegisterButton
...                        btn_register_next=id=button_tmn_next
...                        txt_username=xpath=//android.view.View[@resource-id="home"]/android.view.View[2]/android.widget.EditText
...                        txt_password=xpath=//android.view.View[@resource-id="home"]/android.view.View[4]/android.widget.EditText
...                        btn_submit_login=xpath=//android.widget.Button[@resource-id="bt_signin"]

&{trueid_login_ios}        btn_register=xpath=//*[@type = 'XCUIElementTypeButton' and @name = 'เปิดร้านใหม่กับ ทรู สมาร์ต เมอร์ชันต์']
...                        btn_register_next=xpath=//*[@type = 'XCUIElementTypeButton' and @name = 'เริ่มดำเนินการ']
...                        txt_username=xpath=//*[@type = 'XCUIElementTypeTextField' and @value = 'เบอร์มือถือ หรือ อีเมล']
...                        txt_password=xpath=//*[@type = 'XCUIElementTypeSecureTextField' and @value = 'อย่างน้อย 8 ตัว อักษร และ ตัวเลข']
...                        btn_submit_login=xpath=//*[@type = 'XCUIElementTypeButton' and @name = 'เข้าสู่ระบบ']

