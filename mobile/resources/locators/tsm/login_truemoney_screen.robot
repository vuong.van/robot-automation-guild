*** Variables ***
${truemoney_login}              &{truemoney_login_${OS}}

&{truemoney_login_android}    txt_phone=xpath=//android.widget.EditText[@text='เบอร์โทรศัพท์']
...                           txt_thai_id=xpath=//android.view.View[7]/android.widget.EditText
...                           btn_submit_login=xpath=//android.widget.Button[@text='เข้าสู่ระบบ']
...                           txt_otp=xpath=//android.widget.EditText
...                           btn_submit_otp=xpath=//android.widget.Button
...                           lbl_no_store=xpath=//*[@text='ไม่มีหน้าร้าน']
...                           btn_allow=id=com.android.packageinstaller:id/permission_allow_button


&{truemoney_login_ios}        txt_phone=xpath=//*[@type = 'XCUIElementTypeTextField']
...                           txt_thai_id=xpath=//*[@value = '9-9999-99999-99-9']
...                           btn_submit_login=xpath=//XCUIElementTypeButton[@name="เข้าสู่ระบบ"]
...                           txt_otp=xpath=//XCUIElementTypeTextField
...                           btn_submit_otp=xpath=//XCUIElementTypeButton[@name="อนุญาตดำเนินการ"]
...                           lbl_no_store=xpath=//*[@type = 'XCUIElementTypeStaticText' and @name = 'ไม่มีหน้าร้าน']
...                           btn_done=xpath=//XCUIElementTypeButton[@name="Done"]



