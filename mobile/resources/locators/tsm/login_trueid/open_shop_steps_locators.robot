# เปิดร้านง่ายๆ ใน 3 ขั้นตอน

*** Variables ***
${open_shop_steps}              &{open_shop_steps_${OS}}

&{open_shop_steps_ios}     btn_embark=accessibility_id=เริ่มดำเนินการ

&{open_shop_steps_android}     btn_embark=xpath=//*[contains(@resource-id,'button_tmn_next')]