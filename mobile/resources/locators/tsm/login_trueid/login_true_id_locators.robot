# เข้าสู่ระบบ

*** Variables ***
${login_true_id}              &{login_true_id_${OS}}

&{login_true_id_android}     txt_user_name=xpath=//android.view.View[@resource-id="home"]/android.view.View[2]/android.widget.EditText
...    txt_password=xpath=//android.view.View[@resource-id="home"]/android.view.View[4]/android.widget.EditText
...    btn_submit_login=xpath=//android.widget.Button[@resource-id="bt_signin"]

&{login_true_id_ios}     txt_user_name=xpath=//XCUIElementTypeOther[@name="tab panel"]/XCUIElementTypeTextField
...    txt_password=xpath=//XCUIElementTypeOther[@name="tab panel"]/XCUIElementTypeSecureTextField
...    btn_submit_login=xpath=//XCUIElementTypeButton[@name="เข้าสู่ระบบ"]