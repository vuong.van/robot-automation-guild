*** Variables ***
${create_trueyou}              &{create_trueyou_${OS}}

&{create_trueyou_android}    btn_apply_trueyou=xpath=//*[@text='สมัครสิทธิพิเศษทรูยู']
...                          chk_discount=xpath=//*[@text='ส่วนลด 10%']
...                          txt_privilege_spec=id=com.eggdigital.trueyouedc.alpha:id/edt_privilege_spec
...                          chk_shop_size=id=com.eggdigital.trueyouedc.alpha:id/rb_shop_size
...                          btn_continue=id=com.eggdigital.trueyouedc.alpha:id/continue_btn
...                          btn_next=id=com.eggdigital.trueyouedc.alpha:id/btn_next
...                          chk_term=xpath=//*[@text='ยอมรับข้อกำหนดและเงื่อนไข']
...                          btn_sign=id=com.eggdigital.trueyouedc.alpha:id/signature_btn
...                          btn_finish=id=com.eggdigital.trueyouedc.alpha:id/btn_finish
...    
&{create_trueyou_ios}        btn_apply_trueyou=xpath=//XCUIElementTypeStaticText[@name="สมัครสิทธิพิเศษทรูยู"]
...                          chk_discount=xpath=//XCUIElementTypeButton[@name="ส่วนลด 10%"]
...                          txt_privilege_spec=xpath=(//XCUIElementTypeCell[@name="privilege"])[1]/XCUIElementTypeTextField
...                          btn_done=xpath=//XCUIElementTypeButton[@name="Toolbar Done Button"]
...                          chk_shop_size=xpath=//XCUIElementTypeButton[@name="ร้านขนาดเล็ก (ไม่เกิน 30 ตรม.)"]
...                          btn_continue=xpath=//XCUIElementTypeButton[@name="ดำเนินการต่อ"]
...                          btn_next=xpath=//XCUIElementTypeButton[@name="ดำเนินการต่อ"]
...                          chk_term=xpath=//XCUIElementTypeButton[@name="ยอมรับข้อกำหนดและเงื่อนไข"]
...                          btn_sign=xpath=//XCUIElementTypeButton[@name="เซ็นต์ลายเซ็นต์"]
...                          btn_finish=xpath=//XCUIElementTypeButton[@name="เสร็จสิ้น"]
...                          btn_next_step=xpath=//XCUIElementTypeButton[@name="ยืนยัน"]
...                          lbl_success=xpath=//XCUIElementTypeStaticText[@name="ท่านได้สมัครเป็นพันธมิตรร้านค้ากับทรูยูเรียบร้อยแล้ว"]
...                          btn_close=xpath=//XCUIElementTypeButton[@name="ปิด"]