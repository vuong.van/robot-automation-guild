*** Variables ***
${create_merchant}              &{create_merchant_${OS}}

&{create_merchant_android}    chk_gender=id=com.eggdigital.trueyouedc.alpha:id/rbt_female
...                           ddl_name_select=xpath=//*[@text='กรุณาเลือกคำนำหน้าชื่อ']
...                           ddl_name_mr=xpath=//*[@text='นาย']
...                           ddl_district=id=tv_district
...                           ddl_district_opt=xpath=//*[@text='ราชเทวี']
...                           ddl_sub_district=id=tv_subdistrict
...                           ddl_sub_district_opt=xpath=//*[@text='ทุ่งพญาไท']
...                           btn_photo_first=id=com.eggdigital.trueyouedc.alpha:id/img_photo
...                           btn_photo_second=xpath=//android.widget.FrameLayout[2]/androidx.recyclerview.widget.RecyclerView/android.widget.FrameLayout/android.widget.ImageView
...                           btn_photo_third=id=com.eggdigital.trueyouedc.alpha:id/img_photo
...                           btn_take_photo=xpath=//*[@text='กล้องถ่ายรูป']
...                           btn_shutter=xpath=//android.widget.ImageView[@content-desc="Shutter"]
...                           btn_done=xpath=//android.widget.ImageButton[@content-desc="Done"]
...                           txt_merchant_th=xpath=//*[@text='สำหรับร้านค้าจดในนามบุคคลธรรมดาเท่านั้น']
...                           txt_merchant_en=xpath=//*[@text='กรอกชื่อร้าน ภาษาอังกฤษ']
...                           chk_use_address=xpath=//*[@text='ใช้ที่อยู่เดียวกับเจ้าของกิจการ']
...                           txt_phone=xpath=//*[@text='กรอกเบอร์โทรศัพท์ร้าน']
...                           ddl_business_type=xpath=//*[@text='กรุณาเลือกประเภทธุรกิจ']
...                           ddl_business_type_opt=xpath=//*[@text='อาหารและเครื่องดื่ม']
...                           btn_allow_permission=id=com.android.packageinstaller:id/permission_allow_button
...                           btn_confirm=id=com.eggdigital.trueyouedc.alpha:id/btn_confimation
...                           btn_thanks=id=com.eggdigital.trueyouedc.alpha:id/btn_close_popup_thank_you



&{create_merchant_ios}        chk_gender=xpath=//*[@type = 'XCUIElementTypeButton' and @name = 'หญิง']
...                           ddl_name_select=xpath=//XCUIElementTypeTextField[@value="กรุณาเลือกคำนำหน้าชื่อ"]
...                           ddl_name_mr=xpath=//XCUIElementTypeButton[@name="เสร็จสิ้น"]
...                           ddl_district=xpath=//XCUIElementTypeTextField[@value="กรุณาเลือกเขต/อำเภอ"]
...                           ddl_district_opt=xpath=//XCUIElementTypeStaticText[@name="พระนคร"]
...                           ddl_sub_district=xpath=//XCUIElementTypeTextField[@value="กรุณาเลือกแขวง/ตำบล"]
...                           ddl_sub_district_opt=xpath=//XCUIElementTypeStaticText[@name="พระบรมมหาราชวัง"]
...                           btn_photo_first=xpath=//XCUIElementTypeCollectionView[@name="ThaiIDCollection"]/XCUIElementTypeCell/XCUIElementTypeOther/XCUIElementTypeOther
...                           btn_photo_second=xpath=//XCUIElementTypeCollectionView[@name="MerchantWithThaiID"]/XCUIElementTypeCell/XCUIElementTypeOther/XCUIElementTypeOther
...                           btn_photo_third=xpath=//XCUIElementTypeCollectionView[@name="ShopInfoPhoto"]/XCUIElementTypeCell/XCUIElementTypeOther/XCUIElementTypeOther
...                           btn_take_photo=xpath=//*[@type = 'XCUIElementTypeButton' and @label = 'อัลบั้ม' and @name = 'อัลบั้ม']
...                           btn_album=xpath=//*[@type = 'XCUIElementTypeButton' and @label = 'อัลบั้ม' and @name = 'อัลบั้ม']
...                           btn_ok=xpath=//*[@type = 'XCUIElementTypeButton' and @label = 'OK' and @name = 'OK']
...                           btn_moment=xpath=//XCUIElementTypeCell[@name="Moments"]
...                           img_photo=xpath=//XCUIElementTypeCell[@name="Photo, Landscape, August 09, 2012, 1:52 AM"]
...                           txt_merchant_th=xpath=//*[@type = 'XCUIElementTypeTextField' and @value = 'สำหรับร้านค้าจดในนามบุคคลธรรมดาเท่านั้น']
...                           txt_merchant_en=xpath=//*[@type = 'XCUIElementTypeTextField' and @value = 'กรอกชื่อร้าน ภาษาอังกฤษ']
...                           btn_done=xpath=//XCUIElementTypeButton[@name="Toolbar Done Button"]
...                           chk_use_address=xpath=//XCUIElementTypeButton[@name="ยอมรับข้อกำหนดและเงื่อนไข"]
...                           txt_phone=xpath=//*[@type = 'XCUIElementTypeTextField' and @value = 'กรอกเบอร์โทรศัพท์ร้าน']
...                           ddl_business_type=xpath=//XCUIElementTypeTextField[@value="กรุณาเลือกประเภทธุรกิจ"]
...                           ddl_business_type_opt=xpath=//XCUIElementTypeButton[@name="เสร็จสิ้น"]
...                           btn_confirm=xpath=//XCUIElementTypeButton[@name="ตกลง"]
...                           btn_thanks=xpath=//XCUIElementTypeButton[@name="ปิด"]