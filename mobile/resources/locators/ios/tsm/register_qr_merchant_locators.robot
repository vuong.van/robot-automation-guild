*** Variables ***
${ddl_owner_title_name}    xpath=//XCUIElementTypeTextField[@value="กรุณาเลือกคำนำหน้าชื่อ"]
${txt_merchant_thai_lastname}    xpath=(//*[contains(@resource-id,'edt_content')])[1]
${txt_merchant_thai_surname}    xpath=(//*[contains(@resource-id,'edt_content')])[2]
${ddl_province}    xpath=//XCUIElementTypeTextField[@value='กรุงเทพมหานคร']
${txt_search_on_popup}    xpath=//*[@value='ค้นหา']
${ddl_district}    xpath=//XCUIElementTypeTextField[@value='กรุณาเลือกเขต/อำเภอ']
${ddl_sub_district}    xpath=//XCUIElementTypeTextField[@value='กรุณาเลือกแขวง/ตำบล']
${btn_upload_id_card_picture}    xpath=//XCUIElementTypeCollectionView[@name="ThaiIDCollection"]/XCUIElementTypeCell
${btn_upload_shop_keeper_with_id_card_picture}    xpath=//XCUIElementTypeCollectionView[@name="MerchantWithThaiID"]/XCUIElementTypeCell
${btn_camera_upload_picture_popup}    accessibility_id=อัลบั้ม
${btn_allow_popup}    accessibility_id=OK
${btn_camera_roll}    accessibility_id=Camera Roll
${btn_first_picture}    xpath=(//XCUIElementTypeCell)[1]

${txt_thai_store_name}    xpath=//XCUIElementTypeTextField[@value='สำหรับร้านค้าจดในนามบุคคลธรรมดาเท่านั้น']
${txt_english_store_name}    xpath=//XCUIElementTypeTextField[@value='กรอกชื่อร้าน ภาษาอังกฤษ']
${chk_use_same_address_with_business_owner}    accessibility_id=ยอมรับข้อกำหนดและเงื่อนไข
${txt_merchant_phone_number}    xpath=//XCUIElementTypeTextField[@value='กรอกเบอร์โทรศัพท์ร้าน']
${ddl_merchant_type}    xpath=//XCUIElementTypeTextField[@value='กรุณาเลือกประเภทธุรกิจ']
${txa_shop_highlight}    xpath=//XCUIElementTypeStaticText[@name="จุดเด่นของร้านคุณ เพื่อช่วยโปรโมทในสื่อของทรูยู"]//following::XCUIElementTypeTextView
${btn_upload_store_product_picture}    xpath=//XCUIElementTypeCollectionView[@name="ShopInfoPhoto"]/XCUIElementTypeCell
${btn_confirmation}    accessibility_id=ตกลง
${txt_email_address}    xpath=(//XCUIElementTypeStaticText[@name="อีเมล*"]//following::XCUIElementTypeTextField)[1]

#Dynamic Locators
${rdo_owner_title_name}    xpath=//*[contains(@resource-id,'tv_content') and @text='_DYNAMIC_0']
${rdo_merchant_gender}    accessibility_id=_DYNAMIC_0
${ddl_item_on_popup}    xpath=//XCUIElementTypeStaticText[@name='_DYNAMIC_0']
${rdo_merchant_type}    xpath=//*[contains(@text,'_DYNAMIC_0')]
${rdo_contact_information}    accessibility_id=_DYNAMIC_0