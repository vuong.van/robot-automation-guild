*** Variables ***
${txt_mobile_no}    xpath=//XCUIElementTypeOther[@name="เบอร์โทรศัพท์"]//following-sibling::XCUIElementTypeTextField
${txt_thai_id}    xpath=//*[@value='9-9999-99999-99-9']
${btn_login}    accessibility_id=เข้าสู่ระบบ
${txt_true_money_otp}    xpath=(//XCUIElementTypeStaticText[@name="รหัสยืนยันจะส่งไปที่เบอร์"]//following:: XCUIElementTypeTextField)[1]
${btn_done_on_keyboard}    accessibility_id=Done