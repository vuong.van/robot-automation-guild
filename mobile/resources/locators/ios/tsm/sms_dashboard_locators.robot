*** Variables ***
${lbl_sms_remaining_credit}    xpath=//*[contains(@content-desc,'คงเหลือ ')][contains(@content-desc,' เครดิต')]
${btn_create_sms_campaign}    accessibility_id=close
${lbl_sms_credit}    accessibility_id=SMS เครดิต
${btn_arrow_back}    accessibility_id=arrow back

# Dynamic Locators
${tbl_cell_content_by_content}    xpath=(//XCUIElementTypeStaticText[@name="_DYNAMIC_0"])[_DYNAMIC_1]