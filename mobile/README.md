#### PRE-REQUISITES ####
[How To Write ReadMe file](https://www.makeareadme.com/)
# Appium Server
- iOS Device    
    - Run:
        - appium -p 4723 --session-override
- Android Device
    - Run:
        - appium -p 4725 --session-override

# Android Studio V3.1
    - How to list all emulator on your local machine?
        - Run:
            - cd ~/Library/Android/sdk/tools/bin && ./avdmanager list avd

# How to run emulator?
    - Run:
        - cd ~/Library/Android/sdk/tools &&./emulator -avd <EMULATOR NAME>
- Example:
        - cd ~/Library/Android/sdk/tools &&./emulator -avd NEXUS_5X

# Robot Framework
    - AppiumLibrary Installation
        - Command: 
            - Python 2: sudo pip install robotframework-appiumlibrary --ignore-installed six
            - Python 3: sudo pip3 install robotframework-appiumlibrary

# How to run test?
- Run:
    - robot -v ENV:<TEST ENV> -v OS:<PLATFORM> -v DEVICE:<TEST_DEVICE> <TEST FILE PATH>

- Examples:
    - robot -v ENV:alpha -v OS:ANDROID -v DEVICE:NEXUS_5X /Users/nantawan.cha/o2o-mobile-demo/testcases/tsm/tutorial_test.robot
    - robot -v ENV:alpha -v OS:IOS -v DEVICE:IPHONE_X /Users/nantawan.cha/o2o-mobile-demo/testcases/tsm/tutorial_test.robot

- In your local machine, please create folder 'app-path' for storing the .apk and .app test files.

- Example:
```
o2o-automation
 - web
 - mobile
 - api
 - e2e-tests
 
app-path
 - iOS
 - Android
 
NOTE: 
- The 'app-path' folder should be in the same level with mobile folder
```
# ADB
    - Install: brew cask install android-platform-tools
    - Useful commands:
        - List all packages on the connected device: ```adb shell 'pm list packages -f'```
# Install Python3
- [Install Python3](https://www.codingforentrepreneurs.com/blog/changing-default-python-3-in-terminal-for-mac-os)
- Use pip3 to install robotframework and another libraries
# Trouble shooting
    - Android
        - App keep stopping
            - Try to change the app version