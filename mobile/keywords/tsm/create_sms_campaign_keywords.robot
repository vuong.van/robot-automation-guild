*** Settings ***
Resource    ../../resources/locators/${OS}/tsm/create_sms_campaign_locators.robot
Resource    ../common/mobile_common.robot

*** Keywords ***
Create Sms Campaign
    [Arguments]    ${sms_title}    ${sms_content}
    mobile_common.Input Text    ${txt_sms_campaign_title}    ${sms_title}
    mobile_common.Input Text    ${txt_sms_content}    ${sms_content}
    mobile_common.Click Element    ${btn_forward_arrow}