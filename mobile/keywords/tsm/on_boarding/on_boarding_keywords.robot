*** Settings ***
Resource    ../../../keywords/common/mobile_common.robot
Resource    ../../../resources/locators/tsm/on_boarding_screen/on_boarding_locators.robot

*** Keywords ***
Tap On Activate First Button
    Wait Until Element Is Visible        &{onboarding_screen}[btn_activation]
    Click Element     &{onboarding_screen}[btn_activation]

Login To SaleApp
    [Arguments]     ${login_user}        ${login_password}
    Set Library Search Order	         AppiumLibrary
    Wait Until Element Is Visible        &{onboarding_screen}[btn_login]
    Input Text            &{onboarding_screen}[txt_username]            ${login_user}
    Input Text            &{onboarding_screen}[txt_password]            ${login_password}
    Click Element         &{onboarding_screen}[btn_login]