*** Settings ***
Resource    ../../../../keywords/common/mobile_common.robot
Resource    ../../../../resources/locators/tsm/login_trueid_screen.robot
Resource    ../../../../resources/locators/tsm/login_truemoney_screen.robot
Resource    ../../../../resources/locators/tsm/create_merchant_screen.robot
Resource    ../../../../resources/locators/tsm/create_trueyou_screen.robot

*** Variables ***
${NUMBERS}                            1234567890
${trueid_label}                       เข้าสู่ระบบ
${truemoney_label}                    กรุณากรอกข้อมูลสำหรับเชื่อมบัญชี
${trueid_username}                    0639148348
${trueid_password}                    password
${otp_number}                         123456
${phone}                              0600009003
${thai_id}                            1100166101050
${verify_success}                     สถานะการมอบสิทธิพิเศษทรูยู
${verify_merchant}                    ท่านได้ลงทะเบียน รับชำระสินค้าด้วยบัญชีทรูมันนี่ วอลเล็ทเรียบร้อยแล้ว

*** Keywords ***
Scrolling To Draw
     Sleep    ${SHORT_WAIT_TIME}
     Set Library Search Order	    AppiumLibrary
     Swipe               200    200  200   200  500
     Sleep    ${SHORT_WAIT_TIME}

Random Merchant Name
       ${random_merchant}    Generate Random String    10    ${NUMBERS}
       ${random_thai_merchant}=    Set Variable        ทดสอบร้านค้า-${random_merchant}
       ${random_english_merchant}=    Set Variable        test-merchant-${random_merchant}
       Set Suite Variable    ${random_thai_merchant}
       Set Suite Variable    ${random_english_merchant}

Scrolling Down
     Sleep    ${SHORT_WAIT_TIME}
     Set Library Search Order	    AppiumLibrary
     ${element_size}=    Get Element Size    &{screen}[screen_size]
     ${element_location}=    Get Element Location    &{screen}[screen_size]
     ${start_x}=         Evaluate      ${element_location['x']} + (${element_size['width']} * 0.5)
     ${start_y}=         Evaluate      ${element_location['y']} + (${element_size['height']} * 0.7)
     ${end_x}=           Evaluate      ${element_location['x']} + (${element_size['width']} * 0.5)
     ${end_y}=           Evaluate      ${element_location['y']} + (${element_size['height']} * 0.3)
     Swipe               ${start_x}    ${start_y}  ${end_x}  ${end_y}     600
     Sleep    ${SHORT_WAIT_TIME}

Login To TrueID
    [Arguments]    ${username}           ${password}
    AppiumLibrary.Wait Until Element Is Visible        &{trueid_login}[btn_register]
    AppiumLibrary.Click Element                        &{trueid_login}[btn_register]
    AppiumLibrary.Wait Until Element Is Visible        &{trueid_login}[btn_register_next]
    AppiumLibrary.Click Element                        &{trueid_login}[btn_register_next]
    AppiumLibrary.Wait Until Element Is Visible        &{trueid_login}[txt_username]
    AppiumLibrary.Input Text                           &{trueid_login}[txt_username]             ${trueid_username}
    AppiumLibrary.Wait Until Element Is Visible        &{trueid_login}[txt_password]
    AppiumLibrary.Input Text                           &{trueid_login}[txt_password]             ${trueid_password}
    AppiumLibrary.Click Element                        &{trueid_login}[btn_submit_login]

Login To TrueMoney
    [Arguments]            ${phone}           ${thai_id}
    Set Library Search Order	         AppiumLibrary
    Wait Until Page Contains             ${truemoney_label}
    Sleep         ${MEDIUM_WAIT_TIME}
    Wait Until Element Is Visible        &{truemoney_login}[txt_phone]
    Run Keyword If      '${OS_NAME}' == 'android'    Input Text        &{truemoney_login}[txt_phone]                   ${phone}
    Run Keyword If      '${OS_NAME}' == 'ios'        Input Value       &{truemoney_login}[txt_phone]                   ${phone}
    Run Keyword If      '${OS_NAME}' == 'ios'        Input Value       &{truemoney_login}[txt_phone]                   ${phone}
    Sleep         ${MEDIUM_WAIT_TIME}
    Wait Until Element Is Visible        &{truemoney_login}[txt_thai_id]
    Run Keyword If      '${OS_NAME}' == 'android'    Input Text         &{truemoney_login}[txt_thai_id]                ${thai_id}
    Run Keyword If      '${OS_NAME}' == 'ios'        Input Value        &{truemoney_login}[txt_thai_id]                ${thai_id}
    Run Keyword If      '${OS_NAME}' == 'ios'        Input Value        &{truemoney_login}[txt_thai_id]                ${thai_id}
    Run Keyword If      '${OS_NAME}' == 'android'        AppiumLibrary.Hide Keyboard
    Sleep         ${MEDIUM_WAIT_TIME}
    Run Keyword If      '${OS_NAME}' == 'ios'            Click Element     &{truemoney_login}[btn_done]
    Wait Until Element Is Visible        &{truemoney_login}[btn_submit_login]
    Click Element       &{truemoney_login}[btn_submit_login]
    Sleep    ${SHORT_WAIT_TIME}
    Wait Until Element Is Visible             &{truemoney_login}[txt_otp]
    Input Text          &{truemoney_login}[txt_otp]           ${otp_number}
    Wait Until Element Is Visible             &{truemoney_login}[btn_submit_otp]
    Click Element       &{truemoney_login}[btn_submit_otp]
    Wait Until Element Is Visible             &{truemoney_login}[lbl_no_store]
    Click Element       &{truemoney_login}[lbl_no_store]
    Run Keyword If      '${OS_NAME}' == 'android'    Wait Until Element Is Visible            &{truemoney_login}[btn_allow]
    Run Keyword If      '${OS_NAME}' == 'android'    Click Element    &{truemoney_login}[btn_allow]

Create Merchant Information Data
    [Arguments]    ${random_thai_merchant}        ${random_english_merchant}
    Set Library Search Order	         AppiumLibrary
    Wait Until Element Is Visible        &{create_merchant}[ddl_name_select]
    Click Element             	         &{create_merchant}[ddl_name_select]
    Wait Until Element Is Visible        &{create_merchant}[ddl_name_mr]
    Click Element                        &{create_merchant}[ddl_name_mr]
    Click Element     &{create_merchant}[chk_gender]
    Scrolling Down
    Wait Until Element Is Visible        &{create_merchant}[ddl_district]
    Click Element                        &{create_merchant}[ddl_district]
    Wait Until Element Is Visible        &{create_merchant}[ddl_district_opt]
    Click Element                        &{create_merchant}[ddl_district_opt]
    Wait Until Element Is Visible        &{create_merchant}[ddl_sub_district]
    Click Element                        &{create_merchant}[ddl_sub_district]
    Wait Until Element Is Visible        &{create_merchant}[ddl_sub_district_opt]
    Click Element                        &{create_merchant}[ddl_sub_district_opt]

     Run Keyword If      '${OS_NAME}' == 'android'    Scrolling Down
     Run Keyword If      '${OS_NAME}' == 'android'    Scrolling Down

    ####### UPLAODHON PHOTO  1
    Wait Until Element Is Visible        &{create_merchant}[btn_photo_first]
    Click Element                        &{create_merchant}[btn_photo_first]
    Run Keyword If      '${OS_NAME}' == 'android'    Wait Until Element Is Visible        &{create_merchant}[btn_take_photo]
    Run Keyword If      '${OS_NAME}' == 'android'    Click Element                        &{create_merchant}[btn_take_photo]
    Run Keyword If      '${OS_NAME}' == 'android'    Click Element    &{create_merchant}[btn_allow_permission]
    Run Keyword If      '${OS_NAME}' == 'android'        Wait Until Element Is Visible        &{create_merchant}[btn_shutter]
    Run Keyword If      '${OS_NAME}' == 'android'        Click Element                        &{create_merchant}[btn_shutter]
    Run Keyword If      '${OS_NAME}' == 'android'        Wait Until Element Is Visible        &{create_merchant}[btn_done]
    Run Keyword If      '${OS_NAME}' == 'android'        Click Element                        &{create_merchant}[btn_done]
    
    Run Keyword If      '${OS_NAME}' == 'ios'        Wait Until Element Is Visible        &{create_merchant}[btn_album]
    Run Keyword If      '${OS_NAME}' == 'ios'        Click Element     &{create_merchant}[btn_album]
    Run Keyword If      '${OS_NAME}' == 'ios'        Wait Until Element Is Visible        &{create_merchant}[btn_ok]
    Run Keyword If      '${OS_NAME}' == 'ios'        Click Element     &{create_merchant}[btn_ok]
    Run Keyword If      '${OS_NAME}' == 'ios'        Wait Until Element Is Visible         &{create_merchant}[btn_moment]
    Run Keyword If      '${OS_NAME}' == 'ios'        Click Element     &{create_merchant}[btn_moment]
    Run Keyword If      '${OS_NAME}' == 'ios'        Wait Until Element Is Visible         &{create_merchant}[img_photo]
    Run Keyword If      '${OS_NAME}' == 'ios'        Click Element    &{create_merchant}[img_photo]
    
    Sleep         ${MEDIUM_WAIT_TIME}
    
    ####### UPLAODHON PHOTO  2
    Wait Until Element Is Visible        &{create_merchant}[btn_photo_second]
    Click Element                        &{create_merchant}[btn_photo_second]
    Run Keyword If      '${OS_NAME}' == 'android'        Wait Until Element Is Visible        &{create_merchant}[btn_take_photo]
    Run Keyword If      '${OS_NAME}' == 'android'        Click Element                        &{create_merchant}[btn_take_photo]
    Run Keyword If      '${OS_NAME}' == 'android'        Wait Until Element Is Visible        &{create_merchant}[btn_shutter]
    Run Keyword If      '${OS_NAME}' == 'android'        Click Element                        &{create_merchant}[btn_shutter]
    Run Keyword If      '${OS_NAME}' == 'android'        Wait Until Element Is Visible        &{create_merchant}[btn_done]
    Run Keyword If      '${OS_NAME}' == 'android'        Click Element                        &{create_merchant}[btn_done]
    
    Run Keyword If      '${OS_NAME}' == 'ios'        Wait Until Element Is Visible        &{create_merchant}[btn_album]
    Run Keyword If      '${OS_NAME}' == 'ios'        Click Element     &{create_merchant}[btn_album]

    Run Keyword If      '${OS_NAME}' == 'ios'        Wait Until Element Is Visible         &{create_merchant}[btn_moment]
    Run Keyword If      '${OS_NAME}' == 'ios'        Click Element     &{create_merchant}[btn_moment]
    Run Keyword If      '${OS_NAME}' == 'ios'        Wait Until Element Is Visible         &{create_merchant}[img_photo]
    Run Keyword If      '${OS_NAME}' == 'ios'        Click Element    &{create_merchant}[img_photo]

    Sleep         ${MEDIUM_WAIT_TIME}

    Run Keyword If      '${OS_NAME}' == 'ios'        Scrolling Down

    Input Text                          &{create_merchant}[txt_merchant_th]                    ${random_thai_merchant}
    Run Keyword If      '${OS_NAME}' == 'ios'        Click Element      &{create_merchant}[btn_done]
    Input Text                          &{create_merchant}[txt_merchant_en]                    ${random_english_merchant}
    Run Keyword If      '${OS_NAME}' == 'ios'        Click Element      &{create_merchant}[btn_done]
    
    Run Keyword If      '${OS_NAME}' == 'android'        Scrolling Down
    
    Click Element                       &{create_merchant}[chk_use_address]
    Input Text                          &{create_merchant}[txt_phone]                          ${phone}
    
    Click Element                       &{create_merchant}[ddl_business_type]
    Sleep     ${SHORT_WAIT_TIME}
    Click Element                       &{create_merchant}[ddl_business_type_opt]

    Run Keyword If      '${OS_NAME}' == 'android'    Scrolling Down
    
    ####### UPLAODHON PHOTO  3
    Wait Until Element Is Visible        &{create_merchant}[btn_photo_third]
    Click Element                        &{create_merchant}[btn_photo_third]
    Run Keyword If      '${OS_NAME}' == 'android'        Wait Until Element Is Visible        &{create_merchant}[btn_take_photo]
    Run Keyword If      '${OS_NAME}' == 'android'        Click Element                        &{create_merchant}[btn_take_photo]
    Run Keyword If      '${OS_NAME}' == 'android'        Wait Until Element Is Visible        &{create_merchant}[btn_shutter]
    Run Keyword If      '${OS_NAME}' == 'android'        Click Element                        &{create_merchant}[btn_shutter]
    Run Keyword If      '${OS_NAME}' == 'android'        Wait Until Element Is Visible        &{create_merchant}[btn_done]
    Run Keyword If      '${OS_NAME}' == 'android'        Click Element                        &{create_merchant}[btn_done]
    
    Run Keyword If      '${OS_NAME}' == 'ios'        Wait Until Element Is Visible        &{create_merchant}[btn_album]
    Run Keyword If      '${OS_NAME}' == 'ios'        Click Element     &{create_merchant}[btn_album]

    Run Keyword If      '${OS_NAME}' == 'ios'        Wait Until Element Is Visible         &{create_merchant}[btn_moment]
    Run Keyword If      '${OS_NAME}' == 'ios'        Click Element     &{create_merchant}[btn_moment]
    Run Keyword If      '${OS_NAME}' == 'ios'        Wait Until Element Is Visible         &{create_merchant}[img_photo]
    Run Keyword If      '${OS_NAME}' == 'ios'        Click Element    &{create_merchant}[img_photo]
    
    Sleep    ${MEDIUM_WAIT_TIME}
    
    Scrolling Down
    Scrolling Down
    Scrolling Down

    Wait Until Element Is Visible        &{create_merchant}[btn_confirm]
    Click Element                        &{create_merchant}[btn_confirm]
    Sleep    ${MEDIUM_WAIT_TIME}
    Sleep    ${MEDIUM_WAIT_TIME}
    Wait Until Element Is Visible        &{create_merchant}[btn_thanks]
    Click Element                        &{create_merchant}[btn_thanks]
    Sleep    ${MEDIUM_WAIT_TIME}
    Sleep    ${MEDIUM_WAIT_TIME}

Create TrueYou Information Data
    Scrolling Down
    Run Keyword If      '${OS_NAME}' == 'android'        Scrolling Down
    Wait Until Element Is Visible        &{create_trueyou}[btn_apply_trueyou]
    Click Element       &{create_trueyou}[btn_apply_trueyou]
    Sleep    ${MEDIUM_WAIT_TIME}
    
    Wait Until Element Is Visible        &{create_trueyou}[chk_discount]
    Click Element       &{create_trueyou}[chk_discount]
    Input Text          &{create_trueyou}[txt_privilege_spec]            123
    Run Keyword If      '${OS_NAME}' == 'ios'        Wait Until Element Is Visible         &{create_trueyou}[btn_done]
    Run Keyword If      '${OS_NAME}' == 'ios'        Click Element     &{create_trueyou}[btn_done]
    
    Scrolling Down
    Scrolling Down

    Click Element            &{create_trueyou}[chk_shop_size]
    Scrolling Down
    Click Element            &{create_trueyou}[btn_continue]
    
    Scrolling Down
    Scrolling Down
    
    Wait Until Element Is Visible        &{create_trueyou}[btn_next]
    Click Element            &{create_trueyou}[btn_next]
    Sleep    ${MEDIUM_WAIT_TIME}
    Scrolling Down
    Scrolling Down
    Click Element       &{create_trueyou}[chk_term]
	Sleep    ${MEDIUM_WAIT_TIME}
    Click Element            &{create_trueyou}[btn_sign]
    # Sign: Scrolling To Draw
    Scrolling To Draw
    Scrolling Down
    
    Click Element            &{create_trueyou}[btn_finish]
    Sleep    ${MEDIUM_WAIT_TIME}
    Run Keyword If      '${OS_NAME}' == 'android'        Wait Until Element Is Visible        &{create_trueyou}[btn_continue]
    Run Keyword If      '${OS_NAME}' == 'android'        Click Element            &{create_trueyou}[btn_continue]
    Run Keyword If      '${OS_NAME}' == 'ios'            Click Element     &{create_trueyou}[btn_next_step]
    Sleep    ${MEDIUM_WAIT_TIME}
    Run Keyword If      '${OS_NAME}' == 'android'        Wait Until Page Contains            ${verify_success}
    Run Keyword If      '${OS_NAME}' == 'ios'        Wait Until Element Is Visible         &{create_trueyou}[lbl_success]
    Run Keyword If      '${OS_NAME}' == 'ios'        Click Element     &{create_trueyou}[btn_close]
    Run Keyword If      '${OS_NAME}' == 'ios'        Wait Until Page Contains            ${verify_success}
    Sleep    ${MEDIUM_WAIT_TIME}
    Sleep    ${MEDIUM_WAIT_TIME}
