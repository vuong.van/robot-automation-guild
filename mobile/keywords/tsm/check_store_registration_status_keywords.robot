*** Settings ***
Resource    ../../resources/locators/${OS}/tsm/check_store_registration_status_locators.robot
Resource    ../../../web/keywords/common/locator_common.robot
Resource    ../common/mobile_common.robot

*** Keywords ***
Enter Id Card Number
    [Arguments]    ${id_card_number}
    mobile_common.Input Text    ${txt_id_number}    ${id_card_number}

Tap On Agreed Button
    mobile_common.Click Element    ${btn_agreed}

Select Desired Merchant
    [Arguments]    ${merchant_name}
    ${locator}=    locator_common.Generate Element From Dynamic Locator    ${lbl_merchant}    ${merchant_name}
    mobile_common.Click Element    ${locator}

Tap On Allow Button On Popup
    [Arguments]    ${ignore_platform}=ios
    BuiltIn.Run Keyword If    '${ignore_platform}'!='${OS}'    mobile_common.Click Element    ${btn_allow_popup}

Access With Id Card Number
    [Arguments]    ${id_card_number}
    Enter Id Card Number    ${id_card_number}
    Tap On Agreed Button