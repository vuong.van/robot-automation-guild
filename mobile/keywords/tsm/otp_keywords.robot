*** Settings ***
Resource    ../../resources/locators/${OS}/tsm/otp_locators.robot

*** Keywords ***
Submit Otp Number
    [Arguments]    ${otp_number}
    mobile_common.Input Text    ${txt_otp_number}    ${otp_number}
    mobile_common.Click Element    ${btn_submit_otp_number}