*** Settings ***
Resource    ../../resources/locators/${OS}/tsm/select_merchant_type.robot
Resource    ../common/mobile_common.robot
Resource    ../../../web/keywords/common/locator_common.robot

*** Keywords ***
Select Merchant By Type
    [Arguments]    ${merchant_type}
    ${element}=    locator_common.Generate Element From Dynamic Locator    ${btn_merchant_type}    ${merchant_type}
    mobile_common.Click Element    ${element}