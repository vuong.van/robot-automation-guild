*** Settings ***
Resource    ../../resources/locators/${OS}/tsm/home_locators.robot
Resource    ../common/mobile_common.robot

*** Keywords ***
Tap On Find Merchant By Id Card Button
    AppiumLibrary.Wait Until Element Is Visible    ${btn_find_merchant_by_id_card}
    AppiumLibrary.Click Element    ${btn_find_merchant_by_id_card}

Tap On Create New Merchant With True Smart Merchant
    mobile_common.Click Element    ${btn_open_new_merchant_with_true_smart_merchant}