*** Settings ***
Resource    ../../resources/locators/${OS}/tsm/register_qr_merchant_locators.robot
Resource    ../../resources/locators/ios/tsm/register_qr_merchant_locators.robot
Resource    ../common/mobile_common.robot
Resource    ../../../web/keywords/common/locator_common.robot

*** Keywords ***
Select Owner Title
    [Arguments]    ${owner_title}
    mobile_common.Click Element    ${ddl_owner_title_name}
    BuiltIn.Run Keyword If    '${OS}'=='ios'    mobile_common.Select Dropdown Item On IOS Picker    ${owner_title}    4
    ${element}=    BuiltIn.Run Keyword If    '${OS}'=='android'    locator_common.Generate Element From Dynamic Locator    ${rdo_owner_title_name}    ${owner_title}
    BuiltIn.Run Keyword If    '${OS}'=='android'    mobile_common.Click Element    ${element}

Enter Merchant Owner Thai Name
    [Arguments]    ${lastname}    ${surname}
    mobile_common.Input Text    ${txt_merchant_thai_lastname}    ${lastname}
    mobile_common.Input Text    ${txt_merchant_thai_surname}    ${surname}

Select Merchant Owner Gender
    [Arguments]    ${gender}
    ${element}=    locator_common.Generate Element From Dynamic Locator    ${rdo_merchant_gender}    ${gender}
    mobile_common.Click Element    ${element}

Select Merchant Province
    [Arguments]    ${province}    ${district}    ${sub_district}
    mobile_common.Scroll Down    ${ddl_province}
    mobile_common.Click Element    ${ddl_province}
    mobile_common.Input Text    ${txt_search_on_popup}    ${province}
    BuiltIn.Run Keyword If    '${OS}'=='ios'    Hide The Keyboard
    ${element}=    locator_common.Generate Element From Dynamic Locator    ${ddl_item_on_popup}    ${province}
    mobile_common.Click Element    ${element}
    
    mobile_common.Scroll Down    ${ddl_district}
    mobile_common.Click Element    ${ddl_district}
    mobile_common.Input Text    ${txt_search_on_popup}    ${district}
    BuiltIn.Run Keyword If    '${OS}'=='ios'    Hide The Keyboard
    ${element}=    locator_common.Generate Element From Dynamic Locator    ${ddl_item_on_popup}    ${district}
    mobile_common.Click Element    ${element}
    
    mobile_common.Click Element    ${ddl_sub_district}    
    mobile_common.Input Text    ${txt_search_on_popup}    ${sub_district} 
    BuiltIn.Run Keyword If    '${OS}'=='ios'    Hide The Keyboard
    ${element}=    locator_common.Generate Element From Dynamic Locator    ${ddl_item_on_popup}    ${sub_district}
    mobile_common.Click Element    ${element}

Upload Id Card Picture
    mobile_common.Scroll Down    ${btn_upload_id_card_picture}
    mobile_common.Click Element    ${btn_upload_id_card_picture}
    BuiltIn.Run Keyword If    '${OS}'=='android'    Upload Picture On Android    ${True}
    BuiltIn.Run Keyword If    '${OS}'=='ios'    Upload Picture On IOS    ${True}
    mobile_common.Scroll Down    ${btn_upload_shop_keeper_with_id_card_picture}
    mobile_common.Click Element    ${btn_upload_shop_keeper_with_id_card_picture}
    BuiltIn.Run Keyword If    '${OS}'=='android'    Upload Picture On Android
    BuiltIn.Run Keyword If    '${OS}'=='ios'    Upload Picture On IOS

Enter Email Address
    [Arguments]    ${email_address}
    mobile_common.Scroll Down    ${txt_email_address}
    mobile_common.Input Text    ${txt_email_address}    ${email_address}
    BuiltIn.Run Keyword If    '${OS}'=='ios'    Hide The Keyboard
        
Upload Shop Keeper With Id Card
    mobile_common.Scroll Down    ${btn_upload_shop_keeper_with_id_card_picture}
    mobile_common.Click Element    ${btn_upload_shop_keeper_with_id_card_picture}
    Upload Picture On Android 

Upload Picture On Android
    [Arguments]    ${allow_popup}=${False}
    mobile_common.Click Element    ${btn_camera_upload_picture_popup}
    BuiltIn.Run Keyword If    ${allow_popup}==${True}    mobile_common.Click Element    ${btn_allow_popup}    
    mobile_common.Click Element    ${btn_take_picture}
    mobile_common.Click Element    ${btn_done_taking_picture}

Upload Picture On IOS
    [Arguments]    ${allow_popup}=${False}
    mobile_common.Click Element    ${btn_camera_upload_picture_popup}
    BuiltIn.Run Keyword If    ${allow_popup}==${True}    mobile_common.Click Element    ${btn_allow_popup}
    mobile_common.Click Element    ${btn_camera_roll}
    mobile_common.Click Element    ${btn_first_picture}
    
Enter Merchant Name
    [Arguments]    ${merchant_thai_name}    ${merchant_english_name}
    mobile_common.Scroll Down    ${txt_thai_store_name}
    mobile_common.Input Text    ${txt_thai_store_name}    ${merchant_thai_name}
    BuiltIn.Run Keyword If    '${OS}'=='ios'    Hide The Keyboard
    mobile_common.Scroll Down    ${txt_english_store_name}
    mobile_common.Input Text    ${txt_english_store_name}    ${merchant_english_name}
    BuiltIn.Run Keyword If    '${OS}'=='ios'    Hide The Keyboard

Check Use Same Address With Business Owner Checkbox
    mobile_common.Scroll Down    ${chk_use_same_address_with_business_owner}
    mobile_common.Click Element    ${chk_use_same_address_with_business_owner}

Enter Merchant Information Details
    [Arguments]    ${merchant_phone_number}    ${merchant_type}    ${shop_highlight}
    mobile_common.Scroll Down    ${txt_merchant_phone_number}
    mobile_common.Input Text    ${txt_merchant_phone_number}    ${merchant_phone_number}
    mobile_common.Click Element    ${ddl_merchant_type}
    ${element}=    BuiltIn.Run Keyword If    '${OS}'=='android'    locator_common.Generate Element From Dynamic Locator    ${rdo_merchant_type}    ${merchant_type}
    BuiltIn.Run Keyword If    '${OS}'=='ios'    mobile_common.Select Dropdown Item On IOS Picker    ${merchant_type}
    BuiltIn.Run Keyword If    '${OS}'=='android'    mobile_common.Click Element    ${element}
    mobile_common.Scroll Down    ${txa_shop_highlight}
    mobile_common.Input Text    ${txa_shop_highlight}    ${shop_highlight}
    BuiltIn.Run Keyword If    '${OS}'=='ios'    Hide The Keyboard

Upload Pictures Of Store And Product
    mobile_common.Scroll Down    ${btn_upload_store_product_picture}
    mobile_common.Click Element    ${btn_upload_store_product_picture}
    BuiltIn.Run Keyword If    '${OS}'=='android'    Upload Picture On Android
    BuiltIn.Run Keyword If    '${OS}'=='ios'    Upload Picture On IOS    

Select Contact Information
    [Arguments]    ${contact_information}
    ${element}=    locator_common.Generate Element From Dynamic Locator    ${rdo_contact_information}    ${contact_information}
    mobile_common.Scroll Down    ${element}
    mobile_common.Click Element    ${element}

Submit Merchant Registration Information
    mobile_common.Scroll Down    ${btn_confirmation}
    mobile_common.Check Element Enabled    ${btn_confirmation}    
    mobile_common.Click Element    ${btn_confirmation}
    