*** Settings ***
Resource    ../../resources/locators/${OS}/tsm/promotional_items_locators.robot
Resource    ../../../web/keywords/common/locator_common.robot
Resource    ../common/mobile_common.robot

*** Keywords ***
Select Promotional Item
    [Arguments]    ${promotional_item}
    ${locator}=    locator_common.Generate Element From Dynamic Locator    ${btn_promotional_item}    ${promotional_item}
    mobile_common.Click Element    ${locator}