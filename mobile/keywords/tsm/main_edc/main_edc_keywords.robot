*** Settings ***
Resource    ../../../keywords/common/mobile_common.robot
Resource    ../../../resources/locators/tsm/main_edc/main_edc_locators.robot

*** Keywords ***
Survey Popup Should Be Opened
    Wait Until Element Is Visible       &{main_edc}[survey_popup]

Click To Close Servey Popup
    Click Element      &{main_edc}[bth_close_popup]

Main EDC Screen Should Be Opened
    Run Keyword If      '${OS}' == 'android'
    ...     Run Keywords    Survey Popup Should Be Opened
    ...     AND    Click To Close Servey Popup
    Wait Until Element Is Visible    &{main_edc}[menu_total_sale_today_section]

Tap On Merchant Dashboard
    Wait Until Element Is Visible    &{main_edc}[menu_total_sale_today_section]
    Click Element     &{main_edc}[menu_total_sale_today_section]

Tap On Shop Profile Section
    Click Element      &{main_edc}[shop_profile_section]

Display Advice Menu Correctly
    Swipe Up
    Page Should Contain Text    &{main_edc_text}[menu_advice_1]
    Page Should Contain Text    &{main_edc_text}[menu_advice_2]

Tap On Advice Menu
    Swipe Up
    Click Text      &{main_edc_text}[menu_advice_1]

Display Sell Online Menu Correctly
    Swipe Up
    Page Should Contain Text    &{main_edc_text}[menu_sell_online_1]
    Page Should Contain Text    &{main_edc_text}[menu_sell_online_2]

Tap On Sell Online Menu
    Swipe Up
    Click Text      &{main_edc_text}[menu_sell_online_1]

Get Merchant Name On Shop Profile Section
    Wait Until Page Contains Element     &{main_edc}[lbl_merchant_name]
    ${merchant_name}=    Get Text    &{main_edc}[lbl_merchant_name]
    [Return]    ${merchant_name}
