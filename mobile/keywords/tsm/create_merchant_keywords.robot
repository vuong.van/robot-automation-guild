*** Settings ***
Resource    ../../resources/locators/${OS}/tsm/create_merchant_locators.robot
Resource    ../common/mobile_common.robot

*** Keywords ***
Tap On Allow Button
    BuiltIn.Run Keyword If    '${OS}'=='android'    mobile_common.Click Element    ${btn_allow_popup}

Tap On Start To Create A New Merchant
    mobile_common.Click Element    ${btn_start_to_create_new_merchant}

Random Merchant Name
       ${random_merchant}    Generate Random String    10    [NUMBERS]
       ${RANDOM_THAI_MERCHANT}=    Set Variable        ทดสอบร้านค้า-${random_merchant}
       ${RANDOM_ENGLISH_MERCHANT}=    Set Variable        test-merchant-${random_merchant}
       BuiltIn.Set Test Variable    ${RANDOM_THAI_MERCHANT}
       BuiltIn.Set Test Variable    ${RANDOM_ENGLISH_MERCHANT}