*** Settings ***
Resource    ../../resources/locators/${OS}/tsm/connect_true_money.robot
Resource    ../common/mobile_common.robot

*** Keywords ***
Connect True Money
    [Arguments]    ${mobile_no}    ${thai_id}    ${otp}
    #Work Around - Input Text can not enter True Money Thai Id on iOS, need to click on text box before input text
    BuiltIn.Run Keyword If    '${OS}'=='ios'    mobile_common.Click Element    ${txt_mobile_no}    
    mobile_common.Input Text    ${txt_mobile_no}    ${mobile_no}
    BuiltIn.Run Keyword If    '${OS}'=='ios'    mobile_common.Click Element    ${txt_thai_id} 
    mobile_common.Input Text    ${txt_thai_id}    ${thai_id}
    BuiltIn.Run Keyword If    '${OS}'=='ios'    mobile_common.Click Element    ${btn_done_on_keyboard}
    mobile_common.Click Element    ${btn_login}
    mobile_common.Input Text    ${txt_true_money_otp}    ${otp}    