*** Settings ***
Resource    ../../resources/locators/${OS}/tsm/qr_code_merchant_locators.robot
Resource    ../../../web/keywords/common/locator_common.robot
Resource    ../common/mobile_common.robot

*** Keywords ***
Select Merchant Privilege
    [Arguments]    ${merchant_privilege}
    ${element}=    locator_common.Generate Element From Dynamic Locator    ${btn_merchant_privelege}    ${merchant_privilege}
    BuiltIn.Run Keyword If    '${OS}'=='android'    mobile_common.Scroll Down    ${element}
    mobile_common.Click Element    ${element}    ${VERY_LONG_WAIT_TIME}