*** Settings ***
Resource    ../../resources/locators/${OS}/tsm/customer_privilege_list_locators.robot
Resource    ../../../web/keywords/common/locator_common.robot

*** Keywords ***
Select Customer Privilege
    [Arguments]    ${customer_privilege}
    ${element}=    locator_common.Generate Element From Dynamic Locator    ${btn_customer_privilege}    ${customer_privilege}
    mobile_common.Click Element    ${element}