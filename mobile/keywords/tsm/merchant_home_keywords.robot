*** Settings ***
Resource    ../../resources/locators/${OS}/tsm/merchant_home_locators.robot
Resource    ../../../web/keywords/common/locator_common.robot
Resource    ../common/mobile_common.robot   

*** Keywords ***
Select Merchant Feature
    [Arguments]    ${merchant_feature}
    ${element}=    locator_common.Generate Element From Dynamic Locator    ${btn_merchant_feature}    ${merchant_feature}
    BuiltIn.Run Keyword If    '${OS}'=='android'    mobile_common.Scroll Down    ${element}
    AppiumLibrary.Wait Until Element Is Visible    ${element}      
    mobile_common.Click Element    ${element}