*** Settings ***
Resource    ../../resources/locators/${OS}/tsm/sms_dashboard_locators.robot
Resource    ../../resources/locators/ios/tsm/sms_dashboard_locators.robot
Resource    ../common/mobile_common.robot
Resource    ../../../web/keywords/common/locator_common.robot

*** Keywords ***
Verify Sms Dashboard Page Displays
    AppiumLibrary.Wait Until Element Is Visible    ${lbl_sms_remaining_credit}
    AppiumLibrary.Wait Until Element Is Visible    ${btn_create_sms_campaign}

Tap On Create Sms Campaign Button
    mobile_common.Click Element    ${btn_create_sms_campaign}

Verify Created Sms Campaign On Sms Dashboard
    [Arguments]    ${sms_title}    ${sms_status}    ${total_audiences}    ${sms_created_date}=today
    ${created_date}=    BuiltIn.Run Keyword If    '${sms_created_date}'=='today'    DateTime.Get Current Date    result_format=%d/%m/%y
    ...    ELSE    BuiltIn.Set Variable    ${sms_created_date}
    BuiltIn.Set Test Variable    ${SMS_CAMPAIGN_CREATED_DATE}    ${created_date}

    ${title_locator}=    locator_common.Generate Element From Dynamic Locator    ${tbl_cell_content_by_content}    ${sms_title}    1
    mobile_common.Check Element Value    ${title_locator}    ${sms_title}
    
    ${created_date_locator}=    locator_common.Generate Element From Dynamic Locator    ${tbl_cell_content_by_content}    ${created_date}    1
    mobile_common.Check Element Value    ${created_date_locator}    ${created_date}
    
    ${status_locator}=    locator_common.Generate Element From Dynamic Locator    ${tbl_cell_content_by_content}    ${sms_status}    1
    mobile_common.Check Element Value    ${status_locator}    ${sms_status}
    
    ${total_audiences_locator}=    locator_common.Generate Element From Dynamic Locator    ${tbl_cell_content_by_content}    ${total_audiences}    1
    mobile_common.Check Element Value    ${total_audiences_locator}    ${total_audiences}

Tap On Arrow Back Button
    mobile_common.Click Element    ${btn_arrow_back}

Tap On Sms Title On Table
    [Arguments]    ${sms_title}
    ${title_locator}=    locator_common.Generate Element From Dynamic Locator    ${tbl_cell_content_by_content}    ${sms_title}    1
    mobile_common.Click Element    ${title_locator}