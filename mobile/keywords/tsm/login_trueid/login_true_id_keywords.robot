*** Settings ***
Resource    ../../../resources/locators/tsm/login_trueid/login_true_id_locators.robot
Resource    ../../common/mobile_common.robot

*** Keywords ***
Login With True Id
    [Arguments]    ${true_id_user_name}    ${true_id_password}
    mobile_common.Input Text    &{login_true_id}[txt_user_name]    ${true_id_user_name}
    mobile_common.Input Password    &{login_true_id}[txt_password]    ${true_id_password}
    mobile_common.Click Element    &{login_true_id}[btn_submit_login]