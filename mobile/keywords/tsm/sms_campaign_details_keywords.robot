*** Settings ***
Resource    ../../resources/locators/${OS}/tsm/sms_campaign_details_locators.robot
Resource    ../common/mobile_common.robot
Resource    ../../../web/keywords/common/locator_common.robot

*** Keywords ***
Verify Sms Campaign Summary
    [Arguments]    ${created_date}    ${number_of_receiver}    ${type_of_customer}    ${number_of_usable_quota}
    ${element}=    locator_common.Generate Element From Dynamic Locator    ${lbl_sms_summary}    วันที่สร้าง ​SMS
    mobile_common.Check Element Value    ${element}    ${created_date}    
    ${element}=    locator_common.Generate Element From Dynamic Locator    ${lbl_sms_summary}    จำนวนผู้รับ
    mobile_common.Check Element Value    ${element}    ${number_of_receiver}
    ${element}=    locator_common.Generate Element From Dynamic Locator    ${lbl_sms_summary}    ประเภทลูกค้า
    mobile_common.Check Element Value    ${element}    ${type_of_customer}
    ${element}=    locator_common.Generate Element From Dynamic Locator    ${lbl_sms_summary}    จำนวนเครดิตที่ใช้
    mobile_common.Check Element Value    ${element}    ${number_of_usable_quota}

Verify Sms Campaign Content
    [Arguments]    ${sms_summary}    ${sms_content}
    ${element}=    locator_common.Generate Element From Dynamic Locator    ${lbl_sms_title_and_content}    ${sms_summary}
    mobile_common.Check Element Value    ${element}    ${sms_summary}
    ${element}=    locator_common.Generate Element From Dynamic Locator    ${lbl_sms_title_and_content}    ${sms_content}
    mobile_common.Check Element Value    ${element}    ${sms_content}

Verify Cancel Sms Campaign Button Is Disabled
    mobile_common.Check Element Disabled    ${btn_cancel_sms_campaign}
    
Verify Cancel Sms Campaign Button Is Enabled
    mobile_common.Check Element Enabled    ${btn_cancel_sms_campaign}
