*** Settings ***
Library    String
Library    AppiumLibrary    run_on_failure=Capture Page Screenshot      timeout=${DEFAULT_TIMEOUT}
Library    String
Library    Collections
Library    ../../python_library/common.py
Resource    ../../../api/keywords/common/json_common.robot
Resource   ../../resources/locators/common/common_locators.robot

*** Keywords ***
Open Apps
    [Arguments]     ${APP_NAME}
    ${app_path_android}=    Get Canonical Path      ${CURDIR}/../../../app-path/Android/tsm.apk
    ${OS}=    Convert To Lowercase        ${OS}
    Run Keyword If      '${OS}' == 'android'
    ...     Open Application    ${REMOTE_URL_ANDROID}
    ...     platformName=&{${DEVICE}}[platformName]
    ...     platformVersion=&{${DEVICE}}[platformVersion]
    ...     deviceName=&{${DEVICE}}[deviceName]
    ...     app=${app_path_android}
#appWaitPackage and appWaitActivity are for "incorrect package and activity, retrying" error
#More details: https://github.com/appium/appium/blob/master/docs/en/writing-running-appium/android/activity-startup.md
    ...     appWaitPackage=${${APP_NAME}_PACKAGE}
    ...    appWaitActivity=com.eggdigital.trueyouedc.*
    ...     automationName=UiAutomator2
    ...     noReset=False
    Set Suite Variable    ${OS}

    ##OPEN DEFAULT OR SPECIFIC IOS SIMULATOR
    ${app_path_ios}=    Get Canonical Path      ${CURDIR}/../../../app-path/iOS/tsm.app
    Run Keyword If      '${OS}' == 'ios'
    ...     Open Application    ${REMOTE_URL_IOS}
    ...     platformName=&{${DEVICE}}[platformName]
    ...     platformVersion=&{${DEVICE}}[platformVersion]
    ...     deviceName=&{${DEVICE}}[deviceName]
    ...     app=${app_path_ios}
    ...     waitForAppScript='$.delay(5000); true'
    ...     noReset=False
    ...     automationName=XCUITest
    Set Suite Variable    ${OS}

Hide The Keyboard
     Run Keyword If    '${OS}' == 'android'      Hide Keyboard
     Run Keyword If    '${OS}' == 'ios'          Click Element    &{keyboard}[keyboard_done]

Swipe To Left
     ${area}     Get Element Size    &{screen}[screen_size]
     ${start_x}=      Evaluate    0.67 * &{area}[width]
     ${start_y}=      Evaluate    0.90 * &{area}[height]
     ${offset_x}=     Evaluate    0 * &{area}[width]
     ${offset_y}=     Evaluate    0.30 * &{area}[height]
     Swipe    ${start_x}    ${start_y}    ${offset_x}    ${offset_y}    1000

Swipe Up
     ${area}=    Get Element Size    &{screen}[screen_size]
     ${start_x}=      Evaluate    0.5 * &{area}[width]
     ${start_y}=      Evaluate    0.7 * &{area}[height]
     ${offset_x}=     Evaluate    0.5 * &{area}[width]
     ${offset_y}=     Evaluate    0.3 * &{area}[height]
     Swipe    ${start_x}    ${start_y}    ${offset_x}    ${offset_y}    1000

Select Item On List View
     [Arguments]     ${tbl_list}     ${item}
     @{elements}     Get Webelements    ${tbl_list}
     Click Element    @{elements}[${item}]

Click On Menu
    [Arguments]     ${mnu}
    Click Element       ${mnu}

Click On Back Button
    [Arguments]     ${btn_back}
    Click Element       ${btn_back}

Click On OK Button
    [Arguments]     ${btn_ok}
    Click Element       ${btn_ok}

Alert Popup Is Displayed
    [Arguments]     ${alert}
    Wait Until Element Is Visible       ${alert}        timeout=10s

Click On Allow Button
    [Arguments]     ${btn_allow}
    Wait Until Element Is Visible        ${btn_allow}
    Click Element       ${btn_allow}

Accept Alert Popup
    ${status}   ${value} =      Run Keyword And Ignore Error        Alert Popup Is Displayed    &{alert}[page_alert]
    Run Keyword If      '${status}' == 'PASS'       Click On Allow Button    &{alert}[btn_allow]
    Run Keyword Unless  '${status}' == 'PASS'       Wait Until Page Does Not Contain Element    &{alert}[page_alert]

Switch To Webview Context
    ${default_context}=    Get Current Context
    @{all_context}=    Get Contexts
    ${count_list}=    Get Length    ${all_context}
    ${webview_context}=    Get From List    ${all_context}    ${count_list-1}
    Switch To Context    ${webview_context}

Switch To Native Context
     ${default_context}=    Get Current Context
     @{all_context}=    Get Contexts
     ${count_list}=    Get Length    ${all_context}
     ${native_context}=    Get From List    ${all_context}    0
     Switch To Context    ${native_context}

Page Should Contain Property With Value
    [Arguments]    ${page_content}
    Page Should Contain Text    ${page_content}

Click On Go Button
    [Arguments]     ${btn_go}
    Wait Until Element Is Visible        ${btn_go}
    Click Element       ${btn_go}
    
Open SaleApp Android
    [Arguments]     ${APP_NAME}
    ${app_path_android}=    Get Canonical Path      ${CURDIR}/../../../app-path/Android/saleapp.apk
    ${OS}=    Convert To Lowercase        ${OS}
    Run Keyword If      '${OS}' == 'android'
    ...     Open Application    ${REMOTE_URL_ANDROID}
    ...     platformName=&{${DEVICE}}[platformName]
    # ...     platformVersion=&{${DEVICE}}[platformVersion]
    ...     deviceName=&{${DEVICE}}[deviceName]
    ...     app=${app_path_android}
    ...     appPackage=com.tdcm.android.trueyoupartner.alpha
    ...     automationName=uiautomator2
    ...     noReset=False
    Set Suite Variable    ${OS_NAME}    ${OS}

Click Element
    [Arguments]    ${locator}    ${timeout}=${DEFAULT_TIMEOUT}
    Wait Element Visible    ${locator}    ${timeout}
    AppiumLibrary.Click Element    ${locator}

Input Text
    [Arguments]    ${locator}    ${input_text}    ${timeout}=${DEFAULT_TIMEOUT}
    Wait Element Visible    ${locator}    ${timeout}
    AppiumLibrary.Input Text    ${locator}    ${input_text}

Get Element Attribute
    [Arguments]    ${locator}    ${property}    ${timeout}=${DEFAULT_TIMEOUT}
    Wait Element Visible    ${locator}    ${timeout}
    ${property_value}=    AppiumLibrary.Get Element Attribute    ${locator}    ${property}
    [Return]    ${property_value}

Input Password
    [Arguments]    ${locator}    ${input_password}    ${timeout}=${DEFAULT_TIMEOUT}
    Wait Element Visible    ${locator}    ${timeout}
    AppiumLibrary.Input Password    ${locator}    ${input_password}

Check Element Text
    [Arguments]    ${locator}    ${expected_text}    ${timeout}=${DEFAULT_TIMEOUT}
    Wait Element Visible    ${locator}    ${timeout}
    AppiumLibrary.Element Text Should Be    ${locator}    ${expected_text}

Check Element Attribute
    [Arguments]    ${locator}    ${attribute_name}    ${expected_value}    ${timeout}=${DEFAULT_TIMEOUT}
    Wait Element Visible    ${locator}    ${timeout}
    AppiumLibrary.Element Attribute Should Match    ${locator}    ${attribute_name}    ${expected_value}
    
Check Element Enabled
    [Arguments]    ${locator}    ${timeout}=${DEFAULT_TIMEOUT}
    Wait Element Visible    ${locator}    ${timeout}
    BuiltIn.Run Keyword If    '${OS}'=='ios'    AppiumLibrary.Element Should Be Enabled    ${locator}
    BuiltIn.Run Keyword If    '${OS}'=='android'    AppiumLibrary.Element Attribute Should Match    ${locator}    clickable    true

Check Element Value
    [Arguments]    ${locator}    ${expected_value}    ${timeout}=${DEFAULT_TIMEOUT}
    Wait Element Visible    ${locator}    ${timeout}
    BuiltIn.Run Keyword If    '${OS}'=='ios'    AppiumLibrary.Element Value Should Be    ${locator}    ${expected_value}
    BuiltIn.Run Keyword If    '${OS}'=='android'    AppiumLibrary.Element Attribute Should Match    ${locator}    content-desc    ${expected_value}

Check Element Disabled
    [Arguments]    ${locator}    ${timeout}=${DEFAULT_TIMEOUT}
    Wait Element Visible    ${locator}    ${timeout}
    AppiumLibrary.Element Should Be Disabled    ${locator}

Wait Until Element Property Change
    [Arguments]    ${locator}    ${property}    ${expected_value}    ${timeout}
    :FOR    ${index}    IN RANGE    1    ${timeout}
    \    ${current_property_value}=    BuiltIn.Run Keyword And Ignore Error    AppiumLibrary.Get Element Attribute    ${locator}    ${property}
    \    BuiltIn.Run Keyword And Ignore Error    BuiltIn.Exit For Loop If    '${current_property_value}'=='${expected_value}'
    \    BuiltIn.Sleep    1s    

Wait Element Visible
    [Arguments]    ${locator}    ${timeout}=${DEFAULT_TIMEOUT}
    BuiltIn.Run Keyword And Ignore Error    AppiumLibrary.Wait Until Element Is Visible    ${locator}    ${timeout}

Scroll Down
    [Arguments]    ${locator}
    AppiumLibrary.Register Keyword To Run On Failure    Nothing
    :FOR    ${i}    IN RANGE    10
    \    ${present}=  Run Keyword And Return Status    AppiumLibrary.Wait Until Element Is Visible    ${locator}    ${SHORT_WAIT_TIME}
    \    Exit For Loop If    '${present}' == 'True'
    \    BuiltIn.Run Keyword And Ignore Error    AppiumLibrary.Swipe By Percent    50    80    50    50
    AppiumLibrary.Register Keyword To Run On Failure    AppiumLibrary.Capture Page Screenshot

Select Dropdown Item On IOS Picker
    [Arguments]    ${item}    ${total_items}=20
    ${picker_text}=    Get Element Attribute    ${ddl_ios_picker}    value
    ${json_element_location}=    AppiumLibrary.Get Element Location    ${frm_ios_picker}
    ${x_coordinate_picker}=    json_common.Get Property Value From Json By Index    .x    0    ${json_element_location}
    ${y_coordinate_picker}=    json_common.Get Property Value From Json By Index    .y    0    ${json_element_location}
    ${y_coordinate_to_swipe}=    BuiltIn.Set Variable    ${${y_coordinate_picker-25}}
    :FOR    ${index}    IN RANGE    0    ${total_items}
    \    BuiltIn.Exit For Loop If    '${picker_text}'=='${item}'
    \    AppiumLibrary.Swipe    ${x_coordinate_picker}    ${y_coordinate_picker}    ${0}    ${${y_coordinate_to_swipe}}
    \    ${picker_text}=    Get Element Attribute    ${ddl_ios_picker}    value
    Click Element    ${btn_finish_on_ios_picker}